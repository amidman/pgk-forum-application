package com.example.pgk_forum_app.data.api.user

import com.example.pgk_forum_app.data.api.BaseRoutes

object UserRoutes {

    val roleQ = "role"
    val pwdQ = "pwd"
    val fioQ = "fio"
    val loginQ = "login"

    private const val base = BaseRoutes.USER_ROUTE
    const val REGISTER = base + "register"
    const val LOGIN = base + "login"
    const val INFO = base + "info"
    const val ALL = base + "all"
    const val PASSWORD = base + "change-password"
    const val CHANGE_ROLE = "$base{id?}/change-role"
    const val CHANGE_LOGIN = base + "change-login"
    const val CHANGE_FIO = base + "change-fio"
    fun passwordByAdmin(id:Int) = base + "${id}/change-password"
    fun changeLoginByAdmin(id:Int) = base + "${id}/change-login"
    fun changeFioByAdmin(id: Int) = base + "${id}/change-fio"
    const val ID = "$base{id?}"
}