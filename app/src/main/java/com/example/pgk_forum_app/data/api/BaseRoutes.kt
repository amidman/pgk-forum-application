package com.example.pgk_forum_app.data.api

object BaseRoutes {

    const val BASE_URL = "http://10.0.2.2:8080"
    const val USER_ROUTE = "/users/"
    const val STUDENT_ROUTE = "/students/"
    const val TEACHER_ROUTE = "/teachers/"
    const val GROUP_ROUTE = "/groups/"
    const val THEME_ROUTE = "/themes/"
    const val TEST_ROUTE = "/tests/"
    const val STUDENT_ANSWER_ROUTE = "/student-answers/"
}