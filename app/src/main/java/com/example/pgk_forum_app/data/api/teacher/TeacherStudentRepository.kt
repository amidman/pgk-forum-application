package com.example.pgk_forum_app.data.api.teacher

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*
import kotlin.text.get

class TeacherStudentRepository(
    private val client: HttpClient,
    private val tokenRepository:TokenRepository,
) {


    suspend fun getStudentTeachers():RequestStatus<List<TeacherInfo>>
    = catchResponseException {
        client.get(TeacherRoutes.GET_STUDENT_TEACHERS){
            setHeader(tokenRepository.getToken())
        }.toRequestStatus()
    }


}