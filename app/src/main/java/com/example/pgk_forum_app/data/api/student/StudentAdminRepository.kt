package com.example.pgk_forum_app.data.api.student

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class StudentAdminRepository(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository,
) {

    suspend fun deleteStudent(studentId:Int): RequestStatus<String?> =
        catchResponseException {
            client.delete(StudentRoutes.ID + studentId.toString()){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

    suspend fun setStudentGroupNull(studentLogin: String): RequestStatus<StudentInfo> =
        catchResponseException {
            client.put(StudentRoutes.SET_GROUPNAME_NULL){
                setHeader(tokenRepository.getToken())
                url { parameters.append(StudentRoutes.studentLoginQ,studentLogin) }
            }.toRequestStatus()
        }

    suspend fun changeStudentGroup(studentLogin: String,groupName:String): RequestStatus<StudentInfo> =
        catchResponseException {
            client.put(StudentRoutes.CHANGE_GROUP){
                setHeader(tokenRepository.getToken())
                url {
                    parameters.append(StudentRoutes.groupNameQ,groupName)
                    parameters.append(StudentRoutes.studentLoginQ,studentLogin)
                }
            }.toRequestStatus()
        }
}