package com.example.pgk_forum_app.data.api.student_answers

import com.example.pgk_forum_app.data.api.BaseRoutes

object StudentAnswerRoutes {
    const val base = BaseRoutes.STUDENT_ANSWER_ROUTE


    val groupIdQ = "groupId"
    val studentIdQ = "studentId"
    val testIdQ = "testId"

    const val GROUP_ANSWERS = base + "group-answers"
    const val CREATE = base + "create"
    const val STUDENT_ANSWERS = base + "student-answers"

}