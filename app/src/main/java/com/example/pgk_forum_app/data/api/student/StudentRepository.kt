package com.example.pgk_forum_app.data.api.student

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.domain.model.student.StudentInfoForRegistration
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class StudentRepository(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository
) {


    suspend fun getAllStudents():RequestStatus<List<StudentInfo>> =
        catchResponseException {
            client.get(StudentRoutes.ALL){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

    suspend fun createStudent(studentInfo: StudentInfoForRegistration):RequestStatus<StudentInfo> =
        catchResponseException {
            client.post(StudentRoutes.REGISTER){
                setBody(studentInfo)
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }




}