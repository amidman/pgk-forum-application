package com.example.pgk_forum_app.data.api.test

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class TestTeacherRepository(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository,
) {


    suspend fun getTeacherTests():RequestStatus<List<TestInfo>> =
        catchResponseException {
            client.get(TestRoutes.GET_TEACHER_TESTS){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

    suspend fun createTestByTeacher(testInfo: TestInfo):RequestStatus<TestInfo> =
        catchResponseException {
            client.post(TestRoutes.CREATE){
                setBody(testInfo)
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }


    suspend fun delete(testId:Int):RequestStatus<String?> =
        catchResponseException {
            client.delete(TestRoutes.DELETE){
                setHeader(tokenRepository.getToken())
                url {
                    parameters.append(TestRoutes.testIdQ,testId.toString())
                }
            }.toRequestStatus()
        }

    suspend fun changeTestTheme(testId:Int,themeName:String):RequestStatus<String?> =
        catchResponseException {
            client.put(TestRoutes.CHANGE_THEME){
                setHeader(tokenRepository.getToken())
                url{
                    parameters.append(TestRoutes.testIdQ,testId.toString())
                    parameters.append(TestRoutes.themeNameQ,themeName)
                }
            }.toRequestStatus()
        }

    suspend fun changeTestTitle(testId:Int,newTitle:String):RequestStatus<String?> =
        catchResponseException {
            client.put(TestRoutes.CHANGE_TITLE){
                setHeader(tokenRepository.getToken())
                url{
                    parameters.append(TestRoutes.testIdQ,testId.toString())
                    parameters.append(TestRoutes.themeTitleQ,newTitle)
                }
            }.toRequestStatus()
        }


}