package com.example.pgk_forum_app.data.datastore.auth

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.example.pgk_forum_app.data.datastore.DataStoreKeys
import io.ktor.client.request.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TokenRepository(
    private val context: Context
) {

    private val tokenPreference = stringPreferencesKey(DataStoreKeys.TOKEN)

    suspend fun postToken(token: String) {
        context.userDataStore.edit {
            it[tokenPreference] = token
        }
    }

    suspend fun getToken(): String {
        val token = context.userDataStore.data.map {
            it[tokenPreference]
        }
        return token.first() ?: ""
    }

}

fun HttpRequestBuilder.setHeader(token: String){
    header("Authorization","Bearer $token")
}