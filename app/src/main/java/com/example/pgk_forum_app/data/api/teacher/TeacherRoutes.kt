package com.example.pgk_forum_app.data.api.teacher

import com.example.pgk_forum_app.data.api.BaseRoutes

object TeacherRoutes {

    const val teacherIdQ = "teacherId"


    private const val base = BaseRoutes.TEACHER_ROUTE


    const val REGISTER = base + "register"
    const val INFO = base + "info"
    const val GET_GROUPS = base + "get-groups"
    const val GET_GROUPS_BY_ID = "get-groups-by-id"
    const val ALL = base + "all"
    const val ID = base
    const val ALL_WITH_GROUP = base + "all-with-group"
    const val GET_STUDENT_TEACHERS = base + "get-student-teachers"

}