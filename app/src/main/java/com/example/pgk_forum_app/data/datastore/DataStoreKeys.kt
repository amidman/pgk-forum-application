package com.example.pgk_forum_app.data.datastore

object DataStoreKeys {
    const val USER_DATASTORE = "UserData"

    const val REFRESH_TOKEN = "refreshToken"

    const val ROLE = "role"

    const val TOKEN = ""

}