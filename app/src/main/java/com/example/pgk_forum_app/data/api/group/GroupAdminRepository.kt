package com.example.pgk_forum_app.data.api.group

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.api.teacher.TeacherRoutes
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.group.TeacherInfoWithGroupDTO
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class GroupAdminRepository(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository,
) {

    suspend fun addGroupToTeacher(teacherLogin:String,groupName:String):RequestStatus<TeacherInfoWithGroupDTO> =
        catchResponseException {
            client.put(GroupRoutes.ADD_TO_TEACHER){
                setHeader(tokenRepository.getToken())
                url {
                    parameters.append(GroupRoutes.groupNameQ,groupName)
                    parameters.append(GroupRoutes.teacherLoginQ,teacherLogin)
                }
            }.toRequestStatus()
        }

    suspend fun deleteGroupTeacher(teacherLogin: String,groupId:Int):RequestStatus<String?> =
        catchResponseException {
            client.delete(GroupRoutes.REMOVE_GROUP_FROM_TEACHER){
                setHeader(tokenRepository.getToken())
                url {
                    parameters.append(GroupRoutes.groupIdQ,groupId.toString())
                    parameters.append(GroupRoutes.teacherLoginQ,teacherLogin)
                }
            }.toRequestStatus()
        }

    suspend fun changeGroupName(groupId: Int,newName:String):RequestStatus<String?> =
        catchResponseException {
            client.put(GroupRoutes.changeName(groupId)){
                setHeader(tokenRepository.getToken())
                url{ parameters.append(GroupRoutes.groupNameQ,newName) }
            }.toRequestStatus()
        }

    suspend fun changeShortName(groupId: Int,newShortName:String):RequestStatus<String?> =
        catchResponseException {
            client.put(GroupRoutes.changeShortName(groupId)){
                setHeader(tokenRepository.getToken())
                url{ parameters.append(GroupRoutes.shortNameQ,newShortName) }
            }.toRequestStatus()
        }

    suspend fun getTeacherGroups(teacherId:Int): RequestStatus<List<GroupInfo>> =
        catchResponseException {
            val response = client.get(TeacherRoutes.GET_GROUPS_BY_ID){
                url { parameters.append(TeacherRoutes.teacherIdQ, teacherId.toString()) }
                setHeader(tokenRepository.getToken())
            }
            response.toRequestStatus()
        }
}