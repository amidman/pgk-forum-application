package com.example.pgk_forum_app.data.api.user

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class UserAdminRepository(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository
) {

    suspend fun changeFio(userId:Int,newFio:String):RequestStatus<String?> =
        catchResponseException {
            client.put(UserRoutes.changeFioByAdmin(userId)){
                setHeader(tokenRepository.getToken())
                url{parameters.append(UserRoutes.fioQ,newFio)}
            }.toRequestStatus()
        }



    suspend fun changeLogin(userId: Int,newLogin:String):RequestStatus<String?> =
        catchResponseException{
            client.put(UserRoutes.changeLoginByAdmin(userId)){
                setHeader(tokenRepository.getToken())
                url{parameters.append(UserRoutes.loginQ,newLogin)}
            }.toRequestStatus()
        }

    suspend fun changePassword(userId:Int,newPassword:String):RequestStatus<String?> =
        catchResponseException{
            client.put(UserRoutes.passwordByAdmin(userId)){
                setHeader(tokenRepository.getToken())
                url{parameters.append(UserRoutes.pwdQ,newPassword)}
            }.toRequestStatus()
        }

    suspend fun changeRole(newRole:String){

    }
}