package com.example.pgk_forum_app.data.api

import com.example.pgk_forum_app.domain.model.network_base.ErrorModel
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import io.ktor.client.plugins.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.ConnectException

class Errors<T> {

    val CONNECTION_ERROR = RequestStatus.Error<T>(ErrorModel("connection error","Ошибка подключения"))

    val SERVER_ERROR = RequestStatus.Error<T>(ErrorModel("server error","Ошибка сервера"))
}

suspend fun <T> catchResponseException(action:suspend CoroutineScope.() -> RequestStatus<T>):RequestStatus<T>
= withContext(Dispatchers.IO){
    try {
        action()
    }catch (e:ServerResponseException){
        Errors<T>().SERVER_ERROR
    }catch (e:ConnectException) {
        Errors<T>().CONNECTION_ERROR
    }
}