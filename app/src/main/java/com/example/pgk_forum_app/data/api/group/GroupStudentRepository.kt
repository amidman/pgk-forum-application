package com.example.pgk_forum_app.data.api.group

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.api.student.StudentRoutes
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.group.GroupInfoWithTeachersGroup
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class GroupStudentRepository(
    private val tokenRepository: TokenRepository,
    private val client: HttpClient,
) {

    suspend fun getStudentGroup(): RequestStatus<GroupInfoWithTeachersGroup> =
        catchResponseException {
            client.get(GroupRoutes.STUDENT_GROUP){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }
}