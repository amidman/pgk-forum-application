package com.example.pgk_forum_app.data.api.theme

import com.example.pgk_forum_app.data.api.BaseRoutes

object ThemeRoutes {

    val themeNameQ = "themeName"
    val teacherIdQ = "teacherId"
    val descriptionQ = "description"

    const val base = BaseRoutes.THEME_ROUTE

    const val ALL = base + "all"
    const val CREATE = base + "create"
    const val ID = base
    const val GET_TEACHER_THEMES = base + "get-teacher-themes"
    fun changeName(id:Int) = base + "${id}/change-name"
    fun changeDescription(id:Int) = base + "${id}/change-description"
}