package com.example.pgk_forum_app.data.api.test

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class TestRepository(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository
) {

    suspend fun getTeacherTests(teacherId:Int): RequestStatus<List<TestInfo>>
    = catchResponseException {
        client.get(TestRoutes.GET_TESTS_BY_TEACHER_ID){
            url {
                parameters.append(TestRoutes.teacherIdQ,teacherId.toString())
            }
        }.toRequestStatus()
    }
}