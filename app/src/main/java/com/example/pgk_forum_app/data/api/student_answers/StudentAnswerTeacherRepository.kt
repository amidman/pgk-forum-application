package com.example.pgk_forum_app.data.api.student_answers

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class StudentAnswerTeacherRepository(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository,
) {

    suspend fun getGroupStudentAnswers(testId:Int,groupId:Int):RequestStatus<List<StudentAnswer>> =
        catchResponseException {
            client.get(StudentAnswerRoutes.GROUP_ANSWERS){
                setHeader(tokenRepository.getToken())
                url{
                    parameters.append(StudentAnswerRoutes.groupIdQ,groupId.toString())
                    parameters.append(StudentAnswerRoutes.testIdQ,testId.toString())
                }
            }.toRequestStatus()
        }
}