package com.example.pgk_forum_app.data.api.teacher

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfoWithGroupList
import com.example.pgk_forum_app.domain.model.user.UserDTO
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class TeacherRepository(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository
) {
    suspend fun getAllTeachers():RequestStatus<List<TeacherInfo>> =
        catchResponseException {
            client.get(TeacherRoutes.ALL){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

    suspend fun getAllTeacherWithGroups():RequestStatus<List<TeacherInfoWithGroupList>> =
        catchResponseException {
            client.get(TeacherRoutes.ALL_WITH_GROUP){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

    suspend fun deleteTeacher(id:Int):RequestStatus<String?> =
        catchResponseException {
            client.delete(TeacherRoutes.ID+id.toString()){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

    suspend fun createTeacher(userDTO:UserDTO):RequestStatus<TeacherInfo> =
        catchResponseException{
            client.post(TeacherRoutes.REGISTER){
                setHeader(tokenRepository.getToken())
                setBody(userDTO)
            }.toRequestStatus()
        }








}