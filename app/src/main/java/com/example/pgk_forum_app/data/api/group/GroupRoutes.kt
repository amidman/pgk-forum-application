package com.example.pgk_forum_app.data.api.group

import com.example.pgk_forum_app.data.api.BaseRoutes
import com.example.pgk_forum_app.data.api.student.StudentRoutes

object GroupRoutes {

    val teacherLoginQ = "teacherLogin"
    val teacherIdQ = "teacherId"
    val groupIdQ = "groupId"
    val groupNameQ = "groupName"
    val shortNameQ = "shortName"


    private const val base = BaseRoutes.GROUP_ROUTE

    const val ALL = base + "all"
    const val CREATE = base + "create"
    const val ID = base
    const val ADD_TO_TEACHER = base + "add-to-teacher"
    const val REMOVE_GROUP_FROM_TEACHER = base + "remove-from-teacher"
    const val STUDENT_GROUP = base + "student-group"
    const val GET_BY_NAME = base + "get-by-name"
    fun changeName(id:Int) = base + "${id}/change-name"
    fun changeShortName(id: Int) = base + "${id}/change-shortname"
}