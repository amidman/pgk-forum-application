package com.example.pgk_forum_app.data.api.theme

import android.content.res.Resources
import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.theme.ThemeDTO
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class ThemeTeacherRepository(
    private val client:HttpClient,
    private val tokenRepository: TokenRepository,
) {


    suspend fun getTeacherThemes():RequestStatus<List<ThemeDTO>> =
        catchResponseException {
            client.get(ThemeRoutes.GET_TEACHER_THEMES){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

    suspend fun createTheme(themeDTO: ThemeDTO):RequestStatus<ThemeDTO> =
        catchResponseException {
            client.post(ThemeRoutes.CREATE){
                setHeader(tokenRepository.getToken())
                setBody(themeDTO)
            }.toRequestStatus()
        }

    suspend fun changeThemeName(themeId:Int,newName:String):RequestStatus<String?> =
        catchResponseException {
            client.put(ThemeRoutes.changeName(themeId)) {
                setHeader(tokenRepository.getToken())
                url {
                    parameters.append(ThemeRoutes.themeNameQ,newName)
                }
            }.toRequestStatus()
        }

    suspend fun changeThemeDescription(themeId:Int,newDescription:String):RequestStatus<String?> =
        catchResponseException {
            client.put(ThemeRoutes.changeDescription(themeId)) {
                setHeader(tokenRepository.getToken())
                url {
                    parameters.append(ThemeRoutes.descriptionQ,newDescription)
                }
            }.toRequestStatus()
        }

    suspend fun deleteTheme(themeId: Int):RequestStatus<String?> =
        catchResponseException {
            client.delete(ThemeRoutes.ID + "$themeId"){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

}