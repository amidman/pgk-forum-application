package com.example.pgk_forum_app.data.api.student_answers

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class StudentAnswerStudentRepository(
    private val tokenRepository: TokenRepository,
    private val client: HttpClient,
) {

    suspend fun createStudentAnswer(studentAnswer: StudentAnswer):RequestStatus<StudentAnswer>
    = catchResponseException {
        client.post(StudentAnswerRoutes.CREATE){
            setBody(studentAnswer)
            setHeader(tokenRepository.getToken())
        }.toRequestStatus()
    }

    suspend fun getStudentAnswers(): RequestStatus<List<StudentAnswer>>
    = catchResponseException {
        client.get(StudentAnswerRoutes.STUDENT_ANSWERS){
            setHeader(tokenRepository.getToken())
        }.toRequestStatus()
    }

}