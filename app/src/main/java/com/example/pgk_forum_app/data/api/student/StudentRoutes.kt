package com.example.pgk_forum_app.data.api.student

import com.example.pgk_forum_app.data.api.BaseRoutes

object StudentRoutes {
    const val groupNameQ = "groupName"
    val studentLoginQ = "studentLogin"

    private const val base = BaseRoutes.STUDENT_ROUTE

    const val ALL = base + "all"
    const val REGISTER = base + "register"
    const val CHANGE_GROUP = base + "change-group"
    const val INFO = base + "info"
    const val ID = base
    const val CHANGE_GROUP_BY_STUDENT = base + "change-group-by-student"
    const val SET_GROUPNAME_NULL = base + "set-groupname-null"
}