package com.example.pgk_forum_app.data.api.test

import com.example.pgk_forum_app.data.api.BaseRoutes

object TestRoutes {

    val testIdQ = "testId"
    val themeNameQ = "themeName"
    val themeTitleQ = "themeTitle"
    val teacherIdQ = "teacherId"

    const val base = BaseRoutes.TEST_ROUTE

    const val GET_TEACHER_TESTS = base + "get-teacher-tests"
    const val CREATE = base + "create"
    const val DELETE = base + "delete"
    const val CHANGE_THEME = base + "change-theme"
    const val CHANGE_TITLE = base + "change-title"
    const val GET_TESTS_BY_TEACHER_ID = base + "get-tests-by-teacher-id"

}