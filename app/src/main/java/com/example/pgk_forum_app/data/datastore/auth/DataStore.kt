package com.example.pgk_forum_app.data.datastore.auth

import android.content.Context
import androidx.datastore.preferences.preferencesDataStore
import com.example.pgk_forum_app.data.datastore.DataStoreKeys.USER_DATASTORE

val Context.userDataStore by preferencesDataStore(name = USER_DATASTORE)