package com.example.pgk_forum_app.data.api.group

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.api.teacher.TeacherRoutes
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class GroupTeacherRepository(
    private val client:HttpClient,
    private val tokenRepository: TokenRepository
) {

    suspend fun getTeacherGroups(): RequestStatus<List<GroupInfo>> =
        catchResponseException {
            client.get(TeacherRoutes.GET_GROUPS){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }
}