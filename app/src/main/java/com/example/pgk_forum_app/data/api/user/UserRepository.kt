package com.example.pgk_forum_app.data.api.user

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.domain.model.login.LoginData
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.login.TokenBody
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*

class UserRepository(
    private val tokenRepository: TokenRepository,
    private val client:HttpClient
) {

    suspend fun login(login:String,password:String): RequestStatus<TokenBody> =
        catchResponseException{
            val response = client.post(UserRoutes.LOGIN){ setBody(LoginData(login, password)) }
            if (response.status.isSuccess()){
                val token = response.body<TokenBody>().token
                tokenRepository.postToken(token)
            }
            response.toRequestStatus()
        }


}