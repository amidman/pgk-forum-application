package com.example.pgk_forum_app.data.api.group

import com.example.pgk_forum_app.data.api.catchResponseException
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.data.datastore.auth.setHeader
import com.example.pgk_forum_app.domain.model.group.GroupDTO
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class GroupRepository(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository,
) {


    suspend fun getAllGroups():RequestStatus<List<GroupInfo>> =
        catchResponseException{
            client.get(GroupRoutes.ALL){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

    suspend fun createGroup(groupDTO: GroupDTO):RequestStatus<Int> =
        catchResponseException{
            client.post(GroupRoutes.CREATE){
                setHeader(tokenRepository.getToken())
                setBody(groupDTO)
            }.toRequestStatus()
        }

    suspend fun deleteGroup(groupId:Int):RequestStatus<String?> =
        catchResponseException{
            client.delete(GroupRoutes.ID+groupId.toString()){
                setHeader(tokenRepository.getToken())
            }.toRequestStatus()
        }

}