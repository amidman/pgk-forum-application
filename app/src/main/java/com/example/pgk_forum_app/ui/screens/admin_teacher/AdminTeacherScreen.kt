package com.example.pgk_forum_app.ui.screens.admin_teacher

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.pgk_forum_app.ui.dialogs.ConfirmWindow
import com.example.pgk_forum_app.ui.screens.admin_groups.screen_components.CreateFloatingButton
import com.example.pgk_forum_app.ui.util.BaseScreenState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfoWithGroupList
import com.example.pgk_forum_app.ui.common.EditorList
import com.example.pgk_forum_app.ui.dialogs.TeacherAddWindow
import com.example.pgk_forum_app.ui.navigation.admin.AdminBottomGraphs
import com.example.pgk_forum_app.ui.navigation.admin_teacher.AdminTeacherNavGraphs
import com.example.pgk_forum_app.ui.screens.admin_groups.GroupAdminViewModel
import com.example.pgk_forum_app.ui.screens.admin_teacher.screen_components.SelectedTeacherWithGroup
import com.example.pgk_forum_app.ui.screens.admin_teacher.screen_components.TeacherListWithGroup
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.annotation.Destination
import kotlinx.coroutines.launch


@OptIn(ExperimentalMaterial3Api::class)
@AdminBottomGraphs
@Destination
@Composable
fun AdminTeacherScreen(
    teacherInfoWithGroupList:List<TeacherInfoWithGroupList>,
    vm:TeacherAdminViewModel,
    groupInfoList:List<GroupInfo>
) {
    val adminTeacherNavController = rememberNavController()
    val currentDestination = adminTeacherNavController.currentBackStackEntryAsState().value?.destination?.route
    var selectedTeacherInfo: TeacherInfoWithGroupList? by remember { mutableStateOf(null) }
    var isCreateDialogEnabled by remember { mutableStateOf(false) }
    var isDeleteDialogEnabled by remember { mutableStateOf(false) }
    var isError by remember { mutableStateOf(false) }
    var errorMessage: String? by remember { mutableStateOf(null) }
    var isLoading:Boolean by remember { mutableStateOf(false) }

    collectFlows(onCollect = {
        launch{ vm.selectedTeacherWithGroup.collect { selectedTeacherInfo = it } }
        launch{ vm.isCreateDialogEnabled.collect { isCreateDialogEnabled = it } }
        launch{ vm.isDeleteDialogEnabled.collect { isDeleteDialogEnabled = it } }
        launch{
            vm.state.collect {
                isError = it is BaseScreenState.Error
                errorMessage = it.error
                isLoading = it is BaseScreenState.Loading
            }
        }
    })


    val listDestination = AdminTeacherNavGraphs.TEACHERLIST.name
    val selectedDestination = AdminTeacherNavGraphs.SELECTED.name


    Scaffold(
        topBar = {},
        floatingActionButton = { if (currentDestination == listDestination) CreateFloatingButton(onClick = { vm.setCreateDialog(true) }) }
    ) {
        NavHost(navController = adminTeacherNavController, startDestination = listDestination) {
            composable(listDestination) {
                EditorList(
                    entityList = teacherInfoWithGroupList,
                    titleString = {value ->
                        value.login
                    },
                    onClick = {
                        vm.selectTeacher(it); adminTeacherNavController.navigate(
                        selectedDestination
                    )
                    },
                    onDelete = {
                        vm.setDeletedTeacher(it.teacherId)
                        vm.setDeleteDialog(true)
                    },
                    isLoading = isLoading
                )
            }
            composable(selectedDestination) {
                selectedTeacherInfo?.let { teacherInfoWithGroup ->
                    val userId = teacherInfoWithGroup.userId ?: 0
                    SelectedTeacherWithGroup(
                        teacherInfo = teacherInfoWithGroup,
                        changeFio = {fio -> vm.changeUserFio(userId, newFio = fio)},
                        changeLogin = {login -> vm.changeUserLogin(userId, newLogin = login)},
                        changePassword = {password -> vm.changeUserPassword(userId, newPassword = password)},
                        addGroup = {groupName -> vm.addGroupToTeacher(groupName)},
                        deleteGroup = {groupDTO -> vm.deleteGroupOfTeacher(groupDTO.id ?: id)},
                        isError = isError,
                        errorMessage = errorMessage,
                        onBack = { vm.backFromSelected();adminTeacherNavController.navigate(listDestination) },
                        vm = vm,
                        groupList = groupInfoList,
                    )
                }
            }
        }
    }

    if (isCreateDialogEnabled)
        TeacherAddWindow(
            onDismiss = { vm.setCreateDialog(false) },
            onAdd = { vm.createTeacher(it) },
            isError = isError,
            errorMessage = errorMessage
        )
    if (isDeleteDialogEnabled)
        ConfirmWindow(
            isError = isError,
            errorMessage = errorMessage,
            onCancel = { vm.setDeleteDialog(false) },
            onConfirm = { vm.deleteTeacher() }
        )

}