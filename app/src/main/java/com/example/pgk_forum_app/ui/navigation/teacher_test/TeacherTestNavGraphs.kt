package com.example.pgk_forum_app.ui.navigation.teacher_test

enum class TeacherTestNavGraphs {
    TESTLIST,SELECTEDTEST,CREATETEST
}