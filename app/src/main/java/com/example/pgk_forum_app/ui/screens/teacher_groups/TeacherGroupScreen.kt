package com.example.pgk_forum_app.ui.screens.teacher_groups

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.ui.screens.teacher_groups.screen_components.TeacherGroupList
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.ui.navigation.teacher.TeacherBottomGraphs
import com.example.pgk_forum_app.ui.navigation.teacher_group.TeacherGroupNavGraphs
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.annotation.Destination
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@TeacherBottomGraphs(start = true)
@Destination
@Composable
fun TeacherGroupScreen(
    groupInfoList:List<GroupInfo>,
) {
    val teacherGroupNavController = rememberNavController()
    val currentDestination = teacherGroupNavController.currentBackStackEntryAsState().value?.destination?.route

    collectFlows(onCollect = {

    })

    val listDestination = TeacherGroupNavGraphs.GROUPLIST.name
    val selectedGroupDestination = TeacherGroupNavGraphs.SELECTED.name

    NavHost(navController = teacherGroupNavController, startDestination = listDestination){
        composable(listDestination){
            TeacherGroupList(groupList = groupInfoList, onClick = {}, isLoading = false)
        }
        composable(selectedGroupDestination){

        }
    }
}