package com.example.pgk_forum_app.ui.screens.student_test

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.student_answers.StudentAnswerStudentRepository
import com.example.pgk_forum_app.domain.model.student_answer.QuestionAnswer
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.domain.model.test.Question
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.time.LocalDateTime

class CreateStudentAnswerViewModel() : ViewModel() {


    private val _currentTestQuestionIndex = MutableStateFlow(0)
    val currentTestQuestionIndex = _currentTestQuestionIndex.asStateFlow()

    private val _currentStudentAnswer = MutableStateFlow("")
    val currentStudentAnswer = _currentStudentAnswer.asStateFlow()


    fun setTestInfo(testInfo: TestInfo) = viewModelScope.launch {
        val questionList = testInfo.questions
        val questionAnswerList = questionList.map { question ->
            question.mapToQuestionAnswer()
        }
        _questionAnswerListFlow.emit(questionAnswerList)
        _currentTestQuestionIndex.emit(0)
    }

    private val _questionAnswerListFlow = MutableStateFlow<List<QuestionAnswer>>(listOf())
    val questionAnswerListFlow = _questionAnswerListFlow.asStateFlow()


    private suspend fun setStudentAnswer(index:Int){
        val studentAnswer = questionAnswerListFlow.value[index].studentAnswer
        _currentStudentAnswer.emit(studentAnswer)
    }


    fun configureStudentAnswer(testInfo:TestInfo):StudentAnswer{
        val questionAnswerList = questionAnswerListFlow.value
        val answerDate = LocalDateTime.now().toString()
        val testId = testInfo.id ?: 0
        val testTitle = testInfo.title
        val maxScore = testInfo.maxScore
        val studentAnswer = StudentAnswer(
            studentId = 0,
            testId = testId,
            testTitle = testTitle,
            maxScore = maxScore,
            totalScore = 0,
            answerDate = answerDate,
            questions = questionAnswerList,
        )
        return studentAnswer
    }



    fun nextQuestion() = viewModelScope.launch {
        val index = currentTestQuestionIndex.value
        if (index != questionAnswerListFlow.value.lastIndex){
            _currentTestQuestionIndex.emit(index + 1)
            setStudentAnswer(index + 1)
        }
    }

    fun prevQuestion() = viewModelScope.launch {
        val index = currentTestQuestionIndex.value
        if (index != 0) {
            _currentTestQuestionIndex.emit(index - 1)
            setStudentAnswer(index - 1)
        }
    }

    fun changeAnswer(studentAnswer: String) = viewModelScope.launch{
        val index = currentTestQuestionIndex.value
        val questionAnswerList = questionAnswerListFlow.value
        questionAnswerList[index].studentAnswer = studentAnswer
        _questionAnswerListFlow.emit(questionAnswerList)
        _currentStudentAnswer.emit(studentAnswer)
    }


}
