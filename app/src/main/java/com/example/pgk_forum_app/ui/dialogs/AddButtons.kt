package com.example.pgk_forum_app.ui.dialogs

import androidx.compose.foundation.layout.*
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.pgk_forum_app.R

@Composable
fun BoxScope.AddButtons(
    onAdd:() -> Unit,
    onDismiss:() -> Unit,
    confirmButtonText:String = stringResource(id = R.string.entity_dialog_confirm_button)
) {
    val cancelText = stringResource(id = R.string.entity_dialog_cancel_button)
    Row(modifier = Modifier
        .align(Alignment.BottomCenter)
        .padding(20.dp), horizontalArrangement = Arrangement.Center) {
        OutlinedButton(onClick = onAdd) {
            Text(text = confirmButtonText)
        }
        Spacer(modifier = Modifier.width(20.dp))
        OutlinedButton(onClick = onDismiss) {
            Text(text = cancelText)
        }
    }
}