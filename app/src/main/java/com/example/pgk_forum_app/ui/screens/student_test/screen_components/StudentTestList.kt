package com.example.pgk_forum_app.ui.screens.student_test.screen_components

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.ui.common.EditorList
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.navigation.student_test.StudentTestGraphs
import com.example.pgk_forum_app.ui.screens.destinations.SelectedGroupDestination
import com.example.pgk_forum_app.ui.screens.destinations.SelectedStudentTestDestination
import com.example.pgk_forum_app.ui.screens.destinations.TestTeacherListDestination
import com.example.pgk_forum_app.ui.screens.student_test.CreateStudentAnswerViewModel
import com.example.pgk_forum_app.ui.screens.student_test.StudentTestViewModel
import com.example.pgk_forum_app.ui.screens.teacher_tests.CreateTestViewModel
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.result.ResultBackNavigator
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@StudentTestGraphs
@Destination
@Composable
fun StudentTestList(
    teacherId:Int,
    vm:StudentTestViewModel,
    createStudentAnswerViewModel: CreateStudentAnswerViewModel,
    resultBackNavigator: ResultBackNavigator<Boolean>,
    navigator: DestinationsNavigator
) {
    vm.getTeacherTests(teacherId)
    var testList:List<TestInfo> by remember { mutableStateOf(listOf())}
    val onBack = {
        navigator.navigate(
            TestTeacherListDestination
        )
    }
    BackHandler(true,onBack = onBack)
    var isLoading by remember { mutableStateOf(false) }
    collectFlows{
        launch {
            vm.teacherTestInfoListFlow.collect {
                testList = it
            }
        }
        launch{
            vm.state.collect {
                isLoading = it is BaseScreenState.Loading
            }
        }
    }


    Scaffold(topBar = {
        SelectedEntityTopBar(
            title = "Тесты",
            onBack = onBack
        )
    }){
        EditorList(
            modifier = Modifier.padding(top = it.calculateTopPadding()),
            entityList = testList,
            isLoading = isLoading,
            titleString = { testInfo ->
                testInfo.title
            },
            onClick = { test ->
                createStudentAnswerViewModel.setTestInfo(test)
                navigator.navigate(
                    SelectedStudentTestDestination(
                        testInfo = test
                    )
                )
            },
            onDelete = {},
            isEditMode = false,
        )
    }
}