package com.example.pgk_forum_app.ui.common

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.pgk_forum_app.ui.screens.teacher_theme.screen_components.EntityListItem


@Composable
fun <T> EditorList(
    modifier: Modifier = Modifier,
    entityList: List<T>,
    isLoading: Boolean,
    titleString: (value: T) -> String,
    onClick: (value: T) -> Unit,
    onDelete: (value: T) -> Unit = {},
    isEditMode: Boolean = true
) {

    EntityList(
        entityList = entityList,
        isLoading = isLoading,
        modifier = modifier
    ) { index, entity ->
        EntityListItem(
            onDelete = { onDelete(entity) },
            onClick = { onClick(entity) },
            titleString = titleString(entity),
            isEditMode = isEditMode,
        )
    }
    if (entityList.isEmpty())
        Box(Modifier.fillMaxSize()) {
            AmidTitle(titleString = "Пусто", modifier = Modifier.align(Alignment.Center))
        }
}