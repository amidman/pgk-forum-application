package com.example.pgk_forum_app.ui.screens.teacher_groups.screen_components

import androidx.compose.runtime.Composable
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.ui.common.EntityList
import com.example.pgk_forum_app.ui.screens.admin_groups.screen_components.GroupInfoItem

@Composable
fun TeacherGroupList(
    groupList: List<GroupInfo>,
    onClick: (groupInfo: GroupInfo) -> Unit,
    isLoading:Boolean
) {
    EntityList(entityList = groupList, isLoading = isLoading) { index, groupInfo ->
        GroupInfoItem(
            groupInfo = groupInfo,
            onClick = { onClick(groupInfo) },
            isEditMode = false,
        )

    }
}