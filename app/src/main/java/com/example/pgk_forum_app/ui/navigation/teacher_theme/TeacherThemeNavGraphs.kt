package com.example.pgk_forum_app.ui.navigation.teacher_theme

enum class TeacherThemeNavGraphs(val argument:String? = null) {
    THEMELIST,SELECTEDTHEME(argument = "themeId")
}