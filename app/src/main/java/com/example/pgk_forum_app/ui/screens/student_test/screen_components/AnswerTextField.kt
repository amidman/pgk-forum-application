package com.example.pgk_forum_app.ui.screens.student_test.screen_components

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AnswerTextField(
    modifier: Modifier,
    value: String,
    onValueChange: (value: String) -> Unit
) {
    val placeHolderValue = "Введите ответ на вопрос"
    val labelValue = "Ответ"
    OutlinedTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier,
        singleLine = false,
        label = {
            Text(labelValue)
        },
        placeholder = {
            Text(placeHolderValue)
        }
    )
}