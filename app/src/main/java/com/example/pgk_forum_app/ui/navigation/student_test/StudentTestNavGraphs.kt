package com.example.pgk_forum_app.ui.navigation.student_test

import com.ramcosta.composedestinations.annotation.NavGraph


@NavGraph
annotation class StudentTestGraphs(
    val start:Boolean = false
)

enum class StudentTestNavGraphs(val argument:String? = null) {
    TEACHERLIST,
    TESTLIST(argument = "teacherId"),
    SELECTEDTESTLIST(argument = "testId")
}