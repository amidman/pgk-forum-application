package com.example.pgk_forum_app.ui.screens.student_answer

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.*
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.ui.navigation.student.StudentBottomGraphs
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.destinations.StudentAnswerListDestination
import com.example.pgk_forum_app.ui.util.collectFlow
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.dependency
import kotlinx.coroutines.flow.onEach


@OptIn(ExperimentalMaterial3Api::class)
@StudentBottomGraphs
@Destination
@Composable
fun StudentAnswerScreen(
    vm: StudentAnswerViewModel
) {


    var studentAnswerList: List<StudentAnswer> by remember { mutableStateOf(listOf()) }

    vm.studentAnswerListFlow.onEach {
        studentAnswerList = it
    }.collectFlow()

    DestinationsNavHost(
        navGraph = NavGraphs.studentAnswerGraphs,
        dependenciesContainerBuilder = {
            dependency(vm)
            dependency(StudentAnswerListDestination) { studentAnswerList }
        }
    )

}