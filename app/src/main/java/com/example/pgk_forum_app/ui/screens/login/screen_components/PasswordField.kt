package com.example.pgk_forum_app.ui.screens.login.screen_components

import android.util.Log
import androidx.compose.material.icons.Icons
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.ui.screens.login.LoginViewModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PasswordField(
    modifier: Modifier,
    vm: LoginViewModel,
    isError:Boolean
) {

    val passwordHint = stringResource(id = R.string.password_hint)

    val password = vm.passwordFlow.collectAsState()

    var passwordVisibility by remember {
        mutableStateOf(false)
    }

    val iconResource = remember(passwordVisibility) {
        if (passwordVisibility) R.drawable.visibility_on else R.drawable.visibility_off
    }

    val visualTransformation = remember(passwordVisibility) {
        if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation()
    }

    OutlinedTextField(
        value = password.value,
        onValueChange = { newValue ->
            vm.changePassword(newValue)
        },
        modifier = modifier,
        isError = isError,
        label = {
            Text(text = passwordHint)
        },
        singleLine = true,
        trailingIcon = {
            IconButton(onClick = { passwordVisibility = !passwordVisibility }) {
                Icon(painter = painterResource(id = iconResource), contentDescription = null)
            }
        },
        visualTransformation = visualTransformation
    )

}


