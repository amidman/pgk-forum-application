package com.example.pgk_forum_app.ui.screens.admin_groups.screen_components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import com.example.pgk_forum_app.ui.common.ChangeParamField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.screens.admin_groups.GroupAdminViewModel
import com.example.pgk_forum_app.ui.screens.admin_students.screen_components.ChangeParamList
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlow
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.flow.onEach


@OptIn(ExperimentalMaterial3Api::class, ExperimentalPagerApi::class)
@Composable
fun GroupInfoSelectedItem(
    groupInfo: GroupInfo,
    changeName: (value: String) -> Unit,
    changeShortName: (value: String) -> Unit,
    addTeacher: (teacherLogin: String) -> Unit,
    deleteTeacher: (teacherInfo: TeacherInfo) -> Unit,
    addStudent: (studentLogin: String) -> Unit,
    deleteStudent: (studentInfo: StudentInfo) -> Unit,
    isError: Boolean,
    errorMessage: String?,
    onBack: () -> Unit,
    vm: GroupAdminViewModel,
    teacherList: List<TeacherInfo>,
    studentList: List<StudentInfo>,
) {
    val pagerState = rememberPagerState()
    val scrollState = rememberScrollState()
    val isName = remember { mutableStateOf(false) }
    val isShortName = remember { mutableStateOf(false) }
    val isTeacherAdd = remember { mutableStateOf(false) }
    val isStudentAdd = remember { mutableStateOf(false) }
    vm.state.onEach {
        if (it is BaseScreenState.Success) {
            isName.value = false
            isShortName.value = false
            isTeacherAdd.value = false
            isStudentAdd.value = false
        }
    }.collectFlow()

    Scaffold(topBar = {
        SelectedEntityTopBar(title = "Группа ${groupInfo.groupName}", onBack = onBack)
    }) {
        Column(
            Modifier
                .fillMaxSize()
                .padding(top = it.calculateTopPadding())
                .verticalScroll(scrollState)
        ) {
            ChangeParamField(
                paramValue = groupInfo.groupName,
                paramTitle = "Название",
                onClick = {},
                onChange = changeName,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isName
            )
            ChangeParamField(
                paramValue = groupInfo.groupShortName,
                paramTitle = "Короткое название",
                onClick = { },
                onChange = changeShortName,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isShortName
            )
            HorizontalPager(
                modifier = Modifier.height(350.dp),
                count = 2,
                state = pagerState
            ) {
                Column() {
                    when (it) {
                        0 -> ChangeParamList<TeacherInfo>(
                            paramTitle = "Учителя",
                            onDelete = deleteTeacher,
                            itemList = groupInfo.teachers,
                            itemListString = groupInfo.teachers.map { teacherInfo -> teacherInfo.login },
                            emptyItemTitle = "У группы отсутствуют учителя",
                            valueTitle = "Логин учителя",
                            onAdd = addTeacher,
                            isCreateDialogEnabled = isTeacherAdd,
                            isError = isError,
                            errorString = errorMessage,
                            allItemList = teacherList.map { teacherInfo -> teacherInfo.login }
                                .sorted()
                        )
                        1 -> ChangeParamList(
                            paramTitle = "Студенты",
                            onDelete = deleteStudent,
                            itemList = groupInfo.students,
                            itemListString = groupInfo.students.map { it.login },
                            emptyItemTitle = "У группы отсутствуют студенты",
                            valueTitle = "Логин студента",
                            onAdd = addStudent,
                            isCreateDialogEnabled = isStudentAdd,
                            isError = isError,
                            errorString = errorMessage,
                            allItemList = studentList.map { studentInfo -> studentInfo.login }
                                .sorted()
                        )
                    }
                }
            }
            HorizontalPagerIndicator(
                modifier = Modifier
                    .align(CenterHorizontally)
                    .padding(vertical = 10.dp),
                pagerState = pagerState,
                activeColor = MaterialTheme.colorScheme.primary
            )

        }


    }
}