package com.example.pgk_forum_app.ui.navigation.student_answer

import com.ramcosta.composedestinations.annotation.NavGraph


@NavGraph
annotation class StudentAnswerGraphs(
    val start:Boolean = false
)
