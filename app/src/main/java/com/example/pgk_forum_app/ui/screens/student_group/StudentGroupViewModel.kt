package com.example.pgk_forum_app.ui.screens.student_group

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.group.GroupStudentRepository
import com.example.pgk_forum_app.domain.model.group.GroupInfoWithTeachersGroup
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class StudentGroupViewModel(
    private val groupStudentRepository: GroupStudentRepository,
): ViewModel() {

    private val _groupInfoFlow:MutableStateFlow<GroupInfoWithTeachersGroup?> = MutableStateFlow(null)
    val groupInfoFlow = _groupInfoFlow.asStateFlow()

    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    init {
        getStudentGroup()
    }

    fun getStudentGroup() = viewModelScope.launch {
        val response = groupStudentRepository.getStudentGroup()
        if (response is RequestStatus.Success)
            _groupInfoFlow.emit(response.data)
        response.requestStatusHandle(_state)
    }


}