package com.example.pgk_forum_app.ui.screens.student_test.screen_components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.ui.common.AmidTitle
import com.example.pgk_forum_app.ui.theme.PurpleGrey80

@Composable
fun OptionList(
    itemModifier: Modifier = Modifier,
    selectedValue: String?,
    optionList: List<String>,
    onSelect: (value: String) -> Unit,
) {

    optionList.forEach{ optionValue ->
        OptionItem(
            modifier = itemModifier,
            selected = selectedValue == optionValue,
            value = optionValue,
            onClick = { onSelect(optionValue) }
        )
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OptionItem(
    modifier: Modifier = Modifier,
    selected: Boolean,
    value: String,
    onClick: () -> Unit
) {
    val containerColor = if (selected) MaterialTheme.colorScheme.primary else PurpleGrey80
    val textColor = if (selected) Color.White else MaterialTheme.colorScheme.primary

    Box(
        modifier = modifier
            .clip(RoundedCornerShape(100))
            .background(containerColor)
            .clickable { onClick() }
        ,
    ){
        AmidTitle(
            titleString = value,
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .padding(start = 20.dp)
                .align(Alignment.CenterStart),
            color = textColor,
            singleLine = false
        )
    }


}


@Composable
fun OptionNavigationButtons(
    modifier: Modifier,
    isLast:Boolean,
    isFirst:Boolean,
    onNext:() -> Unit,
    onPrev:() -> Unit,
) {
    val nextText = if (isLast) "Завершить" else "Следующий"
    Box(
        modifier = modifier,
    ) {
        if (!isFirst)
        OutlinedButton(
            onClick = onPrev,
            modifier = Modifier
                .align(Alignment.CenterStart)
                .padding(15.dp)
        ) {
            Text(text = "Предыдущий")
        }

        OutlinedButton(
            onClick = onNext,
            modifier = Modifier
                .align(Alignment.CenterEnd)
                .padding(15.dp)
        ) {
            Text(text = nextText)
        }

    }
}