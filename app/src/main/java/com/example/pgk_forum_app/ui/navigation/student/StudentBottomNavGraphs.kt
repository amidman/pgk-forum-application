package com.example.pgk_forum_app.ui.navigation.student

import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.ui.navigation.BaseBottomNavGraphs
import com.example.pgk_forum_app.ui.navigation.NavGraphData
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.destinations.StudentAnswerScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.StudentGroupScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.StudentTestScreenDestination
import com.ramcosta.composedestinations.annotation.NavGraph
import com.ramcosta.composedestinations.spec.NavGraphSpec

@NavGraph
annotation class StudentBottomGraphs(
    val start: Boolean = false
)

object StudentBottomNavGraphs : BaseBottomNavGraphs {

    const val GROUP_TITLE = "Группа"
    const val TEST_TITLE = "Тесты"
    const val ANSWER_TITLE = "Мои ответы"

    override val bottomGraphs: List<NavGraphData>
        get() = listOf(
            NavGraphData(
                title = GROUP_TITLE,
                icon = R.drawable.group,
                direction = StudentGroupScreenDestination
            ),
            NavGraphData(
                title = TEST_TITLE,
                icon = R.drawable.test,
                direction = StudentTestScreenDestination
            ),
            NavGraphData(
                title = ANSWER_TITLE,
                icon = R.drawable.answer_icon,
                direction = StudentAnswerScreenDestination
            ),
        )
    override val graphs: NavGraphSpec
        get() = NavGraphs.studentBottomGraphs
}