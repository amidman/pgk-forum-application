package com.example.pgk_forum_app.ui.screens.user

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.user.UserAdminRepository
import com.example.pgk_forum_app.data.api.user.UserRepository
import com.example.pgk_forum_app.ui.util.BaseScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class UserViewModel(
    private val userAdminRepository: UserAdminRepository,
    private val userRepository: UserRepository
): ViewModel() {
    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()


}