package com.example.pgk_forum_app.ui.common

import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp

@Composable
fun AmidTitle(
    titleString: String,
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colorScheme.primary,
    fontWeight: FontWeight = FontWeight.Bold,
    fontSize: TextUnit = 24.sp,
    textAlign: TextAlign = TextAlign.Start,
    overflow: TextOverflow = TextOverflow.Ellipsis,
    singleLine: Boolean = true,
) {
    val maxLines = if (singleLine) 1 else 1000
    Text(
        text = titleString,
        modifier = modifier,
        style = TextStyle(
            color = color,
            fontSize = fontSize,
            textAlign = textAlign,
            fontWeight = fontWeight,
            shadow = Shadow(),
        ),
        maxLines = maxLines,
        overflow = overflow,

        )
}

@Composable
fun PGKTextField(
    value: String,
    onValueChange: (value: String) -> Unit,
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colorScheme.primary,
    fontWeight: FontWeight = FontWeight.Bold,
    fontSize: TextUnit = 24.sp,
    textAlign: TextAlign = TextAlign.Start,
    singleLine: Boolean = true,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default
) {
    val maxLines = if (singleLine) 1 else 1000

    BasicTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier,
        textStyle = TextStyle (
            color = color,
            fontSize = fontSize,
            textAlign = textAlign,
            fontWeight = fontWeight,
            shadow = Shadow(),
        ),
        maxLines = maxLines,
        keyboardOptions = keyboardOptions
    )

}

