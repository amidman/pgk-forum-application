package com.example.pgk_forum_app.ui.screens.student_test.screen_components

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import com.example.pgk_forum_app.domain.model.student_answer.QuestionAnswer
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.ui.common.ChangeParamField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.navigation.student_test.StudentTestGraphs
import com.example.pgk_forum_app.ui.screens.destinations.StudentAnswerAfterTestDestination
import com.example.pgk_forum_app.ui.screens.destinations.StudentTestListDestination
import com.example.pgk_forum_app.ui.screens.student_answer.StudentAnswerViewModel
import com.example.pgk_forum_app.ui.screens.student_test.CreateStudentAnswerViewModel
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.result.ResultBackNavigator
import kotlinx.coroutines.launch


@OptIn(ExperimentalMaterial3Api::class)
@StudentTestGraphs
@Destination
@Composable
fun SelectedStudentTest(
    testInfo: TestInfo,
    vm: CreateStudentAnswerViewModel,
    studentAnswerVM: StudentAnswerViewModel,
    resultBackNavigator: ResultBackNavigator<Boolean>,
    navigator: DestinationsNavigator
) {

    val scope = rememberCoroutineScope()

    val onBack = {
        navigator.navigate(
            StudentTestListDestination(
                testInfo.teacherId
            )
        )
    }

    val scrollState = rememberScrollState()
    val themeTitle = "Тема теста"
    var questionAnswerList by remember {
        mutableStateOf(listOf<QuestionAnswer>())
    }
    var currentQuestionIndex by remember {
        mutableStateOf(0)
    }
    var currentStudentAnswer by remember {
        mutableStateOf("")
    }

    collectFlows {
        launch {
            vm.currentTestQuestionIndex.collect { index ->
                currentQuestionIndex = index
            }
        }

        launch {
            vm.questionAnswerListFlow.collect { questionList ->
                questionAnswerList = questionList
            }
        }
        launch {
            vm.currentStudentAnswer.collect{ studentAnswer ->
                currentStudentAnswer = studentAnswer
            }
        }
    }

    BackHandler(true, onBack = onBack)

    Scaffold(
        topBar = {
            SelectedEntityTopBar(
                title = testInfo.title,
                onBack = onBack
            )
        }
    ) {
        Column(
            modifier = Modifier
                .padding(it)
                .fillMaxSize()
                .verticalScroll(scrollState)
        ) {
            ChangeParamField(
                paramValue = testInfo.themeName,
                paramTitle = themeTitle,
                isEditMode = false,
            )
            QuestionItem(
                questionIndex = currentQuestionIndex,
                questionList = questionAnswerList,
                currentValue = currentStudentAnswer,
                onLast = {
                    scope.launch {
                        val studentAnswer = vm.configureStudentAnswer(testInfo)
                        val newStudentAnswer = studentAnswerVM.createStudentAnswer(studentAnswer)
                        newStudentAnswer?.let {
                            navigator.navigate(
                                StudentAnswerAfterTestDestination(
                                    studentAnswer = newStudentAnswer
                                )
                            )
                        }
                    }
                },
                vm = vm
            )
        }
    }
}