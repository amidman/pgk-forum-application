package com.example.pgk_forum_app.ui.common

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.ui.dialogs.ChangeParamWindow

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChangeParamField(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .height(70.dp)
        .padding(horizontal = 25.dp, vertical = 5.dp),
    paramValue: String, // current value
    paramTitle:String, // value of item title and hint in dialog field
    onClick:() -> Unit = {}, // call when we click on item
    onChange:(value:String) -> Unit = {}, // call when typing text in dialog field
    isDialogEnabled: MutableState<Boolean> = mutableStateOf(false), // if true dialog is show
    isError:Boolean = false, // isError param
    errorMessage:String? = null, // error message, show if isError enabled
    isEntityField:Boolean = false, // if value of field depends of any entity enable this parameter
    entityStringList:List<String> = listOf(), // list of available entity names which we can select if isEntity parameter enabled
    isEditMode:Boolean = true, // if true show edit icon which provide change and delete parameters
    dialogSingleLine:Boolean = true, // Change dialog field is single line
    dialogFieldModifier: Modifier? = null, // modifier of dialog field
    fieldContainerModifier:Modifier = Modifier.fillMaxSize() // modifier of box in card
) {

    AmidTitle(
        titleString = paramTitle,
        modifier = Modifier.padding(start = 25.dp, top = 10.dp),
        fontSize = 20.sp
    )
    Card(
        modifier = modifier,
        onClick = onClick,
    ) {
        Box(modifier = fieldContainerModifier){
            AmidTitle(
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .padding(start = 40.dp, top = 10.dp, bottom = 10.dp)
                    .width(240.dp),
                titleString = paramValue,
                singleLine = dialogSingleLine,
                fontSize = 20.sp,
            )
            if (isEditMode)
            ChangeIcon(
                onChange = {isDialogEnabled.value = true},
                modifier = Modifier
                    .padding(end = 20.dp)
                    .align(Alignment.CenterEnd)
            )
        }
    }

    if (isDialogEnabled.value && isEditMode)
        ChangeParamWindow(
            onDismiss = { isDialogEnabled.value = false },
            onAdd = onChange,
            isError = isError,
            errorMessage = errorMessage,
            paramTitle = paramTitle,
            paramValue = paramValue,
            isEntityField = isEntityField,
            entityStringList = entityStringList,
            singleLine = dialogSingleLine,
            dialogFieldModifier = dialogFieldModifier,
        )
}


