package com.example.pgk_forum_app.ui.screens.admin_students.screen_components

import androidx.compose.runtime.Composable
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.ui.common.EntityList

@Composable
fun StudentList(
    studentInfoList: List<StudentInfo>,
    onClick: (studentInfo: StudentInfo) -> Unit,
    onDelete: (studentInfo: StudentInfo) -> Unit,
    isLoading:Boolean
) {

    EntityList(entityList = studentInfoList, isLoading = isLoading) { index, studentInfo->
        StudentInfoItem(
            studentInfo = studentInfo,
            onClick = { onClick(studentInfo) },
            onDelete = { onDelete(studentInfo) },
        )
    }

}




