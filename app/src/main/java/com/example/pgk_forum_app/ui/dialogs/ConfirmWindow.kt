package com.example.pgk_forum_app.ui.dialogs

import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.ui.common.AmidTitle
import com.example.pgk_forum_app.ui.screens.login.screen_components.MiniTitle


@Composable
fun ConfirmWindow(
    confirmTitle:String = stringResource(id = R.string.entity_delete_dialog_title),
    confirmMessage:String = stringResource(id = R.string.entity_delete_dialog_message),
    isError:Boolean,
    errorMessage:String?,
    onConfirm:()->Unit,
    onCancel:()->Unit
) {

    val confirmButtonText = stringResource(id = R.string.entity_confirm_dialog_onconfirm)
    val cancelButtonText = stringResource(id = R.string.entity_confirm_dialog_oncancel)
    val primaryColor = MaterialTheme.colorScheme.primary

    AlertDialog(
        onDismissRequest = {},
        title = {
            AmidTitle(titleString = confirmTitle, modifier = Modifier)
        },
        buttons = {
            Column(
                Modifier
                    .height(180.dp)
                    .width(340.dp)) {
                Spacer(modifier = Modifier.height(20.dp))
                MiniTitle(titleString = confirmMessage, modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(horizontal = 5.dp), fontSize = 16.sp)
                Spacer(modifier = Modifier.height(10.dp))
                DialogErrorTitle(
                    errorMessage = errorMessage,
                    isError = isError,
                    modifier = Modifier
                        .height(30.dp)
                        .align(Alignment.CenterHorizontally)
                )
                Spacer(modifier = Modifier.height(10.dp))
                Row(
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally),
                    horizontalArrangement = Arrangement.Center
                ) {
                    OutlinedButton(onClick = { onConfirm() }) {
                        Text(text = confirmButtonText,color = primaryColor)
                    }
                    Spacer(modifier = Modifier.width(30.dp))
                    OutlinedButton(onClick = onCancel) {
                        Text(text = cancelButtonText, color = primaryColor)
                    }
                }
            }
        }
    )
}