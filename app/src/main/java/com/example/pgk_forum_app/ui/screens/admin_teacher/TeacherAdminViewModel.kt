package com.example.pgk_forum_app.ui.screens.admin_teacher

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.group.GroupAdminRepository
import com.example.pgk_forum_app.data.api.teacher.TeacherRepository
import com.example.pgk_forum_app.data.api.user.UserAdminRepository
import com.example.pgk_forum_app.domain.model.group.GroupDTO
import com.example.pgk_forum_app.domain.model.group.TeacherInfoWithGroupDTO
import com.example.pgk_forum_app.domain.model.network_base.ErrorModel
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfoWithGroupList
import com.example.pgk_forum_app.domain.model.user.UserDTO
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.ViewModelHandle
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class TeacherAdminViewModel(
    private val teacherRepository: TeacherRepository,
    private val userAdminRepository: UserAdminRepository,
    private val groupAdminRepository: GroupAdminRepository,
):ViewModel() {

    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _teacherInfoList = MutableStateFlow<List<TeacherInfoWithGroupList>>(listOf())
    val teacherInfoList = _teacherInfoList.asStateFlow()

    private val _selectedTeacherWithGroup = MutableStateFlow<TeacherInfoWithGroupList?>(null)
    val selectedTeacherWithGroup = _selectedTeacherWithGroup.asStateFlow()

    private val _deleteEntityId = MutableStateFlow<Int?>(null)
    val deleteEntityId = _deleteEntityId.asStateFlow()

    private val _isDeleteDialogEnabled = MutableStateFlow<Boolean>(false)
    val isDeleteDialogEnabled = _isDeleteDialogEnabled.asStateFlow()

    private val _isCreateDialogEnabled = MutableStateFlow<Boolean>(false)
    val isCreateDialogEnabled = _isCreateDialogEnabled.asStateFlow()

    fun setDeleteDialog(value:Boolean) = viewModelScope.launch { _isDeleteDialogEnabled.emit(value) }

    fun setCreateDialog(value:Boolean) = viewModelScope.launch { _isCreateDialogEnabled.emit(value) }

    init { getAllTeachers() }

    private suspend fun loading(){_state.emit(BaseScreenState.Loading)}

    fun getAllTeachers() = viewModelScope.launch {
        loading()
        val allTeachers = teacherRepository.getAllTeacherWithGroups()
        allTeachers.requestStatusHandle(_state,_teacherInfoList)
    }

    fun selectTeacher(teacherInfoWithGroupList: TeacherInfoWithGroupList) = viewModelScope.launch {
        _selectedTeacherWithGroup.emit(teacherInfoWithGroupList)
    }

    fun backFromSelected() = viewModelScope.launch {
        _selectedTeacherWithGroup.emit(null)
    }

    fun setDeletedTeacher(teacherId: Int) = viewModelScope.launch { _deleteEntityId.emit(teacherId) }

    fun deleteTeacher() = viewModelScope.launch {
        loading()
        val id = deleteEntityId.value ?: return@launch
        val response = teacherRepository.deleteTeacher(id)
        val viewModelHandle = object: ViewModelHandle<String?> {
            override suspend fun onSuccess(data: String?) {
                val listValue = teacherInfoList.value.toMutableList()
                val teacherInfo = listValue.find { it.teacherId == id }
                listValue.remove(teacherInfo)
                _teacherInfoList.emit(listValue)
                _isDeleteDialogEnabled.emit(false)
            }
            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state,viewModelHandle)
        _deleteEntityId.emit(null)
    }

    fun createTeacher(userDTO: UserDTO) = viewModelScope.launch {
        loading()
        val response = teacherRepository.createTeacher(userDTO)
        val viewModelHandle = object:ViewModelHandle<TeacherInfo>{
            override suspend fun onSuccess(data: TeacherInfo) {
                val listValue = teacherInfoList.value.toMutableList()
                listValue.add(TeacherInfoWithGroupList.fromParts(teacherInfo = data , listOf()))
                _teacherInfoList.emit(listValue)
                _isCreateDialogEnabled.emit(false)
            }
            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state,viewModelHandle)
    }

    private suspend fun updateTeacherInList(newTeacherInfo: TeacherInfoWithGroupList?){
        newTeacherInfo ?: return
        val teacherList = teacherInfoList.value.toMutableList()
        val indexOfStudent = teacherList.indexOfFirst { newTeacherInfo.teacherId == it.teacherId }
        teacherList[indexOfStudent] = newTeacherInfo
        _selectedTeacherWithGroup.emit(newTeacherInfo)
        _teacherInfoList.emit(teacherList)
    }

    private suspend fun updateTeacherGroupList(groupList: List<GroupDTO>?){
        selectedTeacherWithGroup.value ?: return
        groupList ?: return
        loading()
        val teacherInfo = selectedTeacherWithGroup.value?.copy(groups = groupList)
        updateTeacherInList(teacherInfo)
    }

    fun changeUserLogin(userId:Int,newLogin:String) = viewModelScope.launch {
        loading()
        val response = userAdminRepository.changeLogin(userId, newLogin)
        if (response is RequestStatus.Success)
            updateTeacherInList(selectedTeacherWithGroup.value?.copy(login = newLogin))
        response.requestStatusHandle(_state)
    }

    fun changeUserFio(userId: Int,newFio:String) = viewModelScope.launch {
        val response = userAdminRepository.changeFio(userId, newFio)
        if (response is RequestStatus.Success)
            updateTeacherInList(selectedTeacherWithGroup.value?.copy(fio = newFio))
        response.requestStatusHandle(_state)
    }

    fun changeUserPassword(userId: Int,newPassword:String) = viewModelScope.launch {
        loading()
        val response = userAdminRepository.changePassword(userId, newPassword)
        response.requestStatusHandle(_state)
    }



    fun addGroupToTeacher(groupName:String) = viewModelScope.launch {
        val teacherLogin = selectedTeacherWithGroup.value?.login ?: return@launch
        loading()
        val response = groupAdminRepository.addGroupToTeacher(teacherLogin = teacherLogin,groupName)
        val viewModelHandle = object :ViewModelHandle<TeacherInfoWithGroupDTO>{
            override suspend fun onSuccess(data: TeacherInfoWithGroupDTO) {
                val groupList = selectedTeacherWithGroup.value?.groups ?: return
                updateTeacherGroupList(groupList + data.groupDTO)
            }
            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state,viewModelHandle)
    }

    fun deleteGroupOfTeacher(groupId:Int) = viewModelScope.launch {
        val teacherLogin = selectedTeacherWithGroup.value?.login ?: return@launch
        loading()
        val response = groupAdminRepository.deleteGroupTeacher(teacherLogin, groupId)
        val viewModelHandle = object :ViewModelHandle<String?>{
            override suspend fun onSuccess(data: String?) {
                val groupList = selectedTeacherWithGroup.value?.groups?.toMutableList()
                val groupDTO = groupList?.find { groupDTO -> groupDTO.id == groupId }
                groupList?.remove(groupDTO)
                updateTeacherGroupList(groupList)
            }
            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state,viewModelHandle)
    }



}