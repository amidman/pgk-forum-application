package com.example.pgk_forum_app.ui.screens.teacher_tests

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.domain.model.test.Question
import com.example.pgk_forum_app.domain.model.test.QuestionType
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CreateTestViewModel(): ViewModel() {

    private val _createTestTitle = MutableStateFlow("")
    val createTestTitle = _createTestTitle.asStateFlow()

    fun changeCreateThemeName(value:String) = viewModelScope.launch { _createTestTitle.emit(value) }

    private val _createThemeName = MutableStateFlow("")
    val createThemeName = _createThemeName.asStateFlow()

    fun changeCreateThemeDescription(value:String) = viewModelScope.launch { _createThemeName.emit(value) }

    private val _createQuestionList = MutableStateFlow<List<Question>>(listOf())
    val createQuestionList = _createQuestionList.asStateFlow()

    private fun updateQuestionInList(
        questionIndex: Int,
        updateState:(question: Question) -> Question
    ) = viewModelScope.launch {
        val questionList = createQuestionList.value.toMutableList()
        val question = questionList[questionIndex]
        val changedQuestion = updateState(question)
        questionList[questionIndex] = changedQuestion
        _createQuestionList.emit(questionList)
    }

    fun addOption(questionIndex:Int) =
        updateQuestionInList(
            questionIndex = questionIndex,
            updateState = {question ->
                val options = question.options
                question.copy(options = options + "ВАРИАНТ ${options.size + 1}")
            }
        )
    fun deleteOption(optionIndex: Int,questionIndex: Int) =
        updateQuestionInList(
            questionIndex = questionIndex,
            updateState = {question ->
                val options = question.options.toMutableList()
                options.removeAt(optionIndex)
                question.copy(options = options)
            }
        )
    fun changeOption(newOptionValue:String,optionIndex:Int,questionIndex: Int) =
        updateQuestionInList(
            questionIndex = questionIndex,
            updateState = {question ->
                val options = question.options.toMutableList()
                options[optionIndex] = newOptionValue
                question.copy(options = options)
            }
        )
    fun changeQuestionTitle(questionTitle:String, questionIndex: Int) =
        updateQuestionInList(
            questionIndex = questionIndex,
            updateState = {question ->
                question.copy(questionTitle = questionTitle)
            }
        )

    fun changeQuestionRightAnswer(rightAnswer:String,questionIndex: Int) =
        updateQuestionInList(
            questionIndex = questionIndex,
            updateState = {question ->
                question.copy(rightAnswer = rightAnswer)
            }
        )

    fun changeQuestionType(type:QuestionType,questionIndex: Int) =
        updateQuestionInList(
            questionIndex = questionIndex,
            updateState = { question ->
                val newQuestion =
                    if (type == QuestionType.TEXT)
                        question.copy(type = type.name, options = listOf())
                    else
                        question.copy(type = type.name, options = listOf("ВАРИАНТ 1"))
                newQuestion
            }
        )

    fun changeQuestionScore(score:Int,questionIndex: Int) =
        updateQuestionInList(
            questionIndex = questionIndex,
            updateState = {question ->
                question.copy(score = score)
            }
        )

    fun addNewQuestion() = viewModelScope.launch {
        val number = createQuestionList.value.size + 1
        val question = Question(
            id = null,
            number = 0,
            testId = 0,
            rightAnswer = "ВВЕДИТЕ ПРАВИЛЬНЫЙ ОТВЕТ",
            type = QuestionType.ONE_OPTION.name,
            score = 0,
            options = listOf(
                "ВАРИАНТ 1"
            ),
            questionTitle = "ВОПРОС $number"
        )
        _createQuestionList.emit(createQuestionList.value + question)
    }

    fun deleteQuestion(questionIndex: Int) = viewModelScope.launch {
        val currentListValue = createQuestionList.value.toMutableList()
        currentListValue.removeAt(questionIndex)
        _createQuestionList.emit(currentListValue)
    }


    fun clearTest() = viewModelScope.launch {
        _createTestTitle.emit("")
        _createThemeName.emit("")
        _createQuestionList.emit(listOf())
    }



}
