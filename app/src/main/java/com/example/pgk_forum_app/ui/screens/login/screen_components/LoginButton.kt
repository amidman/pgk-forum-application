package com.example.pgk_forum_app.ui.screens.login.screen_components

import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.ui.screens.login.LoginViewModel

@Composable
fun LoginButton(
    modifier: Modifier,
    vm: LoginViewModel
) {
    val buttonTitle = stringResource(id = R.string.login_button_title)

    OutlinedButton(
        onClick = {
            vm.login()
        },
        modifier = modifier,
    ) {
        Text(text = buttonTitle)
    }
}