package com.example.pgk_forum_app.ui.screens.teacher_tests

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.test.TestTeacherRepository
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.test.Question
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.ui.navigation.teacher_test.TeacherTestNavGraphs
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.ViewModelHandle
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import com.google.gson.Gson
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.time.LocalDateTime

class TeacherTestViewModel(
    private val testTeacherRepository: TestTeacherRepository
):ViewModel() {

    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _testInfoListFlow = MutableStateFlow<List<TestInfo>>(listOf())
    val testInfoListFlow = _testInfoListFlow.asStateFlow()

    private val _isDeleteDialogEnabled = MutableStateFlow<Boolean>(false)
    val isDeleteDialogEnabled = _isDeleteDialogEnabled.asStateFlow()

    fun setDeleteDialog(value:Boolean) = viewModelScope.launch { _isDeleteDialogEnabled.emit(value) }

    private val _createQuestionList = MutableStateFlow<List<Question>>(listOf())
    val createQuestionList = _createQuestionList.asStateFlow()

    private fun updateQuestionInList(
        questionIndex: Int,
        updateState:(question:Question) -> Question
    ) = viewModelScope.launch {
        val questionList = createQuestionList.value.toMutableList()
        val question = questionList[questionIndex]
        val changedQuestion = updateState(question)
        questionList[questionIndex] = changedQuestion
        _createQuestionList.emit(questionList)
    }

    fun addOption(optionValue:String,questionIndex:Int) =
        updateQuestionInList(
            questionIndex = questionIndex,
            updateState = {question ->
                val options = question.options
                question.copy(options = options + optionValue)
            }
        )
    fun deleteOption(optionValue:String,questionIndex: Int) =
        updateQuestionInList(
            questionIndex = questionIndex,
            updateState = {question ->
                val options = question.options - optionValue
                question.copy(options = options)
            }
        )



    private val _deleteEntityId = MutableStateFlow<Int?>(null)

    init {
        getTeacherTests()
    }

    private suspend fun updateTestInfoList(testInfoList: List<TestInfo>){
        _testInfoListFlow.emit(testInfoList)
    }

    fun getTeacherTests() = viewModelScope.launch {
        val testInfoList = testTeacherRepository.getTeacherTests()
        testInfoList.requestStatusHandle(_state,_testInfoListFlow)
    }

    fun createTest(
        testTitle:String,
        themeName:String,
        questions:List<Question>,
        onSuccess:() -> Unit,
    ) = viewModelScope.launch{
        val questionsWithNumber = questions.mapIndexed {index, question -> question.copy(number = index + 1) }
        val testInfo = TestInfo(
            teacherId = 0,
            title = testTitle,
            themeName = themeName,
            createdAt = LocalDateTime.now().toString(),
            questions = questionsWithNumber
        )
        val response = testTeacherRepository.createTestByTeacher(
            testInfo = testInfo
        )
        val viewModelHandle = object :ViewModelHandle<TestInfo> {
            override suspend fun onSuccess(data: TestInfo) {
                val currentList = testInfoListFlow.value
                _testInfoListFlow.emit(currentList + data)
                _state.emit(BaseScreenState.NavEvent(
                    TeacherTestNavGraphs.TESTLIST.name
                ))
                onSuccess()
            }
        }
        response.requestStatusHandle(_state,viewModelHandle)
    }


    fun selectDeleteEntity(testId:Int?) = viewModelScope.launch{
        testId ?: return@launch
        _deleteEntityId.emit(testId)
    }

    fun deleteTest() = viewModelScope.launch {
        val testId = _deleteEntityId.value ?: return@launch
        val response = testTeacherRepository.delete(testId)
        if (response is RequestStatus.Success){
            val currentTestList = testInfoListFlow.value
            val deletedTest = currentTestList.first { testInfo -> testInfo.id == testId }
            updateTestInfoList(currentTestList - deletedTest)
            _deleteEntityId.emit(null)
            _isDeleteDialogEnabled.emit(false)
        }
        response.requestStatusHandle(_state)
    }



}


