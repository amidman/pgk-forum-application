package com.example.pgk_forum_app.ui.dialogs

import androidx.compose.foundation.layout.Spacer
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.example.pgk_forum_app.R

@Composable
fun DialogErrorTitle(
    errorMessage:String?,
    isError:Boolean,
    modifier: Modifier = Modifier,
    style: TextStyle = TextStyle(
        textAlign = TextAlign.Center,
        fontWeight = FontWeight.Bold
    )
) {
    val baseErrorMessage = "Произошла ошибка"
    if(isError)
        Text(
            text = errorMessage?:baseErrorMessage,
            color = MaterialTheme.colorScheme.error,
            modifier = modifier,
            style = style
        )
    else
        Spacer(modifier = modifier)

}