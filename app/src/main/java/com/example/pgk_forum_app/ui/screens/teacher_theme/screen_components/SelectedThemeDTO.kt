package com.example.pgk_forum_app.ui.screens.teacher_theme.screen_components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.domain.model.theme.ThemeDTO
import com.example.pgk_forum_app.ui.common.ChangeParamField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.screens.admin_students.screen_components.ChangeParamList
import com.example.pgk_forum_app.ui.screens.teacher_theme.TeacherThemeViewModel
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlow
import kotlinx.coroutines.flow.onEach

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SelectedThemeDTO(
    themeDTO: ThemeDTO,
    changeName: (value: String) -> Unit,
    changeDescription: (value: String) -> Unit,
    isError: Boolean,
    errorMessage: String?,
    onBack: () -> Unit,
    vm: TeacherThemeViewModel,
    testInfoList: List<TestInfo>,
) {
    val filteredTestList =
        testInfoList.filter { testInfo -> testInfo.themeName == themeDTO.themeName }

    val scrollState = rememberScrollState()
    val isName = remember { mutableStateOf(false) }
    val isDescription = remember { mutableStateOf(false) }

    vm.state.onEach {
        if (it is BaseScreenState.Success) {
            isName.value = false
            isDescription.value = false
        }
    }.collectFlow()

    Scaffold(topBar = {
        SelectedEntityTopBar(title = "Тема ${themeDTO.themeName}", onBack = onBack)
    }) {
        Column(
            Modifier
                .padding(top = it.calculateTopPadding())
                .fillMaxSize()
                .verticalScroll(scrollState)
        ) {
            ChangeParamField(
                paramValue = themeDTO.themeName,
                paramTitle = "Название",
                onClick = {},
                onChange = changeName,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isName,
            )
            ChangeParamField(
                paramValue = themeDTO.description,
                paramTitle = "Описание",
                onClick = { },
                onChange = changeDescription,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isDescription,
                dialogSingleLine = false,
                dialogFieldModifier = Modifier
                    .width(350.dp)
                    .height(350.dp)
                    .padding(vertical = 6.dp, horizontal = 20.dp),
                modifier = Modifier
                    .fillMaxWidth()
                    .sizeIn(maxHeight = 350.dp)
                    .padding(horizontal = 25.dp, vertical = 5.dp),
                fieldContainerModifier = Modifier
                    .fillMaxWidth()
                    .sizeIn(maxHeight = 350.dp)
            )

            Column(modifier = Modifier.sizeIn(maxHeight = 350.dp, minHeight = 170.dp)) {
                ChangeParamList<TestInfo>(
                    paramTitle = "Тесты",
                    itemList = filteredTestList,
                    itemListString = filteredTestList.map { testInfo -> testInfo.title },
                    emptyItemTitle = "По данной теме тестов не найдено",
                    isError = isError,
                    errorString = errorMessage,
                    isEditMode = false,

                    )
            }


        }
    }
}