package com.example.pgk_forum_app.ui.screens.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.user.UserRepository
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.utils.ROLES
import com.example.pgk_forum_app.ui.navigation.main.MainNavGraphs
import com.example.pgk_forum_app.ui.screens.destinations.AdminScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.StudentScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.TeacherScreenDestination
import com.example.pgk_forum_app.ui.util.BaseScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class LoginViewModel(
    private val userRepository:UserRepository,
):ViewModel() {
    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _loginFlow = MutableStateFlow("gof")
    val loginFlow = _loginFlow.asStateFlow()

    private val _passwordFlow = MutableStateFlow("gof")
    val passwordFlow = _passwordFlow.asStateFlow()

    private suspend fun loading(){ _state.emit(BaseScreenState.Loading) }

    fun changeLogin(newValue:String) {
        viewModelScope.launch {
            _loginFlow.emit(newValue)
            _state.emit(BaseScreenState.Nothing)
        }
    }

    fun changePassword(newValue: String){
        viewModelScope.launch {
            _passwordFlow.emit(newValue)
            _state.emit(BaseScreenState.Nothing)
        }
    }

    private suspend fun roleHandler(stringRole:String?){
        val role = ROLES.valueOf(stringRole?:ROLES.GUEST.name)
        when(role){
            ROLES.ADMIN -> _state.emit(BaseScreenState.NavEvent(AdminScreenDestination.route))
            ROLES.TEACHER -> _state.emit(BaseScreenState.NavEvent(TeacherScreenDestination.route))
            ROLES.STUDENT -> _state.emit(BaseScreenState.NavEvent(StudentScreenDestination.route))
            ROLES.GUEST -> {}
        }
    }


    fun login() = viewModelScope.launch{
        loading()
        val login = loginFlow.value
        val password = passwordFlow.value
        val tokenBody = userRepository.login(login, password)
        when(tokenBody){
            is RequestStatus.Error -> {_state.emit(BaseScreenState.Error(tokenBody.error?.description))}
            is RequestStatus.Success -> roleHandler(tokenBody.data?.role)
        }
    }

}