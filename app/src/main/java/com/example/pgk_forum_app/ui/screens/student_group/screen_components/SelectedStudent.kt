package com.example.pgk_forum_app.ui.screens.student_group.screen_components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.ui.common.ChangeParamField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.navigation.student_group.StudentGroupGraphs
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.result.ResultBackNavigator

@OptIn(ExperimentalMaterial3Api::class)
@StudentGroupGraphs
@Destination
@Composable
fun SelectedStudent(
    studentInfo: StudentInfo,
    backResultBackNavigator: ResultBackNavigator<Boolean>
) {

    Scaffold(topBar = {
        SelectedEntityTopBar(title = "Студент ${studentInfo.fio}", onBack = {
            backResultBackNavigator.navigateBack()
        })
    }) {
        Column(
            Modifier
                .fillMaxSize()
                .padding(top = it.calculateTopPadding())
        ) {
            ChangeParamField(
                paramValue = studentInfo.fio,
                paramTitle = "ФИО",
                isEditMode = false
            )
            ChangeParamField(
                paramValue = studentInfo.login,
                paramTitle = "Логин",
                isEditMode = false
            )
            ChangeParamField(
                paramValue = studentInfo.groupName ?: "Отсутствует",
                paramTitle = "Группа студента",
                isEditMode = false
            )
        }
    }
}