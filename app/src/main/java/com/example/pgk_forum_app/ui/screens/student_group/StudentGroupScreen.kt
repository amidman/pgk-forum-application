package com.example.pgk_forum_app.ui.screens.student_group

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.ui.navigation.student.StudentBottomGraphs
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.destinations.SelectedGroupDestination
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.dependency
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@StudentBottomGraphs(start = true)
@Destination
@Composable
fun StudentGroupScreen(
    vm: StudentGroupViewModel
) {

    Scaffold { padding ->
        DestinationsNavHost(
            navGraph = NavGraphs.studentGroupGraphs,
            modifier = Modifier.padding(padding),
            dependenciesContainerBuilder = {
                dependency(SelectedGroupDestination) { vm }
            }
        )
    }

}