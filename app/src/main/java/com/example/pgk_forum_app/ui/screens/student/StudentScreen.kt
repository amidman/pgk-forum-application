package com.example.pgk_forum_app.ui.screens.student

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.ui.common.EntityBottomNavigation
import com.example.pgk_forum_app.ui.navigation.student.StudentBottomNavGraphs
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.destinations.StudentAnswerScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.StudentGroupScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.StudentTestScreenDestination
import com.example.pgk_forum_app.ui.screens.student_answer.StudentAnswerScreen
import com.example.pgk_forum_app.ui.screens.student_answer.StudentAnswerViewModel
import com.example.pgk_forum_app.ui.screens.student_group.StudentGroupScreen
import com.example.pgk_forum_app.ui.screens.student_group.StudentGroupViewModel
import com.example.pgk_forum_app.ui.screens.student_test.CreateStudentAnswerViewModel
import com.example.pgk_forum_app.ui.screens.student_test.StudentTestScreen
import com.example.pgk_forum_app.ui.screens.student_test.StudentTestViewModel
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.manualcomposablecalls.composable
import com.ramcosta.composedestinations.navigation.dependency
import org.koin.androidx.compose.getViewModel


@RootNavGraph
@Destination
@Composable
fun StudentScreen() {
    val studentNavigation = rememberNavController()
    val currentDestination =
        studentNavigation.currentBackStackEntryAsState().value?.destination?.route

    val studentGroupVM = getViewModel<StudentGroupViewModel>()
    val createAnswerVM = getViewModel<CreateStudentAnswerViewModel>()
    val studentTestVM = getViewModel<StudentTestViewModel>()
    val studentAnswerVM = getViewModel<StudentAnswerViewModel>()

    BackHandler(true) {}
    Scaffold(
        bottomBar = {
            EntityBottomNavigation(
                navController = studentNavigation,
                navGraphs = StudentBottomNavGraphs
            )
        }
    ) {
        DestinationsNavHost(
            navGraph = NavGraphs.studentBottomGraphs,
            navController = studentNavigation,
            modifier = Modifier.padding(it)
        ){
            composable(StudentGroupScreenDestination) {
                StudentGroupScreen(
                    vm = studentGroupVM
                )
            }
            composable(StudentTestScreenDestination) {
                StudentTestScreen(
                    vm = studentTestVM,
                    createTestAnswerVM = createAnswerVM,
                    studentAnswerVM = studentAnswerVM
                )
            }
            composable(StudentAnswerScreenDestination) {
                StudentAnswerScreen(
                    vm = studentAnswerVM
                )
            }
        }

    }
}