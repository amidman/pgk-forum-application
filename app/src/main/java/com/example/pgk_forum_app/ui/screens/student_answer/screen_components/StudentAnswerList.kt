package com.example.pgk_forum_app.ui.screens.student_answer.screen_components

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.ui.common.EditorList
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.navigation.student_answer.StudentAnswerGraphs
import com.example.pgk_forum_app.ui.screens.destinations.SelectedStudentAnswerDestination
import com.example.pgk_forum_app.ui.screens.destinations.StudentAnswerScreenDestination
import com.example.pgk_forum_app.ui.screens.student_answer.StudentAnswerViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator


@OptIn(ExperimentalMaterial3Api::class)
@Destination
@StudentAnswerGraphs(start = true)
@Composable
fun StudentAnswerList(
    vm: StudentAnswerViewModel,
    studentAnswerList: List<StudentAnswer>,
    navigator: DestinationsNavigator
) {


    Scaffold(
        topBar = {
            SelectedEntityTopBar(title = "Мои ответы")
        }
    ) {
        EditorList(
            modifier = Modifier.padding(it),
            entityList = studentAnswerList,
            isLoading = false,
            titleString = { studentAnswer ->
                studentAnswer.testTitle
            },
            onClick = { studentAnswer ->
                navigator.navigate(SelectedStudentAnswerDestination(
                    studentAnswer = studentAnswer
                ))
            },
            isEditMode = false
        )
    }
}