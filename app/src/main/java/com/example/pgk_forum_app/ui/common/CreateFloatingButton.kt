package com.example.pgk_forum_app.ui.screens.admin_groups.screen_components

import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import com.example.pgk_forum_app.R

@Composable
fun CreateFloatingButton(
    onClick:() -> Unit
) {
    val plusIconPainter = painterResource(id = R.drawable.plus_icon)
    FloatingActionButton(onClick = onClick) {
        Icon(painter = plusIconPainter, contentDescription = null)
    }
}