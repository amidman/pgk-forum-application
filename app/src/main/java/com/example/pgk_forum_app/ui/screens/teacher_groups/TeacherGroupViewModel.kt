package com.example.pgk_forum_app.ui.screens.teacher_groups

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.group.GroupTeacherRepository
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class TeacherGroupViewModel(
    private val groupTeacherRepository:GroupTeacherRepository,
):ViewModel() {

    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _groupInfoListFlow = MutableStateFlow<List<GroupInfo>>(listOf())
    val groupInfoListFlow = _groupInfoListFlow.asStateFlow()

    init {
        getAllGroupInfo()
    }

    private suspend fun loading() {_state.emit(BaseScreenState.Loading)}

    fun getAllGroupInfo() = viewModelScope.launch{
        loading()
        val groupInfoList = groupTeacherRepository.getTeacherGroups()
        groupInfoList.requestStatusHandle(_state,_groupInfoListFlow)
    }

    fun getGroupAnswers(groupId:Int,testId:Int):Flow<StudentAnswer> = flow {
        loading()
    }



}