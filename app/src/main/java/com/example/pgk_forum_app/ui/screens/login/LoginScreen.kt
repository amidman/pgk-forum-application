package com.example.pgk_forum_app.ui.screens.login

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.ui.common.AmidTitle
import com.example.pgk_forum_app.ui.screens.login.screen_components.*
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlow
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.compose.getViewModel


@RootNavGraph(start = true)
@Destination
@Composable
fun LoginScreen(
    navigator:DestinationsNavigator
) {

    val title = stringResource(id = R.string.login_title)

    val vm:LoginViewModel = getViewModel()

    var isError by remember {
        mutableStateOf(false)
    }

    var isLoading by remember {
        mutableStateOf(false)
    }

    var errorMessage:String? by remember {
        mutableStateOf(null)
    }


    vm.state.onEach{ state ->
        isError = state is BaseScreenState.Error
        errorMessage = state.error
        isLoading = state is BaseScreenState.Loading
        if (state is BaseScreenState.NavEvent && !state.navDestination.isNullOrEmpty())
            navigator.navigate(state.navDestination)
    }.collectFlow()


    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colorScheme.background),
        horizontalAlignment = Alignment.CenterHorizontally,

    ) {
        AmidTitle(
            titleString = title,
            modifier = Modifier.padding(top = 30.dp)
        )

        ErrorTitle(
            isError = isError,
            errorMessage = errorMessage,
            modifier = Modifier.padding(top = 10.dp)
        )

        LoginField(
            modifier = Modifier
                .width(280.dp)
                .padding(top = 30.dp),
            vm = vm,
            isError = isError
        )

        PasswordField(
            modifier = Modifier
                .width(280.dp)
                .padding(top = 20.dp),
            vm = vm,
            isError = isError
        )

        LoginButton(
            modifier = Modifier
                .size(
                    width = 140.dp,
                    height = 100.dp
                )
                .padding(top = 40.dp),
            vm = vm
        )

        if (isLoading)
            CircularProgressIndicator(
                modifier = Modifier.padding(top = 20.dp)
            )

    }

}