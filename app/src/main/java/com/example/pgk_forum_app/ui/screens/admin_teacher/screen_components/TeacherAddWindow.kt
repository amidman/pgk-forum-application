package com.example.pgk_forum_app.ui.dialogs

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.material.AlertDialog
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.domain.model.user.UserDTO
import com.example.pgk_forum_app.ui.common.AmidTitle


@Composable
fun TeacherAddWindow(
    onDismiss:() -> Unit,
    onAdd:(teacher: UserDTO) -> Unit,
    isError:Boolean,
    errorMessage:String?,
    title:String = stringResource(id = R.string.entity_add_title),
) {

    val fioParamTitle = "ФИО"
    val loginParamTitle = "Логин"
    val passwordParamTitle = "Пароль"
    var fio:String by remember{
        mutableStateOf("")
    }
    var login:String by remember {
        mutableStateOf("")
    }
    var password:String by remember {
        mutableStateOf("")
    }

    AlertDialog(
        onDismissRequest = {},
        buttons = {
            Box(){
                Column() {
                    AmidTitle(
                        titleString = title,
                        modifier = Modifier.align(Alignment.CenterHorizontally),
                        fontSize = 18.sp,
                        fontWeight = FontWeight.Bold
                    )
                    DialogParamField(
                        title = fioParamTitle,
                        value = fio,
                        onValueChange = {fio = it}
                    )

                    DialogParamField(
                        title = loginParamTitle,
                        value = login,
                        onValueChange = {login = it}
                    )

                    DialogParamField(
                        title = passwordParamTitle,
                        value = password,
                        onValueChange = {password = it}
                    )

                    DialogErrorTitle(
                        errorMessage = errorMessage,
                        isError = isError,
                        modifier = Modifier
                            .height(120.dp)
                            .align(Alignment.CenterHorizontally)
                    )

                }
                AddButtons(
                    onAdd = {
                        onAdd(UserDTO(
                            login = login,
                            fio = fio,
                            password = password,
                            role = ""
                        ))
                    },
                    onDismiss = onDismiss
                )
            }
        },

        )
}