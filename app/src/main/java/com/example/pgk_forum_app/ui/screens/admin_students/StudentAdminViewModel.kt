package com.example.pgk_forum_app.ui.screens.admin_students

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.student.StudentAdminRepository
import com.example.pgk_forum_app.data.api.student.StudentRepository
import com.example.pgk_forum_app.data.api.user.UserAdminRepository
import com.example.pgk_forum_app.domain.model.network_base.ErrorModel
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.domain.model.student.StudentInfoForRegistration
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.ViewModelHandle
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class StudentAdminViewModel(
    private val studentRepository: StudentRepository,
    private val studentAdminRepository: StudentAdminRepository,
    private val userAdminRepository: UserAdminRepository,
) : ViewModel() {

    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _studentInfoList = MutableStateFlow<List<StudentInfo>>(listOf())
    val studentInfoList = _studentInfoList.asStateFlow()

    private val _deleteEntityId = MutableStateFlow<Int?>(null)
    val deleteEntityId = _deleteEntityId.asStateFlow()

    private val _selectedStudentInfo = MutableStateFlow<StudentInfo?>(null)
    val selectedStudentInfo = _selectedStudentInfo.asStateFlow()

    private val _isDeleteDialogEnabled = MutableStateFlow<Boolean>(false)
    val isDeleteDialogEnabled = _isDeleteDialogEnabled.asStateFlow()

    private val _isCreateDialogEnabled = MutableStateFlow<Boolean>(false)
    val isCreateDialogEnabled = _isCreateDialogEnabled.asStateFlow()

    fun setDeleteDialog(value:Boolean) = viewModelScope.launch { _isDeleteDialogEnabled.emit(value) }

    fun setCreateDialog(value:Boolean) = viewModelScope.launch { _isCreateDialogEnabled.emit(value) }

    init {
        getAllStudentInfo()
    }

    private suspend fun loading(){_state.emit(BaseScreenState.Loading)}

    fun getAllStudentInfo() = viewModelScope.launch {
        loading()
        val allStudents = studentRepository.getAllStudents()
        allStudents.requestStatusHandle(_state, _studentInfoList)
    }

    fun setDeleteStudent(studentId: Int) = viewModelScope.launch { _deleteEntityId.emit(studentId) }


    fun deleteStudent() = viewModelScope.launch {
        loading()
        val id = deleteEntityId.value ?: return@launch
        val response = studentAdminRepository.deleteStudent(id)
        val viewModelHandle = object : ViewModelHandle<String?> {
            override suspend fun onSuccess(data: String?) {
                val listValue = studentInfoList.value.toMutableList()
                val studentInfo = listValue.find { it.studentId == id }
                listValue.remove(studentInfo)
                _studentInfoList.emit(listValue)
                _isDeleteDialogEnabled.emit(false)
            }

            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state, viewModelHandle)
        _deleteEntityId.emit(null)
    }

    fun createStudent(studentInfoForRegistration: StudentInfoForRegistration) = viewModelScope.launch {
        loading()
        val response = studentRepository.createStudent(studentInfoForRegistration)
        val viewModelHandle = object:ViewModelHandle<StudentInfo>{
            override suspend fun onSuccess(data: StudentInfo) {
                val listValue = studentInfoList.value.toMutableList()
                listValue.add(data)
                _studentInfoList.emit(listValue)
                _isCreateDialogEnabled.emit(false)
            }
            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state,viewModelHandle)
    }

    fun selectStudent(studentInfo: StudentInfo) = viewModelScope.launch {
        _selectedStudentInfo.emit(studentInfo)
    }
    fun backFromSelect() = viewModelScope.launch {
        _selectedStudentInfo.emit(null)
    }

    fun changeGroup(studentLogin: String,newGroupName:String?) = viewModelScope.launch{
        loading()
        val response =
            if (newGroupName.isNullOrEmpty())
                studentAdminRepository.setStudentGroupNull(studentLogin)
            else
                studentAdminRepository.changeStudentGroup(studentLogin,newGroupName)
        if (response is RequestStatus.Success)
            updateStudentInList(selectedStudentInfo.value?.copy(groupName = newGroupName))
        response.requestStatusHandle(_state)
    }

    private suspend fun updateStudentInList(newStudentInfo: StudentInfo?){
        newStudentInfo ?: return
        val studentList = studentInfoList.value.toMutableList()
        val indexOfStudent = studentList.indexOfFirst { newStudentInfo.studentId == newStudentInfo.studentId }
        studentList[indexOfStudent] = newStudentInfo
        _selectedStudentInfo.emit(newStudentInfo)
        _studentInfoList.emit(studentList)
    }

    fun changeUserLogin(userId:Int,newLogin:String) = viewModelScope.launch {
        loading()
        val response = userAdminRepository.changeLogin(userId, newLogin)
        if (response is RequestStatus.Success)
            updateStudentInList(selectedStudentInfo.value?.copy(login = newLogin))
        response.requestStatusHandle(_state)
    }

    fun changeUserFio(userId: Int,newFio:String) = viewModelScope.launch {
        loading()
        val response = userAdminRepository.changeFio(userId, newFio)
        if (response is RequestStatus.Success)
            updateStudentInList(selectedStudentInfo.value?.copy(fio = newFio))
        response.requestStatusHandle(_state)
    }

    fun changeUserPassword(userId: Int,newPassword:String) = viewModelScope.launch {
        loading()
        val response = userAdminRepository.changePassword(userId, newPassword)
        response.requestStatusHandle(_state)
    }

}