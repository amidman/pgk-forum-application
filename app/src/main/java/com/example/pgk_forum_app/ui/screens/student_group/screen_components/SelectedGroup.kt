package com.example.pgk_forum_app.ui.screens.student_group.screen_components

import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.pgk_forum_app.domain.model.group.GroupInfoWithTeachersGroup
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.ui.common.ChangeParamField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.navigation.student_group.StudentGroupGraphs
import com.example.pgk_forum_app.ui.screens.admin_students.screen_components.ChangeParamList
import com.example.pgk_forum_app.ui.screens.destinations.SelectedStudentDestination
import com.example.pgk_forum_app.ui.screens.destinations.SelectedTeacherDestination
import com.example.pgk_forum_app.ui.screens.student_group.StudentGroupViewModel
import com.example.pgk_forum_app.ui.util.collectFlow
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import kotlinx.coroutines.flow.onEach

@OptIn(ExperimentalPagerApi::class, ExperimentalMaterial3Api::class)
@StudentGroupGraphs(start = true)
@Destination
@Composable
fun SelectedGroup(
    vm: StudentGroupViewModel,
    navigator: DestinationsNavigator,
) {
    val scrollState = rememberScrollState()
    val pagerState = rememberPagerState()

    var groupInfo: GroupInfoWithTeachersGroup? by remember { mutableStateOf(null) }

    vm.groupInfoFlow.onEach { groupInfo = it }.collectFlow()

    val teacherList = groupInfo?.teachers ?: listOf()
    val studentList = groupInfo?.students ?: listOf()
    val shortName = groupInfo?.groupShortName ?: "Отсутствует"
    Scaffold(topBar = {
        SelectedEntityTopBar(title = "Группа ${groupInfo?.groupName ?: "Название Группы"}")
    }) {
        Column(
            Modifier
                .fillMaxSize()
                .padding(top = it.calculateTopPadding())
                .scrollable(scrollState, Orientation.Vertical)
        ) {
            ChangeParamField(
                paramValue = shortName,
                paramTitle = "Короткое название",
                onClick = { },
                isEditMode = false
            )
            HorizontalPager(
                modifier = Modifier.height(450.dp),
                count = 2,
                state = pagerState
            ) {
                Column() {
                    when (it) {
                        0 -> ChangeParamList(
                            paramTitle = "Студенты",
                            itemList = studentList,
                            itemListString = studentList.map { studentInfo -> studentInfo.fio },
                            emptyItemTitle = "У группы отсутствуют студенты",
                            isEditMode = false,
                            onItemClick = { student: StudentInfo ->
                                navigator.navigate(
                                    SelectedStudentDestination(student)
                                )
                            }
                        )
                        1 -> ChangeParamList(
                            paramTitle = "Учителя",
                            itemList = teacherList,
                            itemListString = teacherList.map { teacherInfo -> teacherInfo.fio },
                            emptyItemTitle = "У группы отсутствуют учителя",
                            isEditMode = false,
                            onItemClick = { teacher ->
                                navigator.navigate(
                                    SelectedTeacherDestination(teacher)
                                )
                            }
                        )
                    }
                }
            }
            HorizontalPagerIndicator(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 10.dp),
                pagerState = pagerState,
                activeColor = MaterialTheme.colorScheme.primary
            )

        }


    }
}