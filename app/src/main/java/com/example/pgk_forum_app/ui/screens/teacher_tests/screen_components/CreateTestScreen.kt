package com.example.pgk_forum_app.ui.screens.teacher_tests.screen_components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.isDigitsOnly
import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.domain.model.test.Question
import com.example.pgk_forum_app.domain.model.test.QuestionType
import com.example.pgk_forum_app.ui.common.AddTextButton
import com.example.pgk_forum_app.ui.common.AmidTitle
import com.example.pgk_forum_app.ui.common.ChangeParamEditField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.dialogs.DialogErrorTitle
import com.example.pgk_forum_app.ui.screens.teacher_tests.CreateTestViewModel
import com.example.pgk_forum_app.ui.screens.teacher_tests.TeacherTestViewModel
import com.example.pgk_forum_app.ui.util.collectFlows
import kotlinx.coroutines.launch


@Composable
fun CreateTestScreen(
    vm: CreateTestViewModel,
    teacherTestViewModel: TeacherTestViewModel,
    themeList: List<String>,
    onBack: () -> Unit,
    isError: Boolean,
    errorMessage: String?,
) {
    val scrollState = rememberScrollState()
    var testName by remember { mutableStateOf("") }
    var themeName by remember { mutableStateOf("") }
    var questionList: List<Question> by remember { mutableStateOf(listOf()) }
    collectFlows {
        launch {
            vm.createQuestionList.collect { questionList = it }
        }
        launch {
            vm.createTestTitle.collect { testName = it }
        }
        launch {
            vm.createThemeName.collect { themeName = it }
        }
    }
    Scaffold(
        topBar = {
            SelectedEntityTopBar(
                title = "Создание Нового Теста",
                onBack = onBack,
                titleFontSize = 18.sp,
                fontWeight = FontWeight.Black,
                actionButtons = {
                    AddTextButton(
                        onClick = {
                            teacherTestViewModel.createTest(
                                testTitle = testName,
                                themeName = themeName,
                                questions = questionList,
                                onSuccess = { vm.clearTest() }
                            )
                        },
                        color = MaterialTheme.colorScheme.tertiary,
                        fontSize = 16.sp,
                        fontWeight = FontWeight.ExtraBold
                    )
                }
            )
        }
    ) {
        LazyColumn(
            Modifier
                .padding(top = it.calculateTopPadding())
        ) {
            item {
                Column(Modifier.fillMaxWidth()) {
                    if (isError)
                        DialogErrorTitle(
                            errorMessage = errorMessage,
                            isError = isError,
                            modifier = Modifier.align(CenterHorizontally),
                            style = TextStyle(
                                fontWeight = FontWeight.ExtraBold,
                                fontSize = 20.sp,
                            )
                        )
                    ChangeParamEditField(
                        paramValue = testName,
                        paramTitle = "Название Теста",
                        onValueChange = { vm.changeCreateThemeName(it) }
                    )
                    ChangeParamEditField(
                        paramValue = themeName,
                        paramTitle = "Тема теста",
                        onValueChange = { vm.changeCreateThemeDescription(it) },
                        isEntityField = true,
                        entityStringList = themeList
                    )
                    AmidTitle(
                        titleString = "Вопросы",
                        modifier = Modifier.padding(start = 25.dp, top = 10.dp),
                        fontSize = 20.sp
                    )
                }
            }

            if (questionList.isEmpty())
                item {
                    Box(
                        modifier = Modifier
                            .height(70.dp)
                            .fillMaxWidth()
                    ) {
                        AmidTitle(
                            titleString = "Вопросы отсутствуют", modifier = Modifier.align(
                                Alignment.Center
                            ),
                            fontSize = 18.sp
                        )
                    }
                }
            itemsIndexed(questionList) { questionIndex, question ->
                QuestionListItem(
                    question = question,
                    index = questionIndex,
                    changeTitle = { title ->
                        vm.changeQuestionTitle(
                            questionTitle = title,
                            questionIndex = questionIndex
                        )
                    },
                    changeOption = { newValue, optionIndex ->
                        vm.changeOption(newValue, optionIndex = optionIndex, questionIndex)
                    },
                    addOption = {
                        vm.addOption(questionIndex)
                    },
                    changeRightOption = { rightAnswerValue ->
                        vm.changeQuestionRightAnswer(rightAnswerValue, questionIndex)
                    },
                    changeType = { type ->
                        vm.changeQuestionType(QuestionType.valueOf(type), questionIndex)
                    },
                    deleteOption = { index ->
                        vm.deleteOption(index, questionIndex)
                    },
                    deleteQuestion = {
                        vm.deleteQuestion(questionIndex)
                    },
                    changeScore = { stringScore ->
                        val scoreValue =
                            stringScore.trimStart { char -> char == ' ' || char == '0' }
                                .ifEmpty { "0" }
                        if (scoreValue.isDigitsOnly() && scoreValue.length < 5)
                            vm.changeQuestionScore(scoreValue.toInt(), questionIndex)
                    }
                )
            }
            item {
                Column(Modifier.fillMaxWidth()){
                    IconButton(
                        onClick = { vm.addNewQuestion() },
                        modifier = Modifier.align(CenterHorizontally)
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.plus_outline),
                            contentDescription = null,
                            tint = MaterialTheme.colorScheme.primary,
                            modifier = Modifier.size(40.dp)
                        )
                    }
                }
            }

        }
    }
}