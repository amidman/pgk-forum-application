package com.example.pgk_forum_app.ui.screens.login.screen_components

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp


@Composable
fun MiniTitle(
    titleString: String,
    modifier: Modifier,
    color: Color = MaterialTheme.colorScheme.primary,
    fontSize: TextUnit = 12.sp
) {
    Text(
        text = titleString,
        modifier = modifier,
        style = TextStyle(
            color = color,
            fontSize = fontSize,
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
            shadow = Shadow()
        )
    )
}