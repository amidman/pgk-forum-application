package com.example.pgk_forum_app.ui.screens.teacher_theme

import androidx.compose.material.Scaffold
import androidx.compose.runtime.*
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.domain.model.theme.ThemeDTO
import com.example.pgk_forum_app.ui.dialogs.ConfirmWindow
import com.example.pgk_forum_app.ui.navigation.teacher_theme.TeacherThemeNavGraphs
import com.example.pgk_forum_app.ui.screens.admin_groups.screen_components.CreateFloatingButton
import com.example.pgk_forum_app.ui.common.EditorList
import com.example.pgk_forum_app.ui.navigation.teacher.TeacherBottomGraphs
import com.example.pgk_forum_app.ui.screens.teacher_theme.screen_components.SelectedThemeDTO
import com.example.pgk_forum_app.ui.screens.teacher_theme.screen_components.ThemeAddWindow
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.annotation.Destination
import kotlinx.coroutines.launch


@TeacherBottomGraphs
@Destination
@Composable
fun TeacherThemeScreen(
    themeDTOList: List<ThemeDTO>,
    vm: TeacherThemeViewModel,
    testInfoList:List<TestInfo>
) {

    val themeNavGraph = rememberNavController()
    val currentDestination = themeNavGraph.currentBackStackEntryAsState().value?.destination?.route
        ?: TeacherThemeNavGraphs.THEMELIST.name

    var isError by remember { mutableStateOf(false) }
    var isLoading by remember { mutableStateOf(false) }
    var errorMessage: String? by remember { mutableStateOf(null) }

    var isCreateDialogEnabled by remember { mutableStateOf(false) }
    var isDeleteDialogEnabled by remember { mutableStateOf(false) }

    collectFlows {
        launch {
            vm.isCreateDialogEnabled.collect { isCreateDialogEnabled = it}
        }
        launch {
            vm.isDeleteDialogEnabled.collect { isDeleteDialogEnabled = it}
        }
        launch {
            vm.state.collect { state ->
                isError = state is BaseScreenState.Error
                isLoading = state is BaseScreenState.Loading
                errorMessage = state.error
            }
        }
    }

    val listDestination = TeacherThemeNavGraphs.THEMELIST.name
    val selectedThemeDestination = TeacherThemeNavGraphs.SELECTEDTHEME.name


    val selectedThemeArgument = TeacherThemeNavGraphs.SELECTEDTHEME.argument!!

    Scaffold(floatingActionButton = {
        if (currentDestination == listDestination)
            CreateFloatingButton {
                vm.setCreateDialog(true)
            }
    }) {
        NavHost(
            navController = themeNavGraph,
            startDestination = listDestination,
        ) {
            composable(listDestination) {
                EditorList(
                    entityList = themeDTOList,
                    isLoading = isLoading,
                    titleString = { themeDTO -> themeDTO.themeName },
                    onClick = { themeDTO ->
                        themeNavGraph.navigate(
                            selectedThemeDestination.plus("?$selectedThemeArgument=${themeDTO.id}")
                        )
                    },
                    onDelete = { themeDTO ->
                        vm.setDeleteEntityId(themeDTO.id)
                        vm.setDeleteDialog(true)
                    }
                )
            }
            composable(
                selectedThemeDestination.plus("?$selectedThemeArgument={$selectedThemeArgument}"),
                arguments = listOf(
                    navArgument(name = selectedThemeArgument) {
                        type = NavType.IntType
                    }
                )
            ) {
                val themeId = it.arguments?.getInt(selectedThemeArgument)
                val themeDTO = themeDTOList.first {themeDTO -> themeDTO.id == themeId }
                SelectedThemeDTO(
                    themeDTO = themeDTO,
                    changeName = {newName -> vm.changeThemeName(newName, themeId)},
                    changeDescription = {description -> vm.changeThemeDescription(newDescription = description, themeId = themeId)},
                    isError = isError,
                    errorMessage = errorMessage,
                    onBack = { themeNavGraph.navigate(listDestination) },
                    vm = vm,
                    testInfoList = testInfoList
                )
            }
        }
    }


    if (isCreateDialogEnabled)
        ThemeAddWindow(onDismiss = { vm.setCreateDialog(false) },
            onAdd = {theme -> vm.createTheme(theme) },
            isError = isError,
            errorMessage = errorMessage
        )

    if (isDeleteDialogEnabled)
        ConfirmWindow(
            isError = isError,
            errorMessage = errorMessage,
            onConfirm = { vm.deleteTheme() },
            onCancel = { vm.setDeleteDialog(false) }
        )
}