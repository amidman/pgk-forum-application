package com.example.pgk_forum_app.ui.screens.student_test.screen_components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.navigation.student_test.StudentTestGraphs
import com.example.pgk_forum_app.ui.screens.student_answer.screen_components.SelectedStudentAnswerEntity
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.result.ResultBackNavigator

@OptIn(ExperimentalMaterial3Api::class)
@StudentTestGraphs
@Destination
@Composable
fun StudentAnswerAfterTest(
    backNavigator:ResultBackNavigator<Boolean>,
    studentAnswer: StudentAnswer,
) {
    Scaffold(topBar = {
        SelectedEntityTopBar(
            studentAnswer.testTitle,
            onBack = {
                backNavigator.navigateBack()
            }
        )
    }) {
        SelectedStudentAnswerEntity(
            studentAnswer = studentAnswer,
            modifier = Modifier
                .padding(it)
                .fillMaxWidth()
        )
    }
}