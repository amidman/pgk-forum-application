package com.example.pgk_forum_app.ui.screens.student_answer.screen_components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.domain.model.student_answer.QuestionAnswer
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.domain.utils.toDateString
import com.example.pgk_forum_app.ui.common.AmidTitle
import com.example.pgk_forum_app.ui.common.ChangeParamField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.navigation.student_answer.StudentAnswerGraphs
import com.example.pgk_forum_app.ui.theme.PurpleGrey80
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.result.ResultBackNavigator
import java.util.*

@OptIn(ExperimentalMaterial3Api::class)
@StudentAnswerGraphs
@Destination
@Composable
fun SelectedStudentAnswer(
    backNavigator: ResultBackNavigator<Boolean>,
    studentAnswer: StudentAnswer
) {
    Scaffold(topBar = {
        SelectedEntityTopBar(
            studentAnswer.testTitle,
            onBack = {
                backNavigator.navigateBack()
            }
        )
    }) {
        SelectedStudentAnswerEntity(
            studentAnswer = studentAnswer,
            modifier = Modifier
                .padding(it)
                .fillMaxWidth()
        )
    }
}

@Composable
fun SelectedStudentAnswerEntity(
    modifier: Modifier,
    studentAnswer: StudentAnswer
) {
    val questionAnswerList = studentAnswer.questions ?: listOf()
    val stringDate = studentAnswer.answerDate.toDateString()
    LazyColumn(
        modifier = modifier
    ) {

        item{
            ChangeParamField(
                paramValue = "${studentAnswer.totalScore} баллов из ${studentAnswer.maxScore}",
                paramTitle = "Результат",
                isEditMode = false,
            )

            ChangeParamField(
                paramValue = stringDate,
                paramTitle = "Дата прохождения теста",
                isEditMode = false,
            )
        }

//            QuestionAnswerList(
//                modifier = Modifier,
//                questionAnswerList = studentAnswer.questions ?: listOf()
//            )
        itemsIndexed(questionAnswerList) { index, questionAnswer ->
            StudentQuestionAnswerItem(
                index,
                questionAnswer
            )
        }


    }
}
