package com.example.pgk_forum_app.ui.util

sealed class BaseScreenState(val error:String? = null,val navDestination:String? = null){
    object Nothing:BaseScreenState()
    object Success:BaseScreenState()
    object Loading:BaseScreenState()
    class Error(error: String? = "Server Error Try Again"):BaseScreenState(error = error)
    class NavEvent(navDestination: String?):BaseScreenState(navDestination=navDestination)
}