package com.example.pgk_forum_app.ui.navigation.teacher

import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.ui.navigation.BaseBottomNavGraphs
import com.example.pgk_forum_app.ui.navigation.NavGraphData
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.destinations.TeacherGroupScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.TeacherTestScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.TeacherThemeScreenDestination
import com.ramcosta.composedestinations.annotation.NavGraph
import com.ramcosta.composedestinations.spec.NavGraphSpec


@NavGraph
annotation class TeacherBottomGraphs(
    val start: Boolean = false
)

object TeacherBottomNavGraphs : BaseBottomNavGraphs {

    const val GROUP_TITLE = "Группы"
    const val TEST_TITLE = "Тесты"
    const val THEME_TITLE = "Темы"
    override val bottomGraphs: List<NavGraphData>
        get() = listOf(
            NavGraphData(
                icon = R.drawable.group,
                title = GROUP_TITLE,
                direction = TeacherGroupScreenDestination
            ),
            NavGraphData(
                icon = R.drawable.test,
                title = TEST_TITLE,
                direction = TeacherTestScreenDestination
            ),
            NavGraphData(
                icon = R.drawable.test,
                title = THEME_TITLE,
                direction = TeacherThemeScreenDestination
            )
        )
    override val graphs: NavGraphSpec
        get() = NavGraphs.teacherBottomGraphs

}