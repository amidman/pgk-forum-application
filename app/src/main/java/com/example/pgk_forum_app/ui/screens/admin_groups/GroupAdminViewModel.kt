package com.example.pgk_forum_app.ui.screens.admin_groups

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.group.GroupAdminRepository
import com.example.pgk_forum_app.data.api.group.GroupRepository
import com.example.pgk_forum_app.data.api.student.StudentAdminRepository
import com.example.pgk_forum_app.domain.model.group.GroupDTO
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.group.TeacherInfoWithGroupDTO
import com.example.pgk_forum_app.domain.model.network_base.ErrorModel
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.ViewModelHandle
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class GroupAdminViewModel(
    private val groupRepository: GroupRepository,
    private val groupAdminRepository: GroupAdminRepository,
    private val studentAdminRepository: StudentAdminRepository,
) : ViewModel() {

    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _groupInfoList = MutableStateFlow<List<GroupInfo>>(listOf())
    val groupInfoList = _groupInfoList.asStateFlow()

    private val _selectedGroupInfo = MutableStateFlow<GroupInfo?>(null)
    val selectedGroupInfo = _selectedGroupInfo.asStateFlow()

    private val _deleteEntityId = MutableStateFlow<Int?>(null)
    val deleteEntityId = _deleteEntityId.asStateFlow()

    private val _isDeleteDialogEnabled = MutableStateFlow<Boolean>(false)
    val isDeleteDialogEnabled = _isDeleteDialogEnabled.asStateFlow()

    private val _isCreateDialogEnabled = MutableStateFlow<Boolean>(false)
    val isCreateDialogEnabled = _isCreateDialogEnabled.asStateFlow()

    fun setDeleteDialog(value: Boolean) =
        viewModelScope.launch { _isDeleteDialogEnabled.emit(value) }

    fun setCreateDialog(value: Boolean) =
        viewModelScope.launch { _isCreateDialogEnabled.emit(value) }

    init {
        getAllGroups()
    }

    private suspend fun loading() {
        _state.emit(BaseScreenState.Loading)
    }

    fun getAllGroups() = viewModelScope.launch {
        loading()
        val allGroups = groupRepository.getAllGroups()
        allGroups.requestStatusHandle(_state, _groupInfoList)
    }

    fun selectGroupInfo(groupInfo: GroupInfo) = viewModelScope.launch {
        _selectedGroupInfo.emit(groupInfo)
    }

    fun backFromSelected() = viewModelScope.launch {
        _selectedGroupInfo.emit(null)
    }

    fun createGroup(groupDTO: GroupDTO) = viewModelScope.launch {
        loading()
        val response = groupRepository.createGroup(groupDTO)
        val viewModelHandle = object : ViewModelHandle<Int> {
            override suspend fun onSuccess(data: Int) {
                val listValue = groupInfoList.value.toMutableList()
                val groupInfo = GroupInfo(
                    id = data,
                    teachers = listOf(),
                    groupName = groupDTO.name,
                    groupShortName = groupDTO.shortName,
                    students = listOf()
                )
                listValue.add(groupInfo)
                _groupInfoList.emit(listValue)
                _isCreateDialogEnabled.emit(false)
            }

            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state, viewModelHandle)
    }

    fun setGroupDelete(id: Int?) = viewModelScope.launch { _deleteEntityId.emit(id) }

    fun deleteGroup() = viewModelScope.launch {
        val id = deleteEntityId.value ?: return@launch
        loading()
        val response = groupRepository.deleteGroup(id)
        val viewModelHandle = object : ViewModelHandle<String?> {
            override suspend fun onSuccess(data: String?) {
                val listValue = groupInfoList.value.toMutableList()
                val groupInfo = listValue.find { it.id == id }
                listValue.remove(groupInfo)
                _groupInfoList.emit(listValue)
                _isDeleteDialogEnabled.emit(false)
            }

            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state, viewModelHandle)
        _deleteEntityId.emit(null)
    }

    private suspend fun updateGroupInList(newGroupInfo: GroupInfo?) {
        newGroupInfo ?: return
        val groupList = groupInfoList.value.toMutableList()
        val indexOfGroup = groupList.indexOfFirst { newGroupInfo.id == it.id }
        groupList[indexOfGroup] = newGroupInfo
        _selectedGroupInfo.emit(newGroupInfo)
        _groupInfoList.emit(groupList)
    }

    private suspend fun updateGroupTeacherList(teacherList: List<TeacherInfo>?) {
        selectedGroupInfo.value ?: return
        teacherList ?: return
        val groupInfo = selectedGroupInfo.value?.copy(teachers = teacherList)
        updateGroupInList(groupInfo)
    }

    private suspend fun updateGroupStudentList(studentList: List<StudentInfo>?) {
        selectedGroupInfo.value ?: return
        studentList ?: return
        val groupInfo = selectedGroupInfo.value?.copy(students = studentList)
        updateGroupInList(groupInfo)
    }

    fun changeGroupName(groupName: String) = viewModelScope.launch {
        val groupInfo = selectedGroupInfo.value ?: return@launch
        val groupId = groupInfo.id ?: return@launch
        loading()
        val response = groupAdminRepository.changeGroupName(groupId = groupId, groupName)
        if (response is RequestStatus.Success)
            updateGroupInList(groupInfo.copy(groupName = groupName))
        response.requestStatusHandle(_state)
    }

    fun changeGroupShortName(groupShortName: String) = viewModelScope.launch {
        val groupInfo = selectedGroupInfo.value ?: return@launch
        val groupId = groupInfo.id ?: return@launch
        loading()
        val response = groupAdminRepository.changeShortName(groupId = groupId, groupShortName)
        if (response is RequestStatus.Success)
            updateGroupInList(groupInfo.copy(groupShortName = groupShortName))
        response.requestStatusHandle(_state)
    }

    fun addTeacherToGroup(teacherLogin: String) = viewModelScope.launch {
        val groupInfo = selectedGroupInfo.value ?: return@launch
        loading()
        val response = groupAdminRepository.addGroupToTeacher(teacherLogin, groupInfo.groupName)
        val viewModelHandle = object : ViewModelHandle<TeacherInfoWithGroupDTO> {
            override suspend fun onSuccess(data: TeacherInfoWithGroupDTO) {
                val teacherList = groupInfo.teachers
                updateGroupTeacherList(teacherList + data.teacherInfo)
            }

            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state, viewModelHandle)
    }

    fun removeTeacherFromGroup(teacherLogin: String) = viewModelScope.launch {
        val groupInfo = selectedGroupInfo.value ?: return@launch
        val groupId = groupInfo.id ?: return@launch
        loading()
        val response = groupAdminRepository.deleteGroupTeacher(teacherLogin, groupId)
        val viewModelHandle = object : ViewModelHandle<String?> {
            override suspend fun onSuccess(data: String?) {
                val teacherList = groupInfo.teachers
                val teacherInfo = teacherList.find { teacherInfo -> teacherInfo.login == teacherLogin } ?: return
                updateGroupTeacherList(teacherList - teacherInfo)
            }

            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state, viewModelHandle)
    }

    fun addStudentToGroup(studentLogin: String) = viewModelScope.launch {
        val groupInfo = selectedGroupInfo.value ?: return@launch
        loading()
        val response = studentAdminRepository.changeStudentGroup(
            studentLogin = studentLogin,
            groupInfo.groupName
        )
        val viewModelHandle = object : ViewModelHandle<StudentInfo> {
            override suspend fun onSuccess(data: StudentInfo) {
                val studentList = groupInfo.students.toMutableList()
                studentList.add(data)
                updateGroupStudentList(studentList)
            }

            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state, viewModelHandle)
    }

    fun removeStudentFromGroup(studentLogin: String) = viewModelScope.launch {
        val groupInfo = selectedGroupInfo.value ?: return@launch
        loading()
        val response = studentAdminRepository.setStudentGroupNull(studentLogin = studentLogin)
        val viewModelHandle = object : ViewModelHandle<StudentInfo> {
            override suspend fun onSuccess(data: StudentInfo) {
                val studentList = groupInfo.students.toMutableList()
                val studentInfo =
                    studentList.find { studentInfo -> studentInfo.login == studentLogin }
                studentList.remove(studentInfo)
                updateGroupStudentList(studentList)
            }

            override suspend fun onError(errorModel: ErrorModel?) {}
        }
        response.requestStatusHandle(_state, viewModelHandle)
    }
}

//fun main() {
//    val info =
//        GroupInfo(
//            id = 4,
//            teacher = listOf(
//                TeacherInfo(
//                    userId = 10,
//                    fio = "levina",
//                    login = "gg",
//                    role = "TEACHER",
//                    teacherId = 2
//                ),
//                TeacherInfo(
//                    userId = 17,
//                    fio = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
//                    login = "ignat",
//                    role = "TEACHER",
//                    teacherId = 6
//                ),
//
//            ),
//            groupName = "isp-335-hello",
//            groupShortName = "isp-335-short",
//            students = listOf()
//        )
//
//    val list =
//        listOf(GroupInfo(
//            id = 4,
//            teacher = listOf(
//            TeacherInfo(
//                userId = 10,
//                fio = "levina",
//                login = "gg",
//                role = "TEACHER",
//                teacherId = 2
//            ), TeacherInfo(
//                userId = 17,
//                fio = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
//                login = "ignat",
//                role = "TEACHER",
//                teacherId = 6
//            )),
//            groupName = "isp-335-hello",
//            groupShortName = "isp-335-short",
//            students = listOf()
//        ),)
//    println(list.indexOf(info))
//    println(listOf(1,2) == listOf(2,1))
//}