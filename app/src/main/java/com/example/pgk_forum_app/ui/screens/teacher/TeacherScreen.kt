package com.example.pgk_forum_app.ui.screens.teacher

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.domain.model.theme.ThemeDTO
import com.example.pgk_forum_app.ui.common.EntityBottomNavigation
import com.example.pgk_forum_app.ui.navigation.teacher.TeacherBottomNavGraphs
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.destinations.TeacherGroupScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.TeacherTestScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.TeacherThemeScreenDestination
import com.example.pgk_forum_app.ui.screens.teacher_groups.TeacherGroupScreen
import com.example.pgk_forum_app.ui.screens.teacher_groups.TeacherGroupViewModel
import com.example.pgk_forum_app.ui.screens.teacher_tests.CreateTestViewModel
import com.example.pgk_forum_app.ui.screens.teacher_tests.TeacherTestScreen
import com.example.pgk_forum_app.ui.screens.teacher_tests.TeacherTestViewModel
import com.example.pgk_forum_app.ui.screens.teacher_theme.TeacherThemeScreen
import com.example.pgk_forum_app.ui.screens.teacher_theme.TeacherThemeViewModel
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.manualcomposablecalls.composable
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel


@OptIn(ExperimentalMaterial3Api::class)
@RootNavGraph
@Destination
@Composable
fun TeacherScreen() {

    val teacherNav = rememberNavController()
    val currentDestination = teacherNav.currentBackStackEntryAsState().value?.destination?.route

    val groupDestination = TeacherBottomNavGraphs.GROUP_TITLE
    val testDestination = TeacherBottomNavGraphs.TEST_TITLE
    val themeDestination = TeacherBottomNavGraphs.THEME_TITLE

    val groupViewModel = getViewModel<TeacherGroupViewModel>()
    val testViewModel = getViewModel<TeacherTestViewModel>()
    val themeViewModel = getViewModel<TeacherThemeViewModel>()
    val createTestViewModel = getViewModel<CreateTestViewModel>()

    var groupInfoList: List<GroupInfo> by remember { mutableStateOf(listOf()) }
    var testInfoList: List<TestInfo> by remember { mutableStateOf(listOf()) }
    var themeList: List<ThemeDTO> by remember { mutableStateOf(listOf()) }

    collectFlows(onCollect = {
        launch {
            groupViewModel.groupInfoListFlow.collect {
                groupInfoList = it
            }
        }
        launch {
            testViewModel.testInfoListFlow.collect {
                testInfoList = it
            }
        }
        launch {
            themeViewModel.themeList.collect {
                themeList = it
            }
        }

    })

    Scaffold(
        bottomBar = {
            EntityBottomNavigation(
                navController = teacherNav,
                navGraphs = TeacherBottomNavGraphs
            )
        }
    ) {
        DestinationsNavHost(
            navGraph = NavGraphs.teacherBottomGraphs,
            navController = teacherNav,
            modifier = Modifier.padding(it)
        ){
            composable(TeacherGroupScreenDestination) {
                TeacherGroupScreen(
                    groupInfoList = groupInfoList,
                )
            }
            composable(TeacherTestScreenDestination) {
                TeacherTestScreen(
                    testInfoList = testInfoList,
                    vm = testViewModel,
                    themeDTOList = themeList,
                    createTestViewModel = createTestViewModel
                )
            }
            composable(TeacherThemeScreenDestination) {
                TeacherThemeScreen(
                    themeDTOList = themeList,
                    vm = themeViewModel,
                    testInfoList = testInfoList
                )
            }
        }
    }


}