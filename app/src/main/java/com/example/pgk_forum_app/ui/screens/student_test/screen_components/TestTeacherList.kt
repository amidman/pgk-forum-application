package com.example.pgk_forum_app.ui.screens.student_test.screen_components

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import com.example.pgk_forum_app.ui.common.EditorList
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.screens.student_test.StudentTestViewModel
import com.example.pgk_forum_app.ui.util.BaseScreenState
import kotlinx.coroutines.flow.onEach
import com.example.pgk_forum_app.ui.navigation.student_test.StudentTestGraphs
import com.example.pgk_forum_app.ui.screens.destinations.SelectedStudentTestDestination
import com.example.pgk_forum_app.ui.screens.destinations.StudentTestListDestination
import com.example.pgk_forum_app.ui.screens.destinations.TeacherTestScreenDestination
import com.example.pgk_forum_app.ui.screens.student_test.CreateStudentAnswerViewModel
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@StudentTestGraphs(start = true)
@Destination
@Composable
fun TestTeacherList(
    vm:StudentTestViewModel,
    navigator: DestinationsNavigator
) {
    var isLoading by remember { mutableStateOf(false) }
    var teacherList by remember { mutableStateOf(listOf<TeacherInfo>()) }

    collectFlows{
        launch {
            vm.teacherInfoListFlow.collect{
                teacherList = it
            }
        }
        launch {
            vm.state.collect {
                isLoading = it is BaseScreenState.Loading
            }
        }
    }

    BackHandler(true){}
    Scaffold(
        topBar = {
            SelectedEntityTopBar(
                title = "Учителя",
                titleFontSize = 22.sp
            )
        }
    ){
        EditorList(
            modifier = Modifier.padding(it),
            entityList = teacherList,
            isLoading = isLoading,
            titleString = { teacherInfo ->
                teacherInfo.fio
            },
            onClick = { teacher ->
                navigator.navigate(
                    StudentTestListDestination(
                        teacherId = teacher.teacherId
                    )
                )
            },
            isEditMode = false,
            onDelete = {}
        )
    }
}