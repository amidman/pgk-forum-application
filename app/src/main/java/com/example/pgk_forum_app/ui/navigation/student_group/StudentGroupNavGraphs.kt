package com.example.pgk_forum_app.ui.navigation.student_group

import com.ramcosta.composedestinations.annotation.NavGraph


@NavGraph
annotation class StudentGroupGraphs(
    val start:Boolean = false
)

