package com.example.pgk_forum_app.ui.dialogs

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun DialogParamField(
    title:String,
    value:String,
    onValueChange:(value:String) -> Unit,
    modifier: Modifier = Modifier
        .width(350.dp)
        .sizeIn(maxHeight = 350.dp)
        .padding(vertical = 6.dp, horizontal = 20.dp),
    singleLine:Boolean = true
) {
    ParamField(
        title = title,
        modifier = modifier ,
        value = value,
        onValueChange = onValueChange,
        singleLine = singleLine
    )
}