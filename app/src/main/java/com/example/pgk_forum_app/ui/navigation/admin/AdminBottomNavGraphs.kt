package com.example.pgk_forum_app.ui.navigation.admin

import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.ui.navigation.BaseBottomNavGraphs
import com.example.pgk_forum_app.ui.navigation.NavGraphData
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.destinations.AdminGroupScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.AdminStudentScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.AdminTeacherScreenDestination
import com.ramcosta.composedestinations.annotation.NavGraph
import com.ramcosta.composedestinations.spec.NavGraphSpec


@NavGraph
annotation class AdminBottomGraphs(
    val start: Boolean = false
)

object AdminBottomNavGraphs : BaseBottomNavGraphs {
    const val GROUP_TITLE = "Группы"
    const val TEACHER_TITLE = "Учителя"
    const val STUDENT_TITLE = "Студенты"

    override val bottomGraphs: List<NavGraphData>
        get() =
            listOf(
                NavGraphData(
                    icon = R.drawable.group,
                    title = GROUP_TITLE,
                    direction = AdminGroupScreenDestination
                ),
                NavGraphData(
                    icon = R.drawable.teacher,
                    title = TEACHER_TITLE,
                    direction = AdminTeacherScreenDestination
                ),
                NavGraphData(
                    icon = R.drawable.student,
                    title = STUDENT_TITLE,
                    direction = AdminStudentScreenDestination
                ),
            )
    override val graphs: NavGraphSpec
        get() = NavGraphs.adminBottomGraphs


}


