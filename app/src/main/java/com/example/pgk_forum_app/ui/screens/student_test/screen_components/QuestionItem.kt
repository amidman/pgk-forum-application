package com.example.pgk_forum_app.ui.screens.student_test.screen_components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment.Companion.BottomCenter
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.domain.model.student_answer.QuestionAnswer
import com.example.pgk_forum_app.domain.model.test.QuestionType
import com.example.pgk_forum_app.ui.common.AmidTitle
import com.example.pgk_forum_app.ui.screens.student_test.CreateStudentAnswerViewModel


@Composable
fun QuestionItem(
    questionIndex: Int,
    questionList: List<QuestionAnswer>,
    currentValue:String,
    onLast: () -> Unit,
    vm:CreateStudentAnswerViewModel
) {

    if (questionList.isEmpty()) return
    val question = questionList[questionIndex]

    val isLast = questionIndex == questionList.lastIndex
    val isFirst = questionIndex == 0


    val optionListModifier = Modifier
        .padding(start = 20.dp, top = 10.dp)
        .fillMaxWidth()
        .sizeIn(minHeight = 300.dp)
    val editFieldModifier = Modifier
        .padding(start = 20.dp, top = 10.dp, end = 20.dp)
        .fillMaxWidth()
        .sizeIn(minHeight = 120.dp, maxHeight = 200.dp)
    Card(
        Modifier
            .fillMaxSize()
            .padding(20.dp)
    ) {
        Box() {
            Column(
                modifier = Modifier
                    .padding(bottom = 70.dp)
                    .fillMaxSize()
            ) {
                AmidTitle(
                    titleString = "Вопрос ${questionIndex + 1}",
                    modifier = Modifier
                        .padding(20.dp),
                )
                AmidTitle(
                    titleString = question.questionTitle,
                    modifier = Modifier
                        .align(CenterHorizontally)
                        .padding(horizontal = 5.dp),
                    singleLine = false,
                    fontSize = 22.sp,
                    textAlign = TextAlign.Center
                )
                if (question.questionType == QuestionType.ONE_OPTION.name)
                    OptionList(
                        itemModifier = Modifier
                            .padding(vertical = 5.dp, horizontal = 15.dp)
                            .fillMaxWidth()
                            .sizeIn(minHeight = 50.dp),
                        selectedValue = currentValue,
                        optionList = question.questionOptions,
                        onSelect = { vm.changeAnswer(it) }
                    )
                else
                    AnswerTextField(
                        modifier = editFieldModifier,
                        value = currentValue,
                        onValueChange = { vm.changeAnswer(it) },
                    )
            }
            OptionNavigationButtons(
                modifier = Modifier
                    .padding(top = 10.dp)
                    .align(BottomCenter)
                    .fillMaxWidth(),
                isLast = isLast,
                isFirst = isFirst,
                onPrev = { vm.prevQuestion() },
                onNext = {
                    if (isLast)
                        onLast()
                    else
                        vm.nextQuestion()
                },
            )
        }

    }

}