package com.example.pgk_forum_app.ui.screens.admin_teacher.screen_components

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfoWithGroupList
import com.example.pgk_forum_app.ui.common.EntityList

@Composable
fun TeacherListWithGroup(
    teacherListWithGroupList: List<TeacherInfoWithGroupList>,
    onClick: (teacherInfoWithGroupList: TeacherInfoWithGroupList) -> Unit,
    onDelete: (teacherInfoWithGroupList: TeacherInfoWithGroupList) -> Unit,
    isLoading:Boolean
) {

    EntityList(entityList = teacherListWithGroupList, isLoading = isLoading) { index, teacherInfo ->
        TeacherInfoItem(
            teacherInfo = teacherInfo.teacherInfo(),
            onClick = { onClick(teacherInfo) },
            onDelete = { onDelete(teacherInfo) },
        )
    }
}

@Composable
fun TeacherList(
    teacherList: List<TeacherInfo>,
    onClick: (teacherInfo: TeacherInfo) -> Unit,
    onDelete: (teacherInfo: TeacherInfo) -> Unit
) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        itemsIndexed(teacherList) { index, teacherInfo ->
            TeacherInfoItem(
                teacherInfo = teacherInfo,
                onClick = { onClick(teacherInfo) },
                onDelete = { onDelete(teacherInfo) })
        }
    }
}