package com.example.pgk_forum_app.ui.screens.admin_teacher.screen_components

import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.pgk_forum_app.domain.model.group.GroupDTO
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfoWithGroupList
import com.example.pgk_forum_app.ui.common.ChangeParamField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.screens.admin_students.screen_components.ChangeParamList
import com.example.pgk_forum_app.ui.screens.admin_teacher.TeacherAdminViewModel
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlow
import kotlinx.coroutines.flow.onEach


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SelectedTeacherWithGroup(
    teacherInfo: TeacherInfoWithGroupList,
    changeFio:(value:String)->Unit,
    changeLogin:(value:String)->Unit,
    changePassword:(value:String)->Unit,
    addGroup:(groupName:String) -> Unit,
    deleteGroup:(groupDTO:GroupDTO) -> Unit,
    isError:Boolean,
    errorMessage:String?,
    onBack:() -> Unit,
    vm: TeacherAdminViewModel,
    groupList:List<GroupInfo>,
) {
    val scrollState = rememberScrollState()
    val isLogin = remember{ mutableStateOf(false) }
    val isPassword = remember{ mutableStateOf(false) }
    val isFio = remember{ mutableStateOf(false) }
    val isGroupAdd = remember { mutableStateOf(false) }
    vm.state.onEach {
        if (it is BaseScreenState.Success){
            isLogin.value = false
            isFio.value = false
            isPassword.value = false
            isGroupAdd.value = false
        }
    }.collectFlow()

    Scaffold(topBar = {
        SelectedEntityTopBar(title = "Учитель ${teacherInfo.fio}",onBack=onBack)
    }) {
        Column(
            Modifier
                .fillMaxSize()
                .padding(top = it.calculateTopPadding())
                .verticalScroll(scrollState)) {
            ChangeParamField(
                paramValue = teacherInfo.fio,
                paramTitle = "ФИО",
                onClick = {},
                onChange = changeFio,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isFio
            )
            ChangeParamField(
                paramValue = teacherInfo.login,
                paramTitle = "Логин",
                onClick = { },
                onChange = changeLogin,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isLogin
            )
            ChangeParamField(
                paramValue = "Измените Значение",
                paramTitle = "Пароль",
                onClick = { },
                onChange = changePassword,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isPassword
            )
            Column(modifier = Modifier.sizeIn(maxHeight = 350.dp, minHeight = 170.dp)){
                ChangeParamList<GroupDTO>(
                    paramTitle = "Группы",
                    onDelete = deleteGroup,
                    itemList = teacherInfo.groups,
                    itemListString = teacherInfo.groups.map { groupDTO -> groupDTO.name },
                    emptyItemTitle = "У учителя отсутствуют группы",
                    valueTitle = "Название группы",
                    onAdd = addGroup,
                    isCreateDialogEnabled = isGroupAdd,
                    isError = isError,
                    errorString = errorMessage,
                    allItemList = groupList.map { groupInfo -> groupInfo.groupName }.sorted()
                )
            }
        }

    }
}