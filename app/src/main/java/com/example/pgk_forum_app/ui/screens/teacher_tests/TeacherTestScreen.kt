package com.example.pgk_forum_app.ui.screens.teacher_tests

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.ui.util.collectFlows
import kotlinx.coroutines.launch
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.pgk_forum_app.domain.model.theme.ThemeDTO
import com.example.pgk_forum_app.ui.dialogs.ConfirmWindow
import com.example.pgk_forum_app.ui.navigation.teacher_test.TeacherTestNavGraphs
import com.example.pgk_forum_app.ui.screens.admin_groups.screen_components.CreateFloatingButton
import com.example.pgk_forum_app.ui.screens.teacher_tests.screen_components.TestInfoItem
import com.example.pgk_forum_app.ui.common.EditorList
import com.example.pgk_forum_app.ui.navigation.teacher.TeacherBottomGraphs
import com.example.pgk_forum_app.ui.screens.teacher_tests.screen_components.CreateTestScreen
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.ramcosta.composedestinations.annotation.Destination


@OptIn(ExperimentalMaterial3Api::class)
@TeacherBottomGraphs
@Destination
@Composable
fun TeacherTestScreen(
    testInfoList:List<TestInfo>,
    vm:TeacherTestViewModel,
    createTestViewModel:CreateTestViewModel,
    themeDTOList:List<ThemeDTO>
) {

    val teacherTestScreenNav = rememberNavController()
    val currentDestination = teacherTestScreenNav.currentBackStackEntryAsState().value?.destination?.route
        ?: TeacherTestNavGraphs.TESTLIST.name

    var isDeleteDialogEnabled by remember { mutableStateOf(false) }
    var isError by remember { mutableStateOf(false) }
    var errorMessage:String? by remember { mutableStateOf(null) }
    var isLoading by remember { mutableStateOf(false) }

    collectFlows {
        launch {
            vm.isDeleteDialogEnabled.collect { isDeleteDialogEnabled = it }
        }
        launch {
            vm.state.collect { state ->
                isError = state is BaseScreenState.Error
                errorMessage = state.error
                isLoading = state is BaseScreenState.Loading
                state.navDestination?.let { destination ->
                    if (destination != currentDestination)
                        teacherTestScreenNav.navigate(destination)
                }
            }
        }
    }

    val listDestination = TeacherTestNavGraphs.TESTLIST.name
    val createdDestination = TeacherTestNavGraphs.CREATETEST.name
    val selectedDestination = TeacherTestNavGraphs.SELECTEDTEST.name

    Scaffold(
        floatingActionButton = {
            if (currentDestination == listDestination)
            CreateFloatingButton {
                teacherTestScreenNav.navigate(createdDestination)
            }
        }
    ) {
        NavHost(navController = teacherTestScreenNav, startDestination = listDestination){
            composable(listDestination){
                EditorList(
                    entityList = testInfoList,
                    isLoading = isLoading,
                    titleString = {testInfo: TestInfo -> testInfo.title },
                    onClick = {

                    },
                    onDelete = { testInfo ->
                        vm.selectDeleteEntity(testId = testInfo.id)
                        vm.setDeleteDialog(true)
                    },
                )
            }
            composable(createdDestination){
                CreateTestScreen(
                    vm = createTestViewModel,
                    themeList = themeDTOList.map { themeDTO -> themeDTO.themeName },
                    teacherTestViewModel = vm,
                    onBack = {
                        teacherTestScreenNav.navigate(listDestination)
                    },
                    isError = isError,
                    errorMessage = errorMessage,
                )
            }
            composable(selectedDestination){

            }
        }
    }



    if (isDeleteDialogEnabled)
        ConfirmWindow(
            errorMessage = errorMessage,
            isError = isError,
            confirmMessage = "Подтвердите удаление теста",
            confirmTitle = "Удаление",
            onCancel = {
                vm.setDeleteDialog(false)
            },
            onConfirm = {
                vm.deleteTest()
            }
        )

}