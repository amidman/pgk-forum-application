package com.example.pgk_forum_app.ui.screens.student_test

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.teacher.TeacherStudentRepository
import com.example.pgk_forum_app.data.api.test.TestRepository
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import com.example.pgk_forum_app.domain.model.test.TestInfo
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class StudentTestViewModel(
    private val teacherStudentRepository: TeacherStudentRepository,
    private val testRepository: TestRepository,
):ViewModel() {


    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _teacherInfoListFlow = MutableStateFlow<List<TeacherInfo>>(listOf())
    val teacherInfoListFlow = _teacherInfoListFlow.asStateFlow()

    private val _teacherTestInfoListFlow = MutableStateFlow<List<TestInfo>>(listOf())
    val teacherTestInfoListFlow = _teacherTestInfoListFlow.asStateFlow()


    init {
        getStudentTeachers()
    }

    fun getStudentTeachers() = viewModelScope.launch{
        val response = teacherStudentRepository.getStudentTeachers()
        response.requestStatusHandle(_state,_teacherInfoListFlow)
    }


    fun getTeacherTests(teacherId:Int) = viewModelScope.launch {
        val response = testRepository.getTeacherTests(teacherId)
        response.requestStatusHandle(_state,_teacherTestInfoListFlow)
    }


}