package com.example.pgk_forum_app.ui.screens.student_answer

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.student_answers.StudentAnswerStudentRepository
import com.example.pgk_forum_app.domain.model.network_base.ErrorModel
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.ViewModelHandle
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class StudentAnswerViewModel(
    private val studentAnswerStudentRepository: StudentAnswerStudentRepository
): ViewModel() {

    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _studentAnswerListFlow = MutableStateFlow<List<StudentAnswer>>(listOf())
    val studentAnswerListFlow = _studentAnswerListFlow.asStateFlow()

    init {
        getAllStudentAnswer()
    }


    fun getAllStudentAnswer() = viewModelScope.launch {
        val response = studentAnswerStudentRepository.getStudentAnswers()
        response.requestStatusHandle(_state,_studentAnswerListFlow)

    }

    suspend fun createStudentAnswer(
        studentAnswer: StudentAnswer
    ): StudentAnswer? {
        val response = studentAnswerStudentRepository.createStudentAnswer(studentAnswer)
        val studentAnswerList = studentAnswerListFlow.value.toMutableList()
        val viewModelHandle = object:ViewModelHandle<StudentAnswer>{
            override suspend fun onSuccess(data: StudentAnswer) {
                val currentStudentAnswer = studentAnswerList.firstOrNull{ studentAnswer ->
                    studentAnswer.id == data.id
                }
                if (currentStudentAnswer == null){
                    studentAnswerList.add(data)
                }else{
                    val indexOfStudentAnswer = studentAnswerList.indexOf(currentStudentAnswer)
                    studentAnswerList[indexOfStudentAnswer] = data
                }
                _studentAnswerListFlow.emit(studentAnswerList)
            }

        }
        response.requestStatusHandle(_state,viewModelHandle)
        return response.data
    }


}