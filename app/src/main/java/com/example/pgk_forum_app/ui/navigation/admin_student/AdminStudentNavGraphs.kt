package com.example.pgk_forum_app.ui.navigation.admin_student

enum class AdminStudentNavGraphs {
    STUDENTLIST, SELECTED
}