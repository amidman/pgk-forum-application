package com.example.pgk_forum_app.ui.screens.teacher_tests.screen_components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterEnd
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.ui.common.DeleteIcon
import com.example.pgk_forum_app.ui.common.PGKTextField

@Composable
fun OptionItem(
    index: Int,
    optionValue: String,
    onDelete: () -> Unit,
    onOptionValueChange: (value: String) -> Unit
) {
    Box(
        modifier = Modifier
            .padding(start = 50.dp, end = 20.dp, top = 10.dp)
            .fillMaxWidth()
    ) {
        PGKTextField(
            value = optionValue,
            onValueChange = onOptionValueChange,
            singleLine = false,
            fontSize = 14.sp,
            modifier = Modifier
                .align(
                    Alignment.CenterStart
                )
                .width(200.dp),
            color = Color.Black
        )

        DeleteIcon(
            onDelete = onDelete,
            modifier = Modifier.align(CenterEnd)
        )

    }
}