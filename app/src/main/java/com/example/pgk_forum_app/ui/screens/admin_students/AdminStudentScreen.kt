package com.example.pgk_forum_app.ui.screens.admin_students

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.ui.common.EditorList
import com.example.pgk_forum_app.ui.dialogs.ConfirmWindow
import com.example.pgk_forum_app.ui.navigation.admin.AdminBottomGraphs
import com.example.pgk_forum_app.ui.screens.admin_students.screen_components.StudentAddWindow
import com.example.pgk_forum_app.ui.navigation.admin_student.AdminStudentNavGraphs
import com.example.pgk_forum_app.ui.screens.admin_groups.GroupAdminViewModel
import com.example.pgk_forum_app.ui.screens.admin_groups.screen_components.CreateFloatingButton
import com.example.pgk_forum_app.ui.screens.admin_students.screen_components.SelectedStudentInfo
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.annotation.Destination
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@AdminBottomGraphs
@Destination
@Composable
fun AdminStudentScreen(
    studentInfoList:List<StudentInfo>,
    groupInfoList:List<GroupInfo>,
    vm: StudentAdminViewModel,
    groupViewModel: GroupAdminViewModel
) {
    val adminStudentNavController = rememberNavController()
    val currentDestination = adminStudentNavController.currentBackStackEntryAsState().value?.destination?.route

    var isCreateDialogEnabled by remember { mutableStateOf(false) }
    var isDeleteDialogEnabled by remember { mutableStateOf(false) }
    var isError by remember { mutableStateOf(false) }
    var errorMessage: String? by remember { mutableStateOf(null) }
    var isLoading:Boolean by remember { mutableStateOf(false) }
    var selectedStudentInfo: StudentInfo? by remember { mutableStateOf(null) }

    collectFlows(onCollect = {
        launch{ vm.isDeleteDialogEnabled.collect { isDeleteDialogEnabled = it } }
        launch{ vm.isCreateDialogEnabled.collect { isCreateDialogEnabled = it } }
        launch{ vm.selectedStudentInfo.collect { selectedStudentInfo = it } }
        launch{
            vm.state.collect {
                isError = it is BaseScreenState.Error
                errorMessage = it.error
                isLoading = it is BaseScreenState.Loading
            }
        }
    })

    val listDestination = AdminStudentNavGraphs.STUDENTLIST.name
    val selectedDestination = AdminStudentNavGraphs.SELECTED.name


    Scaffold(
        topBar = {},
        floatingActionButton = {
            if (currentDestination == listDestination) CreateFloatingButton(
                onClick = { vm.setCreateDialog(true) })
        }
    ) {
        NavHost(navController = adminStudentNavController, startDestination = listDestination) {
            composable(listDestination) {
                EditorList(
                    entityList = studentInfoList,
                    titleString = {value ->
                        value.login
                    },
                    onClick = {
                        vm.selectStudent(it); adminStudentNavController.navigate(selectedDestination)
                    },
                    onDelete = {
                        vm.setDeleteStudent(it.studentId)
                        vm.setDeleteDialog(true)
                    },
                    isLoading = isLoading
                )
            }
            composable(selectedDestination) {
                selectedStudentInfo?.let { studentInfo ->
                    val userId = studentInfo.userId?:0
                    SelectedStudentInfo(
                        studentInfo = studentInfo,
                        changeFio = {fio -> vm.changeUserFio(userId,fio)},
                        changeLogin = {login-> vm.changeUserLogin(userId,login)},
                        changePassword = {password-> vm.changeUserPassword(userId,password)},
                        changeGroup = {groupName -> vm.changeGroup(studentInfo.login,groupName)},
                        isError = isError,
                        errorMessage = errorMessage,
                        onBack = {vm.backFromSelect();adminStudentNavController.navigate(listDestination)},
                        vm = vm,
                        groupList = groupInfoList
                    )
                }
            }
        }
    }

    if (isCreateDialogEnabled)
        StudentAddWindow(
            groupList = groupInfoList,
            onDismiss = { vm.setCreateDialog(false) },
            onAdd = { vm.createStudent(it) },
            isError = isError,
            errorMessage = errorMessage
        )
    if (isDeleteDialogEnabled)
        ConfirmWindow(
            isError = isError,
            errorMessage = errorMessage,
            onCancel = { vm.setDeleteDialog(false) },
            onConfirm = { vm.deleteStudent() }
        )
}