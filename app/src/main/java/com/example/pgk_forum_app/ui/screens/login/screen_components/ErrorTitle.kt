package com.example.pgk_forum_app.ui.screens.login.screen_components

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp

@Composable
fun ErrorTitle(
    isError:Boolean,
    errorMessage:String?,
    modifier: Modifier
) {
    if (isError){
        ErrorText(errorMessage ?: "Неизвестная ошибка",modifier)
    }
}

@Composable
private fun ErrorText(message:String,modifier: Modifier) {
    val errorColor = MaterialTheme.colorScheme.error
    Text(modifier = modifier, text = message,
        style = TextStyle(
            color = errorColor,
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )
    )
}