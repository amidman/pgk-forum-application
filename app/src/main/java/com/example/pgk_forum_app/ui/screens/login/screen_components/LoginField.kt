package com.example.pgk_forum_app.ui.screens.login.screen_components

import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.ui.screens.login.LoginViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginField(
    modifier: Modifier,
    vm: LoginViewModel,
    isError:Boolean
) {

    val loginHint = stringResource(id = R.string.login_hint)

    val login = vm.loginFlow.collectAsState()

    OutlinedTextField(
        value = login.value,
        onValueChange = { newValue ->
            vm.changeLogin(newValue)
        },
        modifier = modifier,
        isError = isError,
        label = {
            Text(text = loginHint)
        },
        singleLine = true
    )

}