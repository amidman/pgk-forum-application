package com.example.pgk_forum_app.ui.screens.teacher_theme

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pgk_forum_app.data.api.theme.ThemeTeacherRepository
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.theme.ThemeDTO
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.ViewModelHandle
import com.example.pgk_forum_app.ui.util.requestStatusHandle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class TeacherThemeViewModel(
    private val themeTeacherRepository: ThemeTeacherRepository
):ViewModel() {

    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _themeList = MutableStateFlow<List<ThemeDTO>>(listOf())
    val themeList = _themeList.asStateFlow()

    private val _deleteEntityId = MutableStateFlow<Int?>(null)
    val deleteEntityId = _deleteEntityId.asStateFlow()

    fun setDeleteEntityId(id:Int?) = viewModelScope.launch {
        _deleteEntityId.emit(id)
    }

    private val _isDeleteDialogEnabled = MutableStateFlow<Boolean>(false)
    val isDeleteDialogEnabled = _isDeleteDialogEnabled.asStateFlow()

    private val _isCreateDialogEnabled = MutableStateFlow<Boolean>(false)
    val isCreateDialogEnabled = _isCreateDialogEnabled.asStateFlow()

    fun setDeleteDialog(value: Boolean) =
        viewModelScope.launch { _isDeleteDialogEnabled.emit(value) }

    fun setCreateDialog(value: Boolean) =
        viewModelScope.launch { _isCreateDialogEnabled.emit(value) }

    init {
        getAllThemes()
    }

    private suspend fun loading() {
        _state.emit(BaseScreenState.Loading)
    }

    fun getAllThemes() = viewModelScope.launch {
        loading()
        val themeDTOList = themeTeacherRepository.getTeacherThemes()
        themeDTOList.requestStatusHandle(_state,_themeList)
    }

    fun changeThemeName(newName:String,themeId:Int?) = viewModelScope.launch {
        themeId ?: return@launch
        val response = themeTeacherRepository.changeThemeName(themeId, newName)
        if (response is RequestStatus.Success){
            val currentList = _themeList.value.toMutableList()
            val themeIndex = currentList.indexOfFirst { themeDTO -> themeDTO.id == themeId }
            currentList[themeIndex] = currentList[themeIndex].copy(themeName = newName)
            _themeList.emit(currentList)
        }
        response.requestStatusHandle(_state)
    }

    fun changeThemeDescription(newDescription:String,themeId:Int?) = viewModelScope.launch {
        themeId ?: return@launch
        val response = themeTeacherRepository.changeThemeDescription(themeId, newDescription)
        if (response is RequestStatus.Success){
            val currentList = _themeList.value.toMutableList()
            val themeIndex = currentList.indexOfFirst { themeDTO -> themeDTO.id == themeId }
            currentList[themeIndex] = currentList[themeIndex].copy(description = newDescription)
            _themeList.emit(currentList)
        }
        response.requestStatusHandle(_state)
    }


    fun createTheme(themeDTO: ThemeDTO) = viewModelScope.launch {
        val response = themeTeacherRepository.createTheme(themeDTO)
        val viewModelHandle = object :ViewModelHandle<ThemeDTO>{
            override suspend fun onSuccess(data: ThemeDTO) {
                val currentList = _themeList.value
                _themeList.emit(currentList + data)
                _isCreateDialogEnabled.emit(false)
                _deleteEntityId.emit(null)
            }
        }
        response.requestStatusHandle(_state,viewModelHandle)
    }

    fun deleteTheme() = viewModelScope.launch {
        val themeId = deleteEntityId.value ?: return@launch
        val response = themeTeacherRepository.deleteTheme(themeId)
        if (response is RequestStatus.Success){
            val currentList = _themeList.value
            val themeDTO = currentList.first {themeDTO -> themeDTO.id == themeId }
            _themeList.emit(currentList - themeDTO)
            _isDeleteDialogEnabled.emit(false)
            _deleteEntityId.emit(null)
        }
        response.requestStatusHandle(_state)
    }

}