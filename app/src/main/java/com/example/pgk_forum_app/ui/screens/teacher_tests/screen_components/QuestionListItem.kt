package com.example.pgk_forum_app.ui.screens.teacher_tests.screen_components

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterEnd
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.domain.model.test.Question
import com.example.pgk_forum_app.domain.model.test.QuestionType
import com.example.pgk_forum_app.ui.common.AddTextButton
import com.example.pgk_forum_app.ui.common.DropDownSelector
import com.example.pgk_forum_app.ui.common.DeleteIcon
import com.example.pgk_forum_app.ui.common.PGKTextField
import com.example.pgk_forum_app.ui.common.AmidTitle


@Composable
fun QuestionListItem(
    question: Question,
    index: Int,
    changeTitle: (value: String) -> Unit,
    deleteQuestion:() -> Unit,
    deleteOption:(index:Int) -> Unit,
    changeOption: (newValue: String, index: Int) -> Unit,
    addOption: () -> Unit,
    changeRightOption: (value: String) -> Unit,
    changeType: (value: String) -> Unit,
    changeScore:(value:String) -> Unit
) {
    val questionTitle = question.questionTitle
    val questionType = question.type
    val questionTypeList = listOf(
        QuestionType.ONE_OPTION.name,
        QuestionType.TEXT.name
    )
    val rightOptionModifier = Modifier.padding(start = 30.dp,top = 15.dp)

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp, vertical = 8.dp)
    ) {
        Box(
            modifier = Modifier
                .padding(start = 10.dp)
                .align(Alignment.Start)
                .fillMaxWidth(),
        ) {
            Text(
                text = (index+1).toString(),
                fontSize = 14.sp,
                modifier = Modifier
                    .clip(CircleShape)
                    .border(2.dp, MaterialTheme.colorScheme.primary, CircleShape)
                    .size(20.dp)
                    .align(CenterStart),
                textAlign = TextAlign.Center
            )
            DropDownSelector(
                value = questionType,
                valueList = questionTypeList,
                onValueChange = changeType,
                modifier = Modifier
                    .padding(start = 60.dp)
                    .align(CenterStart),
                style = TextStyle(
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Bold
                )
            )

            DeleteIcon(
                onDelete = deleteQuestion,
                modifier = Modifier
                    .align(CenterEnd),
                size = 30.dp
            )
        }
        PGKTextField(
            value = questionTitle,
            textAlign = TextAlign.Center,
            onValueChange = changeTitle,
            fontSize = 18.sp,
            singleLine = false,
            color = Color.Black,
            modifier = Modifier
                .padding(vertical = 20.dp, horizontal = 50.dp)
                .align(CenterHorizontally),
        )

        if (question.type == QuestionType.ONE_OPTION.name){
            Row(
                modifier = Modifier.padding(start = 30.dp),
                verticalAlignment = CenterVertically
            ){
                AmidTitle(
                    titleString = "Варианты ответа",
                    fontSize = 18.sp,
                    singleLine = true,
                )
                Spacer(modifier = Modifier.width(60.dp))
                AddTextButton(onClick = addOption)
            }
            question.options.forEachIndexed { index, optionValue ->
                OptionItem(
                    index = index,
                    optionValue = optionValue,
                    onDelete = {
                        deleteOption(index)
                    },
                    onOptionValueChange = { changeOption(it,index) }
                )
            }
            Row(
                modifier = rightOptionModifier,
                verticalAlignment = CenterVertically
            ) {
                AmidTitle(
                    titleString = "Верный ответ",
                    fontSize = 16.sp,
                    singleLine = true,
                )
                DropDownSelector(
                    value = question.rightAnswer,
                    valueList = question.options,
                    onValueChange = changeRightOption,
                    modifier = Modifier
                        .padding(start = 30.dp),
                    style = TextStyle(
                        fontSize = 14.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
            }
        }else{
            Row(
                modifier = rightOptionModifier,
                verticalAlignment = CenterVertically
            ) {
                AmidTitle(
                    titleString = "Верный ответ",
                    fontSize = 16.sp,
                    singleLine = true,
                )
                Spacer(modifier = Modifier.width(30.dp))
                PGKTextField(
                    value = question.rightAnswer,
                    onValueChange = changeRightOption,
                    fontSize = 14.sp,
                    singleLine = false,
                    color = Color.Black
                )
            }
        }
        Row(
            modifier = rightOptionModifier,
            verticalAlignment = CenterVertically
        ) {
            AmidTitle(
                titleString = "Баллы",
                fontSize = 18.sp,
                singleLine = true,
            )
            Spacer(modifier = Modifier.width(30.dp))
            PGKTextField(
                value = question.score.toString(),
                onValueChange = changeScore,
                fontSize = 18.sp,
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number
                ),
                color = Color.Black,
                modifier = Modifier.fillMaxWidth()
            )
        }
        Spacer(modifier = Modifier.height(20.dp))



    }


}


