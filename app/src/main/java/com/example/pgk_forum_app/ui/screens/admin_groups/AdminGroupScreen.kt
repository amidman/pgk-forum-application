package com.example.pgk_forum_app.ui.screens.admin_groups

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfoWithGroupList
import com.example.pgk_forum_app.ui.common.EditorList
import com.example.pgk_forum_app.ui.navigation.admin.AdminBottomGraphs
import com.example.pgk_forum_app.ui.navigation.admin_group.AdminGroupNavGraphs
import com.example.pgk_forum_app.ui.screens.admin_groups.screen_components.CreateFloatingButton
import com.example.pgk_forum_app.ui.screens.admin_groups.screen_components.GroupInfoSelectedItem
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.annotation.Destination
import kotlinx.coroutines.launch


@OptIn(ExperimentalMaterial3Api::class)
@AdminBottomGraphs(start = true)
@Destination
@Composable
fun AdminGroupScreen(
    groupInfoList: List<GroupInfo>,
    teacherList: List<TeacherInfoWithGroupList>,
    studentList: List<StudentInfo>,
    vm: GroupAdminViewModel,
) {
    val adminGroupNavController = rememberNavController()
    val currentDestination =
        adminGroupNavController.currentBackStackEntryAsState().value?.destination?.route

    var selectedGroupInfo: GroupInfo? by remember { mutableStateOf(null) }
    var isCreateDialogEnabled by remember { mutableStateOf(false) }
    var isDeleteDialogEnabled by remember { mutableStateOf(false) }
    var isError by remember { mutableStateOf(false) }
    var errorMessage: String? by remember { mutableStateOf(null) }
    var isLoading by remember { mutableStateOf(false) }

    collectFlows(onCollect = {
        launch { vm.selectedGroupInfo.collect { selectedGroupInfo = it } }
        launch { vm.isCreateDialogEnabled.collect { isCreateDialogEnabled = it } }
        launch { vm.isDeleteDialogEnabled.collect { isDeleteDialogEnabled = it } }
        launch {
            vm.state.collect {
                isError = it is BaseScreenState.Error
                errorMessage = it.error
                isLoading = it is BaseScreenState.Loading
            }
        }
    })

    val listDestination = AdminGroupNavGraphs.GROUPLIST.name
    val selectedDestination = AdminGroupNavGraphs.SELECTED.name

    Scaffold(
        topBar = {},
        floatingActionButton = {
            if (currentDestination == listDestination) CreateFloatingButton(
                onClick = { vm.setCreateDialog(true) })
        }
    ) {
        NavHost(navController = adminGroupNavController, startDestination = listDestination) {
            composable(listDestination) {
                EditorList(
                    entityList = groupInfoList,
                    isLoading = isLoading,
                    titleString = { value: GroupInfo ->
                        value.groupName
                    },
                    onClick = {
                        vm.selectGroupInfo(it); adminGroupNavController.navigate(
                        selectedDestination
                    )
                    },
                    onDelete = {
                        vm.setGroupDelete(it.id)
                        vm.setDeleteDialog(true)
                    }
                )
            }
            composable(selectedDestination) {
                selectedGroupInfo?.let { groupInfo ->
                    GroupInfoSelectedItem(
                        groupInfo = groupInfo,
                        changeName = { groupName -> vm.changeGroupName(groupName) },
                        changeShortName = { shortName -> vm.changeGroupShortName(shortName) },
                        addTeacher = { teacherLogin -> vm.addTeacherToGroup(teacherLogin = teacherLogin) },
                        deleteTeacher = { teacherInfo -> vm.removeTeacherFromGroup(teacherInfo.login) },
                        addStudent = { studentLogin -> vm.addStudentToGroup(studentLogin = studentLogin) },
                        deleteStudent = { studentInfo -> vm.removeStudentFromGroup(studentInfo.login) },
                        isError = isError,
                        errorMessage = errorMessage,
                        onBack = { adminGroupNavController.navigate(listDestination); vm.backFromSelected() },
                        vm = vm,
                        teacherList = teacherList.map { it.teacherInfo() },
                        studentList = studentList,
                    )
                }
            }
        }


    }
}

