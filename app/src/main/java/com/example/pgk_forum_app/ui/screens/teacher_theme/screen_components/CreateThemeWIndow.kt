package com.example.pgk_forum_app.ui.screens.teacher_theme.screen_components

import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.R
import com.example.pgk_forum_app.domain.model.theme.ThemeDTO
import com.example.pgk_forum_app.ui.dialogs.AddButtons
import com.example.pgk_forum_app.ui.dialogs.DialogErrorTitle
import com.example.pgk_forum_app.ui.dialogs.DialogParamField
import com.example.pgk_forum_app.ui.common.AmidTitle

@Composable
fun ThemeAddWindow(
    onDismiss:() -> Unit,
    onAdd:(theme:ThemeDTO) -> Unit,
    isError:Boolean,
    errorMessage:String?,
    title:String = stringResource(id = R.string.entity_add_title),
) {

    val themeNameParamTitle = "Название темы"
    val shortNameParamTitle = "Описание"
    var themeName:String by remember{
        mutableStateOf("")
    }
    var description:String by remember {
        mutableStateOf("")
    }

    AlertDialog(
        modifier = Modifier.fillMaxHeight().padding(vertical = 60.dp),
        onDismissRequest = {},
        buttons = {
            Box(){
                Column() {
                    AmidTitle(
                        titleString = title,
                        modifier = Modifier.align(Alignment.CenterHorizontally),
                        fontSize = 18.sp,
                        fontWeight = FontWeight.Bold
                    )
                    DialogParamField(
                        title = themeNameParamTitle,
                        value = themeName,
                        onValueChange = {themeName = it}
                    )

                    DialogParamField(
                        title = shortNameParamTitle,
                        value = description,
                        onValueChange = {description = it},
                        singleLine = false,
                        modifier = Modifier
                            .width(350.dp)
                            .sizeIn(maxHeight = 350.dp, minHeight = 250.dp)
                            .padding(vertical = 6.dp, horizontal = 20.dp)
                    )
                    DialogErrorTitle(
                        errorMessage = errorMessage,
                        isError = isError,
                        modifier = Modifier
                            .height(120.dp)
                            .align(Alignment.CenterHorizontally)
                    )
                }
                AddButtons(
                    onAdd = {
                        onAdd(ThemeDTO(themeName = themeName, description = description))
                    },
                    onDismiss = onDismiss
                )
            }
        },

        )
}