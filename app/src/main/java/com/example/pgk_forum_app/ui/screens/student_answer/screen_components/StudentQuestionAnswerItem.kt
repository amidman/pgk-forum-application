package com.example.pgk_forum_app.ui.screens.student_answer.screen_components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.domain.model.student_answer.QuestionAnswer
import com.example.pgk_forum_app.domain.model.student_answer.StudentAnswer
import com.example.pgk_forum_app.domain.model.test.QuestionType
import com.example.pgk_forum_app.ui.common.AmidTitle
import com.example.pgk_forum_app.ui.theme.PurpleGrey80
import java.util.*


private fun String.toAnswerString(): String {
    return this.lowercase(Locale.getDefault()).trim()
}

@Composable
fun StudentQuestionAnswerItem(
    questionIndex: Int,
    questionAnswer: QuestionAnswer
) {

    val editFieldModifier = Modifier
        .padding(start = 20.dp, top = 10.dp, end = 20.dp)
        .fillMaxWidth()
    Card(
        Modifier
            .fillMaxWidth()
            .padding(top = 20.dp, start = 20.dp, end = 20.dp)
    ) {
        Box() {
            Column(
                modifier = Modifier
                    .padding(bottom = 70.dp)
                    .fillMaxSize()
            ) {
                AmidTitle(
                    titleString = "Вопрос ${questionIndex + 1}",
                    modifier = Modifier
                        .padding(20.dp),
                )
                AmidTitle(
                    titleString = questionAnswer.questionTitle,
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .padding(horizontal = 5.dp),
                    singleLine = false,
                    fontSize = 22.sp,
                    textAlign = TextAlign.Center
                )
                if (questionAnswer.questionType == QuestionType.ONE_OPTION.name)
                    OptionAnswerList(
                        questionAnswer = questionAnswer,
                        itemModifier = Modifier
                            .padding(vertical = 5.dp, horizontal = 15.dp)
                            .fillMaxWidth()
                            .sizeIn(minHeight = 50.dp),
                    )
                else
                    StudentAnswerTextField(
                        modifier = editFieldModifier,
                        questionAnswer = questionAnswer
                    )
//                    AnswerTextField(
//                        modifier = editFieldModifier,
//                        value = currentValue,
//                        onValueChange = { vm.changeAnswer(it) },
//                    )
                ResultField(
                    questionAnswer = questionAnswer,
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .padding(top = 10.dp, start = 20.dp, end = 20.dp)
                        .fillMaxWidth()
                        .height(50.dp),

                    )
            }


        }

    }
}


@Composable
fun StudentAnswerTextField(
    questionAnswer: QuestionAnswer,
    modifier: Modifier = Modifier
) {
    val itemModifier = Modifier
        .padding(vertical = 5.dp,)
        .fillMaxWidth()
        .sizeIn(minHeight = 50.dp)
    Column(modifier = modifier){
        AmidTitle(
            titleString = "Ваш Ответ",
            fontSize = 18.sp
        )
        OptionAnswerItem(
            isRight = false,
            isSelected = false,
            value = questionAnswer.studentAnswer,
            modifier = itemModifier
        )
        AmidTitle(
            titleString = "Правильный Ответ",
            fontSize = 18.sp
        )
        OptionAnswerItem(
            isRight = false,
            isSelected = false,
            value = questionAnswer.rightAnswer,
            modifier = itemModifier
        )
    }

}

@Composable
fun ResultField(
    questionAnswer: QuestionAnswer,
    modifier: Modifier,
) {
    val isRight =
        questionAnswer.studentAnswer.toAnswerString() == questionAnswer.rightAnswer.toAnswerString()
    val (containerColor, contentColor) =
        if (isRight)
            Pair(Color.Green, Color.White)
        else
            Pair(
                MaterialTheme.colorScheme.error,
                Color.White
            )
    val titleString =
        if (isRight)
            "Верно, баллы за этот вопрос: ${questionAnswer.questionScore}"
        else
            "Неверный ответ, баллы за этот вопрос: 0"
    Card(
        modifier = modifier,
        colors = CardDefaults.cardColors(
            containerColor = containerColor,
        )
    ) {
        Box(modifier = Modifier.fillMaxSize()) {
            AmidTitle(
                titleString = titleString,
                color = contentColor,
                fontSize = 18.sp,
                modifier = Modifier.align(
                    Alignment.Center
                ),
                singleLine = false,
                textAlign = TextAlign.Center
            )
        }
    }
}

@Composable
fun OptionAnswerList(
    questionAnswer: QuestionAnswer,
    itemModifier: Modifier = Modifier,
) {


    val optionList = questionAnswer.questionOptions
    optionList.forEach { optionValue ->
        OptionAnswerItem(
            modifier = itemModifier,
            isRight = optionValue == questionAnswer.rightAnswer,
            isSelected = questionAnswer.studentAnswer.toAnswerString() == optionValue.toAnswerString(),
            value = optionValue,
        )
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OptionAnswerItem(
    modifier: Modifier = Modifier,
    isRight: Boolean,
    isSelected: Boolean,
    value: String,
) {

    val (containerColor, textColor) = when {
        isRight -> Pair(Color.Green, Color.White)
        !isRight && isSelected -> Pair(
            MaterialTheme.colorScheme.error,
            Color.White
        )
        else -> Pair(PurpleGrey80, MaterialTheme.colorScheme.primary)
    }

    Box(
        modifier = modifier
            .clip(RoundedCornerShape(100))
            .background(containerColor),
    ) {
        AmidTitle(
            titleString = value,
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .padding(start = 20.dp)
                .align(Alignment.CenterStart),
            color = textColor,
            singleLine = false
        )
    }


}