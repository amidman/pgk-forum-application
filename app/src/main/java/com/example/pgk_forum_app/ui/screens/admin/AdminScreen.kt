package com.example.pgk_forum_app.ui.screens.admin

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfoWithGroupList
import com.example.pgk_forum_app.ui.common.EntityBottomNavigation
import com.example.pgk_forum_app.ui.navigation.admin.AdminBottomNavGraphs
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.admin_groups.AdminGroupScreen
import com.example.pgk_forum_app.ui.screens.admin_groups.GroupAdminViewModel
import com.example.pgk_forum_app.ui.screens.admin_students.AdminStudentScreen
import com.example.pgk_forum_app.ui.screens.admin_students.StudentAdminViewModel
import com.example.pgk_forum_app.ui.screens.admin_teacher.AdminTeacherScreen
import com.example.pgk_forum_app.ui.screens.admin_teacher.TeacherAdminViewModel
import com.example.pgk_forum_app.ui.screens.destinations.AdminGroupScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.AdminStudentScreenDestination
import com.example.pgk_forum_app.ui.screens.destinations.AdminTeacherScreenDestination
import com.example.pgk_forum_app.ui.util.collectFlows
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.manualcomposablecalls.composable
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel


@RootNavGraph
@Destination
@Composable
fun AdminScreen() {

    val groupViewModel = getViewModel<GroupAdminViewModel>()
    val teacherAdminViewModel = getViewModel<TeacherAdminViewModel>()
    val studentAdminViewModel = getViewModel<StudentAdminViewModel>()
    val adminNav = rememberNavController()
    val currentDestination = adminNav.currentBackStackEntryAsState().value?.destination?.route


    var groupInfoList: List<GroupInfo> by remember { mutableStateOf(listOf()) }
    var teacherInfoWithGroupList: List<TeacherInfoWithGroupList> by remember { mutableStateOf(listOf()) }
    var studentInfoList: List<StudentInfo> by remember { mutableStateOf(listOf()) }

    collectFlows(onCollect = {
        launch { groupViewModel.groupInfoList.collect { groupInfoList = it } }
        launch { teacherAdminViewModel.teacherInfoList.collect { teacherInfoWithGroupList = it } }
        launch { studentAdminViewModel.studentInfoList.collect { studentInfoList = it } }
    })


    Scaffold(
        bottomBar = {
            EntityBottomNavigation(
                adminNav,
                AdminBottomNavGraphs
            )
        }
    ) {

        DestinationsNavHost(
            navGraph = NavGraphs.adminBottomGraphs,
            navController = adminNav,
            modifier = Modifier.padding(it)
        ){
            composable(AdminGroupScreenDestination) {
                groupViewModel.getAllGroups()
                AdminGroupScreen(
                    groupInfoList = groupInfoList,
                    teacherList = teacherInfoWithGroupList,
                    studentList = studentInfoList,
                    vm = groupViewModel,
                )
            }
            composable(AdminTeacherScreenDestination) {
                teacherAdminViewModel.getAllTeachers()
                AdminTeacherScreen(
                    teacherInfoWithGroupList = teacherInfoWithGroupList,
                    vm = teacherAdminViewModel,
                    groupInfoList = groupInfoList
                )
            }
            composable(AdminStudentScreenDestination) {
                studentAdminViewModel.getAllStudentInfo()
                AdminStudentScreen(
                    studentInfoList = studentInfoList,
                    vm = studentAdminViewModel,
                    groupViewModel = groupViewModel,
                    groupInfoList = groupInfoList,
                )
            }
        }

    }
}