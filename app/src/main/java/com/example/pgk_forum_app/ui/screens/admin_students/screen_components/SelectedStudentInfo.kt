package com.example.pgk_forum_app.ui.screens.admin_students.screen_components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import com.example.pgk_forum_app.domain.model.group.GroupInfo
import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.ui.common.ChangeParamField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.screens.admin_students.StudentAdminViewModel
import com.example.pgk_forum_app.ui.util.BaseScreenState
import com.example.pgk_forum_app.ui.util.collectFlow
import kotlinx.coroutines.flow.onEach


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SelectedStudentInfo(
    studentInfo: StudentInfo,
    changeFio:(value:String)->Unit,
    changeLogin:(value:String)->Unit,
    changePassword:(value:String)->Unit,
    changeGroup:(groupName:String?) -> Unit,
    isError:Boolean,
    errorMessage:String?,
    onBack:() -> Unit,
    vm:StudentAdminViewModel,
    groupList:List<GroupInfo>,
) {
    val scrollState = rememberScrollState()
    val isLogin = remember{ mutableStateOf(false)}
    val isPassword = remember{ mutableStateOf(false)}
    val isFio = remember{ mutableStateOf(false)}
    val isGroup = remember{ mutableStateOf(false)}
    vm.state.onEach {
        if (it is BaseScreenState.Success){
            isLogin.value = false
            isFio.value = false
            isGroup.value = false
            isPassword.value = false
        }
    }.collectFlow()
    
    Scaffold(topBar = {
            SelectedEntityTopBar(title = "Студент ${studentInfo.fio}",onBack=onBack)
    }) {
        Column(
            Modifier
                .fillMaxSize()
                .padding(top = it.calculateTopPadding())
                .verticalScroll(scrollState)) {
            ChangeParamField(
                paramValue = studentInfo.fio,
                paramTitle = "ФИО",
                onClick = {},
                onChange = changeFio,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isFio
            )
            ChangeParamField(
                paramValue = studentInfo.login,
                paramTitle = "Логин",
                onClick = { },
                onChange = changeLogin,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isLogin
            )
            ChangeParamField(
                paramValue = "Измените Значение",
                paramTitle = "Пароль",
                onClick = { },
                onChange = changePassword,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isPassword
            )
            ChangeParamField(
                paramValue = studentInfo.groupName ?: "Отсутствует",
                paramTitle = "Группа студента",
                onClick = { },
                onChange = changeGroup,
                isError = isError,
                errorMessage = errorMessage,
                isDialogEnabled = isGroup,
                isEntityField = true,
                entityStringList = groupList.map { it.groupName }
            )
    }
    
        

    }
}