package com.example.pgk_forum_app.ui.screens.student_group.screen_components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.pgk_forum_app.domain.model.group.GroupDTO
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfoWithGroupList
import com.example.pgk_forum_app.ui.common.ChangeParamField
import com.example.pgk_forum_app.ui.common.SelectedEntityTopBar
import com.example.pgk_forum_app.ui.navigation.student_group.StudentGroupGraphs
import com.example.pgk_forum_app.ui.screens.admin_students.screen_components.ChangeParamList
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.result.ResultBackNavigator

@OptIn(ExperimentalMaterial3Api::class)
@StudentGroupGraphs
@Destination
@Composable
fun SelectedTeacher(
    teacherInfo: TeacherInfoWithGroupList,
    resultBackNavigator: ResultBackNavigator<Boolean>
) {

    Scaffold(topBar = {
        SelectedEntityTopBar(title = "Учитель ${teacherInfo.fio}", onBack = {
            resultBackNavigator.navigateBack()
        })
    }) {
        Column(
            Modifier
                .fillMaxSize()
                .padding(top = it.calculateTopPadding())
        ) {
            ChangeParamField(
                paramValue = teacherInfo.fio,
                paramTitle = "ФИО",
                isEditMode = false
            )
            ChangeParamField(
                paramValue = teacherInfo.login,
                paramTitle = "Логин",
                isEditMode = false
            )
            Column(modifier = Modifier.height(350.dp)) {
                ChangeParamList<GroupDTO>(
                    paramTitle = "Группы",
                    itemList = teacherInfo.groups,
                    itemListString = teacherInfo.groups.map { groupDTO -> groupDTO.name },
                    emptyItemTitle = "У учителя отсутствуют группы",
                    isEditMode = false,
                )
            }
        }

    }
}