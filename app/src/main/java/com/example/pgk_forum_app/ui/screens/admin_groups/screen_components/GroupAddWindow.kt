package com.example.pgk_forum_app.ui.screens.admin_groups.screen_components

import androidx.compose.foundation.layout.*
import com.example.pgk_forum_app.R
import androidx.compose.material.AlertDialog
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.domain.model.group.GroupDTO
import com.example.pgk_forum_app.ui.dialogs.AddButtons
import com.example.pgk_forum_app.ui.dialogs.DialogErrorTitle
import com.example.pgk_forum_app.ui.dialogs.DialogParamField
import com.example.pgk_forum_app.ui.common.AmidTitle

@Composable
fun GroupAddWindow(
    onDismiss:() -> Unit,
    onAdd:(group:GroupDTO) -> Unit,
    isError:Boolean,
    errorMessage:String?,
    title:String = stringResource(id = R.string.entity_add_title),
) {

    val nameParamTitle = "Название Группы"
    val shortNameParamTitle = "Короткое название"
    var name:String by remember{
        mutableStateOf("")
    }
    var shortName:String by remember {
        mutableStateOf("")
    }

    AlertDialog(
        onDismissRequest = {},
        buttons = {
            Box(){
                Column() {
                    AmidTitle(
                        titleString = title,
                        modifier = Modifier.align(Alignment.CenterHorizontally),
                        fontSize = 18.sp,
                        fontWeight = FontWeight.Bold
                    )
                    DialogParamField(
                        title = nameParamTitle,
                        value = name,
                        onValueChange = {name = it}
                    )

                    DialogParamField(
                        title = shortNameParamTitle,
                        value = shortName,
                        onValueChange = {shortName = it}
                    )

                    DialogErrorTitle(
                        errorMessage = errorMessage,
                        isError = isError,
                        modifier = Modifier
                            .height(120.dp)
                            .align(Alignment.CenterHorizontally)
                    )

                }
                AddButtons(
                    onAdd = {
                        onAdd(GroupDTO(name = name, shortName = shortName))
                    },
                    onDismiss = onDismiss
                )
            }
        },

        )
}