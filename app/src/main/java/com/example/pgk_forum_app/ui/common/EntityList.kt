package com.example.pgk_forum_app.ui.common

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun <T> EntityList(
    modifier: Modifier = Modifier,
    entityList:List<T>,
    isLoading: Boolean,
    entityItem:@Composable LazyItemScope.(index: Int, entity: T) -> Unit
) {
    if (entityList.isEmpty() && isLoading) {
        Box(modifier = modifier.fillMaxWidth())
        {
            CircularProgressIndicator(
                modifier = Modifier
                    .align(Alignment.TopCenter)
                    .padding(top = 200.dp)
            )
        }
    }
    else {
        LazyColumn(modifier = modifier.fillMaxSize()) {
            itemsIndexed(entityList) { index, entity ->
                entityItem(this,index, entity)
            }
        }
    }
}