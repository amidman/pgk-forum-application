package com.example.pgk_forum_app.ui.common

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import com.example.pgk_forum_app.R

@Composable
fun DeleteIcon(onDelete:()->Unit,modifier:Modifier = Modifier,size: Dp? = null) {
    val deletePainterResource = painterResource(id = R.drawable.ic_baseline_delete_24)
    IconButton(
        modifier = modifier,
        onClick = onDelete
    ) {
        Icon(painter = deletePainterResource, contentDescription = null, modifier = size?.let { Modifier.size(size) } ?: Modifier)
    }
}


@Composable
fun ChangeIcon(onChange:()->Unit,modifier: Modifier=Modifier) {
    val changePainterResource = painterResource(id = R.drawable.ic_baseline_edit_24)
    IconButton(
        modifier = modifier,
        onClick = onChange
    ) {
        Icon(painter = changePainterResource, contentDescription = null)
    }
}