package com.example.pgk_forum_app.ui.screens.student_test

import androidx.compose.runtime.Composable
import com.example.pgk_forum_app.ui.navigation.student.StudentBottomGraphs
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.destinations.SelectedStudentTestDestination
import com.example.pgk_forum_app.ui.screens.student_answer.StudentAnswerViewModel
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.dependency

@StudentBottomGraphs
@Destination
@Composable
fun StudentTestScreen(
    vm: StudentTestViewModel,
    createTestAnswerVM: CreateStudentAnswerViewModel,
    studentAnswerVM:StudentAnswerViewModel
) {


    DestinationsNavHost(
        navGraph = NavGraphs.studentTestGraphs,
        dependenciesContainerBuilder = {
            dependency(vm)
            dependency(createTestAnswerVM)
            dependency(SelectedStudentTestDestination){studentAnswerVM}
        }
    )
}