package com.example.pgk_forum_app.ui.navigation.admin_group

import com.ramcosta.composedestinations.annotation.NavGraph


enum class AdminGroupNavGraphs{
    GROUPLIST,
    SELECTED
}