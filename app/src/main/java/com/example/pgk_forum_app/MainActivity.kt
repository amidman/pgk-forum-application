package com.example.pgk_forum_app

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.pgk_forum_app.ui.navigation.main.MainNavGraphs
import com.example.pgk_forum_app.ui.navigation.main.MainNavigation
import com.example.pgk_forum_app.ui.screens.NavGraphs
import com.example.pgk_forum_app.ui.screens.admin.AdminScreen
import com.example.pgk_forum_app.ui.screens.destinations.LoginScreenDestination
import com.example.pgk_forum_app.ui.screens.login.LoginScreen
import com.example.pgk_forum_app.ui.screens.student.StudentScreen
import com.example.pgk_forum_app.ui.screens.teacher.TeacherScreen
import com.example.pgk_forum_app.ui.theme.PGK_FORUM_APPTheme
import com.example.pgk_forum_app.ui.util.collectFlow
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.navigation.navigate
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.compose.inject

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PGK_FORUM_APPTheme(darkTheme = false) {
                val mainNav = rememberNavController()

                val mainNavigation by inject<MainNavigation>()

                mainNavigation.navFlow.onEach {
                    mainNav.navigate(it)
                }.collectFlow()
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    DestinationsNavHost(navGraph = NavGraphs.root)
                }
            }
        }
    }
}
