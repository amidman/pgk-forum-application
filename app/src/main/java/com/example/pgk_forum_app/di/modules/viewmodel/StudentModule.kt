package com.example.pgk_forum_app.di.modules.viewmodel

import com.example.pgk_forum_app.ui.screens.student_answer.StudentAnswerViewModel
import com.example.pgk_forum_app.ui.screens.student_group.StudentGroupViewModel
import com.example.pgk_forum_app.ui.screens.student_test.CreateStudentAnswerViewModel
import com.example.pgk_forum_app.ui.screens.student_test.StudentTestViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val studentModule = module {

    viewModel<StudentGroupViewModel> {
        StudentGroupViewModel(
            groupStudentRepository = get()
        )
    }

    viewModel<StudentTestViewModel>{
        StudentTestViewModel(
            testRepository = get(),
            teacherStudentRepository = get()
        )
    }

    viewModel<CreateStudentAnswerViewModel>{
        CreateStudentAnswerViewModel()
    }

    viewModel<StudentAnswerViewModel>{
        StudentAnswerViewModel(
            studentAnswerStudentRepository = get()
        )
    }

}