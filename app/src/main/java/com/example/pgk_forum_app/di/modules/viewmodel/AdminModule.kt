package com.example.pgk_forum_app.di.modules.viewmodel

import com.example.pgk_forum_app.ui.screens.admin_groups.GroupAdminViewModel
import com.example.pgk_forum_app.ui.screens.admin_students.StudentAdminViewModel
import com.example.pgk_forum_app.ui.screens.admin_teacher.TeacherAdminViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val adminModule = module {


    viewModel<GroupAdminViewModel> {
        GroupAdminViewModel(
            groupRepository = get(),
            groupAdminRepository = get(),
            studentAdminRepository = get()
        )
    }

    viewModel<StudentAdminViewModel> {
        StudentAdminViewModel(
            studentRepository = get(),
            userAdminRepository = get(),
            studentAdminRepository = get()
        )
    }

    viewModel<TeacherAdminViewModel> {
        TeacherAdminViewModel(
            teacherRepository = get(),
            userAdminRepository = get(),
            groupAdminRepository = get()
        )
    }
}