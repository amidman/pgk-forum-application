package com.example.pgk_forum_app.di.modules

import com.example.pgk_forum_app.ui.navigation.main.MainNavigation
import org.koin.dsl.module

val navigationModule = module {
    single<MainNavigation> {
        MainNavigation()
    }
}