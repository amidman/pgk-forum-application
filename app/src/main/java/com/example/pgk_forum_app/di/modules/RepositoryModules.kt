package com.example.pgk_forum_app.di.modules

import com.example.pgk_forum_app.di.modules.repository.*
import org.koin.core.KoinApplication

fun KoinApplication.repositoryModules(){
    modules(
        authRepositoryModule,
        groupRepositoryModule,
        studentAnswerRepositoryModule,
        studentRepositoryModule,
        teacherRepositoryModule,
        testRepositoryModule,
        themeRepositoryModule,
        userRepositoryModule
    )
}