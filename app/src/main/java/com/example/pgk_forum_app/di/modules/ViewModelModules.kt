package com.example.pgk_forum_app.di.modules

import com.example.pgk_forum_app.di.modules.viewmodel.adminModule
import com.example.pgk_forum_app.di.modules.viewmodel.studentModule
import com.example.pgk_forum_app.di.modules.viewmodel.teacherModule
import org.koin.core.KoinApplication


fun KoinApplication.viewModelModules(){
    modules(
        loginModule,
        adminModule,
        studentModule,
        teacherModule
    )
}