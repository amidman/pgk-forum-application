package com.example.pgk_forum_app.di.modules.repository

import com.example.pgk_forum_app.data.api.student.StudentAdminRepository
import com.example.pgk_forum_app.data.api.student.StudentRepository
import org.koin.dsl.module

val studentRepositoryModule = module {
    single<StudentRepository>{
        StudentRepository(
            client = get(),
            tokenRepository = get()
        )
    }
    single<StudentAdminRepository> {
        StudentAdminRepository(
            client = get(),
            tokenRepository = get()
        )
    }


}