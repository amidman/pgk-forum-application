package com.example.pgk_forum_app.di.modules.repository

import com.example.pgk_forum_app.data.api.test.TestRepository
import com.example.pgk_forum_app.data.api.test.TestTeacherRepository
import org.koin.dsl.module

val testRepositoryModule = module {
    single<TestRepository>{
        TestRepository(
            client = get(),
            tokenRepository = get()
        )
    }

    single<TestTeacherRepository>{
        TestTeacherRepository(
            client = get(),
            tokenRepository = get()
        )
    }
}