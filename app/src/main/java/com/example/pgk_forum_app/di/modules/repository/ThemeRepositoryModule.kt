package com.example.pgk_forum_app.di.modules.repository

import com.example.pgk_forum_app.data.api.theme.ThemeTeacherRepository
import org.koin.dsl.module

val themeRepositoryModule = module {

    single<ThemeTeacherRepository> {
        ThemeTeacherRepository(
            client = get(),
            tokenRepository = get()
        )
    }

}