package com.example.pgk_forum_app.di.modules

import com.example.pgk_forum_app.data.api.user.UserAdminRepository
import com.example.pgk_forum_app.data.api.user.UserRepository
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import com.example.pgk_forum_app.ui.screens.login.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module {
    viewModel<LoginViewModel> {
        LoginViewModel(
            userRepository = get(),
        )
    }

}