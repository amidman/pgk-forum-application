package com.example.pgk_forum_app.di.modules.repository

import com.example.pgk_forum_app.data.api.student_answers.StudentAnswerStudentRepository
import com.example.pgk_forum_app.data.api.student_answers.StudentAnswerTeacherRepository
import org.koin.dsl.module

val studentAnswerRepositoryModule = module {
    single<StudentAnswerTeacherRepository>{
        StudentAnswerTeacherRepository(
            client = get(),
            tokenRepository = get()
        )
    }
    single<StudentAnswerStudentRepository>{
        StudentAnswerStudentRepository(
            tokenRepository = get(),
            client = get()
        )
    }
}