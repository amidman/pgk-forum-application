package com.example.pgk_forum_app.di.modules.repository

import com.example.pgk_forum_app.data.api.teacher.TeacherAdminRepository
import com.example.pgk_forum_app.data.api.teacher.TeacherRepository
import com.example.pgk_forum_app.data.api.teacher.TeacherStudentRepository
import org.koin.dsl.module

val teacherRepositoryModule = module {
    single<TeacherRepository> {
        TeacherRepository(
            client = get(),
            tokenRepository = get(),
        )
    }
    single<TeacherAdminRepository> {
        TeacherAdminRepository(
            client = get(),
            tokenRepository = get(),
        )
    }
    single<TeacherStudentRepository>{
        TeacherStudentRepository(
            client = get(),
            tokenRepository = get()
        )
    }
}