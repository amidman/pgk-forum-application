package com.example.pgk_forum_app.di.modules.repository

import com.example.pgk_forum_app.data.api.group.GroupAdminRepository
import com.example.pgk_forum_app.data.api.group.GroupRepository
import com.example.pgk_forum_app.data.api.group.GroupStudentRepository
import com.example.pgk_forum_app.data.api.group.GroupTeacherRepository
import org.koin.dsl.module

val groupRepositoryModule = module {
    single<GroupRepository>{
        GroupRepository(
            client = get(),
            tokenRepository = get()
        )
    }
    single<GroupAdminRepository>{
        GroupAdminRepository(
            client = get(),
            tokenRepository = get()
        )
    }
    single<GroupStudentRepository>{
        GroupStudentRepository(
            client = get(),
            tokenRepository = get(),
        )
    }
    single<GroupTeacherRepository>{
        GroupTeacherRepository(
            client = get(),
            tokenRepository = get()
        )
    }
}