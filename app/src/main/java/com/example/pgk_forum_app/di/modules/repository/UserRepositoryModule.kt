package com.example.pgk_forum_app.di.modules.repository

import com.example.pgk_forum_app.data.api.user.UserAdminRepository
import com.example.pgk_forum_app.data.api.user.UserRepository
import org.koin.dsl.module

val userRepositoryModule = module {
    single<UserRepository> {
        UserRepository(
            client = get(),
            tokenRepository = get()
        )
    }
    single<UserAdminRepository> {
        UserAdminRepository(
            client = get(),
            tokenRepository = get()
        )
    }
}