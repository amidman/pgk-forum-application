package com.example.pgk_forum_app.di

import android.app.Application
import com.example.pgk_forum_app.di.modules.basicModules
import com.example.pgk_forum_app.di.modules.repositoryModules
import com.example.pgk_forum_app.di.modules.viewModelModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class KoinApplication:Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@KoinApplication)
            basicModules()
            repositoryModules()
            viewModelModules()
        }
    }
}