package com.example.pgk_forum_app.di.modules.repository

import com.example.pgk_forum_app.data.api.user.UserAdminRepository
import com.example.pgk_forum_app.data.api.user.UserRepository
import com.example.pgk_forum_app.data.datastore.auth.TokenRepository
import org.koin.dsl.module

val authRepositoryModule = module {
    single<TokenRepository> {
        TokenRepository(
            context = get(),
        )
    }

}