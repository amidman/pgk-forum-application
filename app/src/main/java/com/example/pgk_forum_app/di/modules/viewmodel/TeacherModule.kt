package com.example.pgk_forum_app.di.modules.viewmodel

import com.example.pgk_forum_app.ui.screens.teacher_groups.TeacherGroupViewModel
import com.example.pgk_forum_app.ui.screens.teacher_tests.CreateTestViewModel
import com.example.pgk_forum_app.ui.screens.teacher_tests.TeacherTestViewModel
import com.example.pgk_forum_app.ui.screens.teacher_theme.TeacherThemeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val teacherModule = module {

    viewModel<TeacherGroupViewModel>(){
        TeacherGroupViewModel(
            groupTeacherRepository = get()
        )
    }

    viewModel<CreateTestViewModel>{
        CreateTestViewModel()
    }

    viewModel<TeacherTestViewModel>{
        TeacherTestViewModel(
            testTeacherRepository = get()
        )
    }

    viewModel<TeacherThemeViewModel>{
        TeacherThemeViewModel(
            themeTeacherRepository = get()
        )
    }

}