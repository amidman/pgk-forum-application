package com.example.pgk_forum_app.di.modules

import com.example.pgk_forum_app.di.modules.basic.apiModule
import org.koin.core.KoinApplication


fun KoinApplication.basicModules(){
    modules(
        apiModule,
        navigationModule
    )
}