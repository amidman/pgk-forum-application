package com.example.pgk_forum_app.domain.model.student

import kotlinx.serialization.Serializable


@Serializable
data class StudentDTO(
    val id:Int? = null,
    val groupName:String? = null,
    val userId:Int,
)
