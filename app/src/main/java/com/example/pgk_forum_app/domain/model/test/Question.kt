package com.example.pgk_forum_app.domain.model.test

import com.example.pgk_forum_app.domain.model.student_answer.QuestionAnswer
import kotlinx.serialization.Serializable


@Serializable
data class Question(
    val id:Int? = null,
    val number:Int,
    val testId:Int,
    val rightAnswer:String,
    val type:String,
    val score:Int,
    val options:List<String>,
    val questionTitle:String,
){
    fun testQuestionDTO(): TestQuestionDTO = TestQuestionDTO(id, number, testId, rightAnswer, type, score = score, questionTitle = questionTitle)

    fun testQuestionDTO(testId: Int) = TestQuestionDTO(id, number, testId, rightAnswer, type, score=score, questionTitle = questionTitle)

    fun mapToQuestionAnswer() =
        QuestionAnswer(
            id = null,
            studentAnswerId = null,
            questionId = id ?: 0,
            questionType = type,
            questionNumber = number,
            questionScore = score,
            questionTitle = questionTitle,
            rightAnswer = rightAnswer,
            studentAnswer = "",
            questionOptions = options
        )


    companion object {
        fun fromTestQuestionDTO(dto: TestQuestionDTO, options: List<String>) =
            Question(
                id = dto.id,
                number = dto.number,
                testId = dto.testId,
                rightAnswer = dto.rightAnswer,
                type = dto.type,
                score = dto.score,
                questionTitle = dto.questionTitle,
                options = options,
            )
    }
}


