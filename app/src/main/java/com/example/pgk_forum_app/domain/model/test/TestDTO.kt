package com.example.pgk_forum_app.domain.model.test

import kotlinx.serialization.Serializable
import java.time.LocalDateTime


@Serializable
data class TestDTO(
    val id:Int? = null,
    val teacherId:Int,
    val title:String,
    val themeName:String,
    val createdAt:String,
    val maxScore:Int = 0,
)
