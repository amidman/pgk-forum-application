package com.example.pgk_forum_app.domain.utils

enum class ROLES  {
    GUEST,STUDENT,TEACHER,ADMIN
}