package com.example.pgk_forum_app.domain.model.test

import kotlinx.serialization.Serializable
import java.time.LocalDateTime


@Serializable
data class TestInfo(
    val id:Int? = null,
    val teacherId:Int,
    val title:String,
    val themeName:String,
    val maxScore:Int = 0,
    val createdAt:String,
    val questions:List<Question>,
){

    fun testDTO(): TestDTO = TestDTO(id, teacherId, title, themeName,createdAt, calcMaxScore())

    fun calcMaxScore():Int{
        return questions.map{it.score}.reduce { acc, i -> acc + i }
    }

    companion object {
        fun fromTestDTO(dto: TestDTO, questions: List<Question>) =
            TestInfo(
                id = dto.id,
                teacherId = dto.teacherId,
                title = dto.title,
                themeName = dto.themeName,
                maxScore = dto.maxScore,
                createdAt = dto.createdAt,
                questions = questions
            )
    }
}

