package com.example.pgk_forum_app.domain.model.group

import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfoWithGroupList
import kotlinx.serialization.Serializable


@Serializable
data class GroupInfoWithTeachersGroup(
    val id:Int? = null,
    val teachers:List<TeacherInfoWithGroupList>,
    val groupName:String,
    val groupShortName:String,
    val students:List<StudentInfo>
){
    companion object{
        fun fromParts(groupInfo: GroupInfo, teachers:List<TeacherInfoWithGroupList>) =
            GroupInfoWithTeachersGroup(
                id = groupInfo.id,
                teachers = teachers,
                groupName = groupInfo.groupName,
                groupShortName = groupInfo.groupShortName,
                students = groupInfo.students
            )
    }
}
