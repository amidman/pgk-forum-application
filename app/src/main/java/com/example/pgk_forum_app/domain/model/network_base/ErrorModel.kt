package com.example.pgk_forum_app.domain.model.network_base

import kotlinx.serialization.Serializable


@Serializable
data class ErrorModel(
    val code:String,
    val description:String
)
