package com.example.pgk_forum_app.domain.model.teacher


import com.example.pgk_forum_app.domain.model.group.GroupDTO
import kotlinx.serialization.Serializable


@Serializable
data class TeacherInfoWithGroupList(
    val userId:Int? = 0,
    val fio:String,
    val login:String,
    val role:String,
    val teacherId:Int,
    val groups:List<GroupDTO> = listOf()
){
    fun teacherInfo() = TeacherInfo(userId, fio, login, role, teacherId)


    companion object{
        fun fromParts(teacherInfo:TeacherInfo,groups: List<GroupDTO>):TeacherInfoWithGroupList{
            return TeacherInfoWithGroupList(
                userId = teacherInfo.userId,
                fio = teacherInfo.fio,
                login = teacherInfo.login,
                role = teacherInfo.role,
                teacherId = teacherInfo.teacherId,
                groups = groups,
            )
        }
    }
}