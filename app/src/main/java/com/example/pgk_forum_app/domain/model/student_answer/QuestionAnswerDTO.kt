package com.example.pgk_forum_app.domain.model.student_answer

import kotlinx.serialization.Serializable


@Serializable
data class QuestionAnswerDTO(
    val id:Int? = null,
    val studentAnswerId:Int,
    val questionId:Int,
    val studentAnswer:String,
)
