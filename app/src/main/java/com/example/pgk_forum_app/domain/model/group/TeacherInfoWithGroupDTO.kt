package com.example.pgk_forum_app.domain.model.group

import com.example.pgk_forum_app.domain.model.group.GroupDTO
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import kotlinx.serialization.Serializable


@Serializable
data class TeacherInfoWithGroupDTO(
    val teacherInfo: TeacherInfo,
    val groupDTO: GroupDTO,
)