package com.example.pgk_forum_app.domain.model.test

import kotlinx.serialization.Serializable


@Serializable
data class QuestionOptionDTO(
    val id:Int? = null,
    val questionId:Int,
    val value:String,
)
