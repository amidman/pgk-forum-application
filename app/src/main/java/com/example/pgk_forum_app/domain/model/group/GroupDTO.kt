package com.example.pgk_forum_app.domain.model.group

import kotlinx.serialization.Serializable

@Serializable
data class GroupDTO(
    val id:Int? = null,
    val name:String,
    val shortName:String,
)