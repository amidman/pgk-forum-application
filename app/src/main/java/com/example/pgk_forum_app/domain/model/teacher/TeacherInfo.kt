package com.example.pgk_forum_app.domain.model.teacher

import kotlinx.serialization.Serializable


@Serializable
data class TeacherInfo(
    val userId:Int? = 0,
    val fio:String,
    val login:String,
    val role:String,
    val teacherId:Int,
)