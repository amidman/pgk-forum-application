package com.example.pgk_forum_app.domain.model.user

import kotlinx.serialization.Serializable


@Serializable
data class UserDTO(
    val id:Int? = null,
    val fio:String,
    val login:String,
    val password:String,
    val role:String
)
