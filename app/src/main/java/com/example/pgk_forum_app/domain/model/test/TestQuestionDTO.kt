package com.example.pgk_forum_app.domain.model.test

import kotlinx.serialization.Serializable


@Serializable
data class TestQuestionDTO(
    val id:Int? = null,
    val number:Int,
    val testId:Int,
    val rightAnswer:String,
    val type:String,
    val questionTitle:String,
    val score:Int,
)
