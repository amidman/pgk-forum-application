package com.example.pgk_forum_app.domain.utils

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


fun String.toDateString():String = try {
    val localDateTime = LocalDateTime.parse(this)
    val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
    localDateTime.format(formatter)
}catch (e:Exception){
    "Ошибка при конвертации"
}