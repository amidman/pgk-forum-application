package com.example.pgk_forum_app.domain.model.student_answer

import kotlinx.serialization.Serializable
import java.time.LocalDateTime
import java.util.*


private fun String.toAnswerString():String{
    return this.lowercase(Locale.getDefault()).trim()
}


@Serializable
data class StudentAnswer(
    val id:Int? = null,
    val studentId:Int,
    val testId:Int,
    val testTitle:String,
    val maxScore:Int,
    var totalScore:Int = 0,
    val answerDate:String,
    val questions:List<QuestionAnswer>? = null,
){

}