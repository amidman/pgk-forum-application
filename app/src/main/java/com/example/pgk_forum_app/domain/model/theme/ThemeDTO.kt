package com.example.pgk_forum_app.domain.model.theme

import kotlinx.serialization.Serializable


@Serializable
data class ThemeDTO(
    val id:Int? = null,
    val teacherId:Int? = null,
    val themeName:String,
    val description:String = "",
)
