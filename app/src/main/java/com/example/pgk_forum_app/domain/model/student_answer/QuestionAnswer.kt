package com.example.pgk_forum_app.domain.model.student_answer

import kotlinx.serialization.Serializable


@Serializable
data class QuestionAnswer(
    val id:Int? = null,
    val studentAnswerId: Int? = null,
    val questionId:Int,
    val questionType:String,
    val questionNumber:Int,
    val questionScore:Int,
    val questionTitle:String,
    val rightAnswer:String,
    var studentAnswer: String,
    val questionOptions: List<String>
){
    fun toQuestionAnswerDTO(studentAnswerId: Int): QuestionAnswerDTO = QuestionAnswerDTO(id, studentAnswerId, questionId, studentAnswer)


}