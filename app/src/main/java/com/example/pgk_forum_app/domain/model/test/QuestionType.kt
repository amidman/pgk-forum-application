package com.example.pgk_forum_app.domain.model.test

import kotlinx.serialization.Serializable


@Serializable
enum class QuestionType {
    ONE_OPTION,TEXT
}