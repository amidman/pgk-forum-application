package com.example.pgk_forum_app.domain.utils

import com.example.pgk_forum_app.domain.model.network_base.ErrorModel
import com.example.pgk_forum_app.domain.model.network_base.RequestStatus
import io.ktor.client.call.*
import io.ktor.client.statement.*
import io.ktor.http.*

suspend inline fun <reified T> HttpResponse.toRequestStatus(): RequestStatus<T> {
    return if (status.isSuccess()){
        val body = body<T>()
        RequestStatus.Success(data = body)
    }else{
        val error = body<ErrorModel>()
        RequestStatus.Error(error)
    }
}


