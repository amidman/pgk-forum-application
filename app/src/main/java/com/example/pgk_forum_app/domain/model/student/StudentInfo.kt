package com.example.pgk_forum_app.domain.model.student

import kotlinx.serialization.Serializable


@Serializable
data class StudentInfo(
    val userId:Int? = 0,
    val fio:String,
    val login:String,
    val role:String,
    val studentId:Int,
    val groupName:String? = null,
)
