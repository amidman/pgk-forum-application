package com.example.pgk_forum_app.domain.model.group

import com.example.pgk_forum_app.domain.model.student.StudentInfo
import com.example.pgk_forum_app.domain.model.teacher.TeacherInfo
import kotlinx.serialization.Serializable


@Serializable
data class GroupInfo(
    val id:Int? = null,
    val teachers:List<TeacherInfo>,
    val groupName:String,
    val groupShortName:String,
    val students:List<StudentInfo>
)