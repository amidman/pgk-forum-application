package com.example.pgk_forum_app.domain.model.student

import com.example.pgk_forum_app.domain.model.user.UserDTO
import com.example.pgk_forum_app.domain.utils.ROLES
import kotlinx.serialization.Serializable


@Serializable
data class StudentInfoForRegistration(
    val fio:String,
    val login:String,
    val password:String,
    val role:String,
    val groupName:String? = null,
){
    fun userDTO() = UserDTO(fio = fio, login = login, password = password, role = ROLES.STUDENT.name)

    fun studentDTO(userId:Int) = StudentDTO(groupName = groupName, userId = userId)
}